Ext.define('Ext.ux.grid.SysXFiltersFeature', {
	extend : 'Ext.ux.grid.FiltersFeature',
	alias: 'feature.sysx.filters',
	defaultValues : [],
	onMenuBeforeShow : function() {
		console.log('onMenuBeforeShow');
		var defaultValues = this.defaultValues;
		var me = this;
        me.createFilters();
        me.onMenuBeforeShow();
		if(defaultValues.length > 0) {
			this.autoReload = false;
			Ext.each(defaultValues, function(item, index) {
				this.getFilter(item.dataIndex).setValue(item.value);
			}, this);
			console.log(this.filters);
			this.autoReload = true;
		}
	},
});

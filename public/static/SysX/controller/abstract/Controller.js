Ext.define('SysX.controller.abstract.Controller', {
	extend : 'Ext.app.Controller',
	onBeforeGetGridPanel : Ext.emptyFn,
	onAfterGetGridPanel : Ext.emptyFn,
	onBeforeGetWindow : Ext.emptyFn,
	onAfterGetWindow : Ext.emptyFn,
	onBeforeEditRecord : Ext.emptyFn,
	onAfterEditRecord : Ext.emptyFn,
	init : function() {
	},

	getModuleName : function() {
		return Ext.getClassName(this).replace(/^SysX\.controller\./, '');
	},

	getGridPanel : function(gridConfig) {
		if (Ext.emptyFn !== this.onBeforeGetGridPanel) {
			gridConfig = this.onBeforeGetGridPanel(gridConfig);
		}
		gridConfig = Ext.isObject(gridConfig) ? gridConfig : {};
		var view = Ext.create('SysX.view.' + this.getModuleName() + '.Grid', gridConfig);
		this.onAfterGetGridPanel(view);
		return view;
	},

	getWindow : function(windowConfig) {
		if (Ext.emptyFn !== this.onBeforeGetWindow) {
			windowConfig = this.onBeforeGetWindow(windowConfig);
		}
		//TODO: hard code to remove admin or client in the class name for SysX.store.xxx, need improvement
		windowConfig = Ext.isObject(windowConfig) ? windowConfig : {};
		var view = Ext.create('SysX.view.' + this.getModuleName() + '.Window', windowConfig);
		this.onAfterGetWindow(view);
		return view;

	},

	editRecord : function(record) {
		var view = this.getWindow();
		this.onBeforeEditRecord(view, record);

		view.down('form').load({
			method : 'get',
			params : {
				id : record.getId()
			}
		});
		this.onAfterEditRecord(view, record);
		return view;
	},

	saveForm : function(button, event) {
		var form = button.up('form').getForm();
		if (form.isValid()) {
			form.submit({
				url : this.getStore(this.getModuleName()).getProxy().url,
				success : function(form, action) {
					form.owner.up('window').close();
				},
				scope : this
			});
		}
	}
});

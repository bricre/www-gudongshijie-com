Ext.define('SysX.controller.MobileAdmin.ContainerProductTransaction', {
	extend : 'Ext.app.Controller',
	config : {
		views : ['SysX.view.MobileAdmin.ContainerProductTransaction.Main'],
		control : {
			'containerproducttransaction numberfield' : {
				keyup : function(view, e, eOpts) {
					if (e.event.keyCode === 13) {
						var button = view.up('list').down('button[itemId=search]');
						button.fireEvent('tap', button);
					}
				}
			},
			'viewcontainerproducttransactionmodifystock button[itemId=submit]' : {
				tap : function(button) {
					var form = button.up('formpanel'), values = form.getValues();
					if (values.product !== null && values.container_id !== null && values.quantity !== null) {
						form.down('button[itemId=submit]').disable();
						form.submit({
							success : function(form, result) {
								Ext.Msg.alert('Success', 'Inventory updated!');
								form.down('button[itemId=submit]').enable();
								form.reset();
							},
							failure : function(form, result) {
								form.down('button[itemId=submit]').enable();
								Ext.Msg.alert('Failed', 'Inventory updated failed!');
							},
							params : {
								product_id : values.product_id,
								container_id : values.container_id,
								quantity : values.add === true ? values.quantity : values.quantity * -1
							}
						});
					}
				}
			},
			'viewcontainerproducttransactionmovestock button[itemId=submit]' : {
				tap : function(button) {
					var form = button.up('formpanel'), values = form.getValues();
					if (values.product !== null && values.container_id_from !== null && values.container_id_to !== null && values.quantity !== null) {
						form.down('button[itemId=submit]').disable();
						form.submit({
							success : function(form, result) {
								Ext.Msg.alert('Success', 'Stock moved!');
								form.down('button[itemId=submit]').enable();
								form.reset();
							},
							failure : function(form, result) {
								form.down('button[itemId=submit]').enable();
								Ext.Msg.alert('Failed', 'Stock move failed!');
							},
							params : values
						});
					}
				}
			}
		}
	}
});

Ext.define('SysX.controller.MobileAdmin.Main', {
	extend : 'Ext.app.Controller',
	config : {
		views : ['SysX.view.MobileAdmin.Main'],
		control : {
			'containerproducttransaction numberfield' : {
				keyup : function(view, e, eOpts) {
					if (e.event.keyCode === 13) {
						var button = view.up('list').down('button[itemId=search]');
						button.fireEvent('tap', button);
					}
				}
			},
			'containerproducttransaction button[itemId=submit]' : {
				tap : function(button) {
					var form = button.up('formpanel'), values = form.getValues();
					if (values.product !== null && values.container_id !== null && values.quantity !== null) {
						form.submit({
							success : function(form, result) {
								Ext.Msg.alert('Success', 'Inventory updated!');
								form.reset();
							},
							failure : function(form, result) {
								Ext.Msg.alert('Failed', 'Inventory updated failed!');
							},
							params : {
								product_id : values.product_id,
								container_id : values.container_id,
								quantity : values.add === true ? values.quantity : values.quantity * -1
							}
						});
					}
				}
			}
		}
	}
});

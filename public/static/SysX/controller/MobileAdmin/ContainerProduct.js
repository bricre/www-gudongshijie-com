Ext.define('SysX.controller.MobileAdmin.ContainerProduct', {
	extend : 'Ext.app.Controller',
	config : {
		views : ['MobileAdmin.ContainerProduct.List', 'MobileAdmin.ContainerProductTransaction.Main'],
		control : {
			'viewcontainerproductlist' : {
				disclose : function(view, record, target, index, e, eOpts) {
					var mainView = view.up('main'), viewContainerProductTransactionMain = Ext.create('SysX.view.MobileAdmin.ContainerProductTransaction.Main'), viewcontainerproducttransactionmodifystock = viewContainerProductTransactionMain.down('viewcontainerproducttransactionmodifystock'), viewcontainerproducttransactionmovestock = viewContainerProductTransactionMain.down('viewcontainerproducttransactionmovestock');
					
					viewcontainerproducttransactionmovestock.down('numberfield[name=product_id]').setValue(record.get('product_id'));
					viewcontainerproducttransactionmovestock.down('numberfield[name=container_id_from]').setValue(record.get('container_id'));
					viewcontainerproducttransactionmovestock.down('numberfield[name=quantity]').setValue(record.get('quantity'));

					viewcontainerproducttransactionmodifystock.down('radiofield[action=minus]').check();
					viewcontainerproducttransactionmodifystock.down('numberfield[name=product_id]').setValue(record.get('product_id'));
					viewcontainerproducttransactionmodifystock.down('numberfield[name=container_id]').setValue(record.get('container_id'));
					viewcontainerproducttransactionmodifystock.down('numberfield[name=quantity]').setValue(record.get('quantity'));
					mainView.push(viewContainerProductTransactionMain);
				}
			}
		}
	}
});

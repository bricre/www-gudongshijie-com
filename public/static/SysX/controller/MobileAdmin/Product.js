Ext.define('SysX.controller.MobileAdmin.Product', {
	extend : 'Ext.app.Controller',
	config : {
		views : ['MobileAdmin.Product.Main'],
		control : {
			'viewproductmain searchfield' : {
				keyup : function(view, e, eOpts) {
					if (e.event.keyCode === 13) {
						var button = view.up('list').down('button[itemId=search]');
						button.fireEvent('tap', button);
					}
				}
			},
			'viewproductmain viewcontainerproductlist button[itemId=search]' : {
				tap : function(button) {
					var list = button.up('list'), store = list.getStore(), searchfield = list.down('searchfield');
					store.setRemoteFilter(true);
					store.clearFilter();
					store.setFilters(Ext.create('Ext.util.Filter', {
						property : 'product_id',
						value : searchfield.getValue()
					}));
					store.load();
					searchfield.blur();
				}
			},
			'viewproductmain viewcontainerproducttransactionlist button[itemId=search]' : {
				tap : function(button) {
					var list = button.up('list'), store = list.getStore(), searchfield = list.down('searchfield');
					store.setRemoteFilter(true);
					store.clearFilter();
					store.setSorters(Ext.create('Ext.util.Sorter', {
						property : 'update_time',
						direction : 'DESC'
					}));
					store.setFilters(Ext.create('Ext.util.Filter', {
						property : 'product_id',
						value : searchfield.getValue()
					}));
					store.load();
					searchfield.blur();
				}
			}
		}
	}
});

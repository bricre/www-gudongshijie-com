Ext.define('SysX.controller.MobileAdmin.Login', {
	extend : 'Ext.app.Controller',
	config : {
		views : ['SysX.view.MobileAdmin.Login'],
		control : {
			'viewlogin textfield' : {
				keyup : function(view, e, eOpts) {
					if (e.event.keyCode === 13) {
						var button = view.up('formpanel').down('button[itemId=submit]');
						button.fireEvent('tap', button);
					}
				}
			},
			'viewlogin button[itemId=submit]' : {
				tap : function(button) {
					var form = button.up('formpanel'), values = form.getValues();
					if (values.username.length > 0 && values.password.length > 0) {
						form.down('button[itemId=submit]').disable();
						form.submit({
							method : 'POST',
							success : function(form, result) {
								location.reload(false);
							},
							failure : function(form, result) {
								Ext.Msg.alert(SysX.t.MessageBoxTitle_Alert, result.message);
								form.reset();
								form.down('button[itemId=submit]').enable();
								form.down('textfield[name=username]').focus();
							}
						});
					}
				}
			}
		}
	}
});

Ext.define('SysX.controller.client.Consignment', {
	extend : 'SysX.controller.abstract.Controller',

	editRecord : function(record) {
		if (record.data.status == 'PROCESSING') {
			Ext.Msg.alert(SysX.t.MessageBoxTitle_Notice, 'You can no longer modify this ' + record.data.status + ' consignment');
			return false;
		}

		var view = this.callParent(arguments), additionalController, additionalView;

		view.down('textfield[name=id]').setVisible(true);
		view.down('fieldcontainer[itemId=price_fieldcontainer]').setVisible(true);
		view.down('displayfield[name=finish_time]').setVisible(true);
		view.down('datetimefield[name=arrive_time]').setVisible(true);

		//Recalculate delivery fee if delivery service is changed
		view.down('combo[name=delivery_service_id]').on('select', function(field, newValue, oldValue) {
			view.down('form').getForm().submit({
				params : {
					recalculateTotalFee : true,
					id : record.data.id
				},
				success : function(form, action) {
					form.load({
						method : 'GET',
						params : {
							id : record.data.id
						}
					});
				}
			});
		}, this);

		//Apply Calculate Fee button handler
		view.down('button[itemId=calculate_fee_button]').on('click', function(button) {
			button.up('form').getForm().submit({
				params : {
					recalculateTotalFee : true,
					id : record.data.id
				},
				success : function(form, action) {
					form.load({
						method : 'GET',
						params : {
							id : record.data.id
						}
					});
				}
			});
		}, this);

		//Add ConsignmentProduct panel
		additionalController = app.getController('SysX.controller.client.ConsignmentProduct');
		additionalView = additionalController.getGridPanel();
		additionalView.getStore().proxy.extraParams.consignment_id = record.data.id;
		additionalView.getStore().on({
			scope : view,
			'load' : view.reloadForm,
			'remove' : view.reloadForm
		});
		additionalController.onAfterGetWindow = function(view) {
			view.down('form').getForm().setValues({
				consignment_id : record.data.id,
				client_id : record.data.client_id
			});
			view.down('combo[itemId=ConsignmentItemNameCombo]').getStore().proxy.extraParams.client_id = record.data.client_id;
			view.down('combo[itemId=ConsignmentItemNameCombo]').getStore().proxy.extraParams.with_shared = true;
			view.down('combo[itemId=ConsignmentItemClientRefCombo]').getStore().proxy.extraParams.client_id = record.data.client_id;
			view.down('combo[itemId=ConsignmentItemClientRefCombo]').getStore().proxy.extraParams.with_shared = true;
			view.down('combo[itemId=ConsignmentItemCompanyRefCombo]').getStore().proxy.extraParams.client_id = record.data.client_id;
			view.down('combo[itemId=ConsignmentItemCompanyRefCombo]').getStore().proxy.extraParams.with_shared = true;
		};

		view.down('tabpanel').add(additionalView);

		//Add ContainerProductTransaction Panel
		additionalController = app.getController('SysX.controller.client.ContainerProductTransaction');
		additionalView = additionalController.getGridPanel();
		additionalView.getStore().proxy.extraParams.consignment_id = record.data.id;
		additionalController.onAfterGetWindow = function(view) {
			view.down('form').getForm().setValues({
				consignment_id : record.data.id
			});
		};
		view.down('tabpanel').add(additionalView);

		//Add Ticket panel
		if (isBS) {
			additionalController = app.getController('SysX.controller.client.Ticket');
			additionalView = additionalController.getGridPanel();
			additionalView.getStore().proxy.extraParams.sysxtype = 'Consignment';
			additionalView.getStore().proxy.extraParams.record_id = record.data.id;
			additionalController.onAfterGetWindow = function(view) {
				view.down('form').getForm().setValues({
					sysxtype : 'Consignment',
					record_id : record.data.id
				});
			};

			view.down('tabpanel').add(additionalView);
		}

		if (record.data.status === 'FINISHED' || record.data.status === 'PENDING') {
			var form = view.down('form'), formItems = form.getForm().getFields();
			formItems.each(function(field) {
				field.disable();
			});
			
			form.down('button[itemId=saveButton]').disable();

			// Remove toolbar from ConsignmentProduct tab
			var consignmentProductGridPanel = view.down('gridpanel[itemId=client.ConsignmentProduct.Grid]');
			consignmentProductGridPanel.down('toolbar[dock=top]').removeAll();
			consignmentProductGridPanel.disableItemDbClick = true;
		}
		
		view.down('datetimefield[name=arrive_time]').enable();
		view.down('textfield[name=id]').enable();
		return view;
	}
});

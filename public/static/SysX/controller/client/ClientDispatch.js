Ext.define('SysX.controller.client.ClientDispatch', {
	extend : 'SysX.controller.abstract.Controller',
	editRecord : function(record) {
		var view = this.callParent(arguments), additionalController, additionalView;

		if ('EXPRESS' != record.data.final_freight_method) {
			view.down('combo[name=warehouse_dispatch_freight_id]').setVisible(true);
		}

		view.down('button[itemId="ClientDispatchFileUploadButton"]').setVisible(true);
		view.down('button[itemId="ClientDispatchPrintPackingListButton"]').setVisible(true);

		// Add ClientDispatchContainer panel
		additionalController = app.getController('SysX.controller.client.ClientDispatchContainer');
		additionalView = additionalController.getGridPanel();
		additionalView.getStore().proxy.extraParams.client_dispatch_id = record.data.id;
		additionalController.onAfterGetWindow = function(view) {
			view.down('form').getForm().setValues({
				client_dispatch_id : record.data.id
			});
		};
		view.down('tabpanel').add(additionalView);

		// Add WarehouseDispatchClientDispatch panel
		additionalController = app.getController('SysX.controller.client.WarehouseDispatchClientDispatch');
		additionalView = additionalController.getGridPanel();
		additionalView.getStore().proxy.extraParams.client_dispatch_id = record.data.id;
		additionalController.onAfterGetWindow = function(view) {
			view.down('form').getForm().setValues({
				client_dispatch_id : record.data.id
			});
		};
		view.down('tabpanel').add(additionalView);
	},
	onBeforeEditRecord : function(view, record) {
		view.down('combo[name=warehouse_dispatch_freight_id]').valueNotFoundText = record.get('warehouse_dispatch_freight_reference');
	}
});

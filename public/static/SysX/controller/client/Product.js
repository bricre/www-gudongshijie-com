Ext.define('SysX.controller.client.Product', {
	extend : 'SysX.controller.abstract.Controller',
	editRecord : function(record) {
		var view = this.callParent(arguments), additionalController, additionalView;

		//Show Barcode
		view.down('form').down('displayfield[itemId="clientProductBarcode"]').update({
			src : modulePath + '/Printer/Product-Barcode/' + record.data.id,
			id : record.data.id
		});

		//Show Producte Category
		// var updateProductCategoryDisplayFieldQuery = {
		// url : modulePath + '/Product-Product-Category/',
		// method : 'GET',
		// params : {
		// product_id : record.data.id
		// },
		// success : function(response, opts) {
		// var result = Ext.decode(response.responseText);
		// if (result.success) {
		// view.down('displayfield[itemId=productProductCategoryDisplayField]').update(result.data.length > 0 ? result.data : {
		// product_category_name : 'Click to set Category'
		// });
		// }
		// }
		// };
		// Ext.Ajax.request(updateProductCategoryDisplayFieldQuery);
		//
		// view.down('displayfield[itemId=productProductCategoryDisplayField]').getEl().on('click', function() {
		// var view = app.getController('SysX.controller.client.ProductCategory').getGridPanel({
		// withCheckBox : true,
		// buttons : [{
		// text : 'Submit',
		// handler : function() {
		// var checkedNodesString = '', checkedNodes = view.getChecked();
		// Ext.each(checkedNodes, function(node) {
		// if (checkedNodesString.length > 0) {
		// checkedNodesString += ','
		// };
		// checkedNodesString += node.id;
		// });
		// Ext.Ajax.request({
		// url : modulePath + '/Product-Product-Category/',
		// method : 'POST',
		// params : {
		// productId : record.data.id,
		// productCategoryIdString : checkedNodesString
		// },
		// success : function(response, opts) {
		// var result = Ext.decode(response.responseText);
		// if (result.success) {
		// Ext.Ajax.request(updateProductCategoryDisplayFieldQuery);
		// ProductCategoryWindow.close();
		// }
		// }
		// });
		// }
		// }]
		// });
		//
		// view.getRootNode().on('expand', function() {
		// Ext.getCmp('ProductCategorySelectionWindow').getEl().mask('Loading');
		// Ext.Ajax.request({
		// url : modulePath + '/Product-Product-Category/',
		// method : 'GET',
		// params : {
		// product_id : record.data.id
		// },
		// success : function(response, opts) {
		// Ext.getCmp('ProductCategorySelectionWindow').getEl().unmask();
		// var result = Ext.decode(response.responseText);
		// if (result.success) {
		// Ext.each(result.data, function(record) {
		// var node = view.getRootNode().findChild('id', record.product_category_id, true);
		// if (node) {
		// node.ui.toggleCheck(true);
		// }
		// });
		// }
		// }
		// });
		// });
		//
		// var ProductCategoryWindow = Ext.create('Ext.window.Window', {
		// id : 'ProductCategorySelectionWindow',
		// autoScroll : true,
		// width : Ext.getBody().getWidth() / 2,
		// height : Ext.getBody().getHeight() / 2,
		// modal : true,
		// items : view
		// });
		//
		// ProductCategoryWindow.show();
		// });

		//Add ProductSecondarySku Panel
		additionalController = app.getController('SysX.controller.client.ProductSecondarySku');
		additionalView = additionalController.getGridPanel({
			disableTopToolbar : false
		});
		additionalView.getStore().proxy.extraParams.product_id = record.data.id;
		additionalController.onAfterGetWindow = function(view) {
			view.down('form').getForm().setValues({
				product_id : record.data.id,
				'product-name' : record.data.name
			});
		};
		view.down('tabpanel').add(additionalView);

		//Add ProductSalesHistory Panel
		additionalController = app.getController('SysX.controller.client.ProductSalesHistory');
		additionalView = additionalController.getGridPanel({
			disableTopToolbar : true
		});
		additionalView.getStore().proxy.extraParams.product_id = record.data.id;
		view.down('tabpanel').add(additionalView);

		//Add ContainerProductTransaction Panel
		additionalController = app.getController('SysX.controller.client.ContainerProductTransaction');
		additionalView = additionalController.getGridPanel();
		additionalView.getStore().proxy.extraParams.product_id = record.data.id;
		additionalController.onAfterGetWindow = function(view) {
			view.down('form').getForm().setValues({
				product_id : record.data.id
			});
		};
		view.down('tabpanel').add(additionalView);

		//Add Ticket panel
		if (isBS) {
			additionalController = app.getController('SysX.controller.client.Ticket');
			additionalView = additionalController.getGridPanel();
			additionalView.getStore().proxy.extraParams.sysxtype = 'Product';
			additionalView.getStore().proxy.extraParams.record_id = record.data.id;
			additionalController.onAfterGetWindow = function(view) {
				view.down('form').getForm().setValues({
					sysxtype : 'Product',
					record_id : record.data.id
				});
			};
			view.down('tabpanel').add(additionalView);
		}

		return view;
	}
});

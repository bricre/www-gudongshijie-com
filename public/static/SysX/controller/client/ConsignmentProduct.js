Ext.define('SysX.controller.client.ConsignmentProduct', {
	extend : 'SysX.controller.abstract.Controller',
	requires : ['SysX.model.Product'],
	editRecord : function(record) {
		var view = this.callParent(arguments),productModel = app.getModel('SysX.model.Product'),productStore = app.getStore('SysX.store.Product');
		productModel.setProxy(productStore.getProxy());
		productModel.load(record.get('product_id'), {
			callback : function(productRecord,operation) {
				view.down('combo[itemId=ConsignmentItemNameCombo]').setRawValue(productRecord.data.name);
				view.down('combo[itemId=ConsignmentItemClientRefCombo]').setRawValue(productRecord.data.client_ref);
				view.down('combo[itemId=ConsignmentItemCompanyRefCombo]').setRawValue(productRecord.data.company_ref);
				view.down('numberfield[name=quantity]').setRawValue(record.data.quantity);
				
				view.down('combo[itemId=ConsignmentItemNameCombo]').disable();
				view.down('combo[itemId=ConsignmentItemClientRefCombo]').disable();
				view.down('combo[itemId=ConsignmentItemCompanyRefCombo]').disable();
			}
		});
		return view;
	}
});

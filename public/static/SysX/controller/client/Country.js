Ext.define('SysX.controller.client.Country',{
	extend:'SysX.controller.abstract.Controller',
	init:function(){
		this.control({
			'#CountryButton':{
				click:this.showGridPanel
			},
			'CountryGrid':{
				itemdblclick:this.onEdit
			},
			'CountryWindow button[action=save]':{
				click:this.saveForm
			}
		});
	}
});
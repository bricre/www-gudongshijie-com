Ext.define('SysX.controller.client.ClientUser',{
	extend:'SysX.controller.abstract.Controller',
	init:function(){
		this.control({
			'#ClientUserButton':{
				click:this.showGridPanel
			},
			'ClientUserGrid':{
				itemdblclick:this.onEdit
			},
			'ClientUserWindow button[action=save]':{
				click:this.saveForm
			}
		});
	}
});
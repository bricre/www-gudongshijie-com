Ext.define('SysX.controller.client.ProductSalesHistory', {
	extend : 'SysX.controller.abstract.Controller',
	require:['SysX.controller.client.Consignment'],
	getGridPanel : function() {
		return this.callParent([{
			disableTopToolbar : true
		}]);
	},
	editRecord : function(record) {
		var ConsignmentController = app.getController('SysX.controller.client.Consignment');
		ConsignmentController.editRecord(record);
	}
}); 
Ext.define('SysX.controller.client.ClientUserGroup', {
	extend : 'SysX.controller.abstract.Controller',
	editRecord : function(record) {
		var view = this.callParent(arguments), additionalController, additionalView;

		// Add ClientUserGroupPrivilege panel
		additionalController = app.getController('SysX.controller.client.ClientUserGroupPrivilege');
		additionalView = additionalController.getGridPanel({
			sysxtype : 'ClientUserGroupPrivilege',
			pageSize : 0,
			disableTopToolbar : true,
			disableBottomToolbar : true,
			disableItemDbClick : true
		});
		additionalView.getStore().proxy.extraParams.client_user_group_id = record.data.id;

		view.down('tabpanel').add(additionalView);
		return view;
	}
});

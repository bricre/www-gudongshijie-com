Ext.define('SysX.controller.client.DeliveryService', {
	extend : 'SysX.controller.abstract.Controller',
	editRecord : function(record) {
		var view = this.callParent(arguments), additionalController, additionalView, deliveryServiceStampImageButton = view.down('button[itemId="DeliveryServiceStampImage"]'), fileUploadWindow;

		deliveryServiceStampImageButton.setIcon('/upload/delivery_service_stamp/' + record.data.id + '.jpg');
		deliveryServiceStampImageButton.enable();
		deliveryServiceStampImageButton.on('click', function() {
			Ext.create('Ext.window.Window', {
				title : (view.title) + ' File Upload',
				width : 350,
				height : 120,
				maximizable : false,
				modal : true,
				autoShow : true,
				layout : 'fit',
				items : [{
					xtype : 'form',
					fileUpload : true,
					bodyStyle : 'padding:5px',
					items : [{
						name : 'userfile',
						xtype : 'fileuploadfield',
						allowBlank : false,
						fieldLabel : 'File',
						buttonText : 'Browse'
					}, {
						name : 'delivery_service_id',
						xtype : 'hidden',
						value : record.data.id
					}],
					buttons : [{
						text : 'Cancel',
						handler : function(button) {
							button.up('window').destroy();
						}
					}, {
						text : 'Upload',
						handler : function(button) {
							if (button.up('form').getForm().isValid()) {
								button.up('form').getForm().submit({
									url : modulePath + '/Upload/Delivery-Service-Stamp',
									success : function(formPanel, action) {
										deliveryServiceStampImageButton.setIcon('/upload/delivery_service_stamp/' + record.data.id + '.jpg?' + Ext.id());
										formPanel.owner.up('window').destroy();
									},
									failure : function(formPanel, action) {
										Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, 'File upload failed:<br />' + action.result.message);
									}
								});
							}
						}
					}]
				}]
			})
		});

		// Add Delivery Service Country panel
		additionalController = app.getController('SysX.controller.client.Country');
		additionalView = additionalController.getGridPanel({
			sysxtype : 'Country',
			pageSize : 0,
			disableTopToolbar : true,
			disableBottomToolbar : true,
			disableItemDbClick : true,
			sm : {
				checkOnly : true
			},
			DeliveryServiceCountryUpdateCounter : 0
		});

		additionalView.getStore().on('load', function(store, records, options) {
			additionalView.getEl().mask(SysX.t.Loading);
			Ext.Ajax.request({
				url : modulePath + '/Delivery-Service-Country',
				method : 'GET',
				params : {
					delivery_service_id : record.data.id
				},
				callback : function(options, success, response) {
					additionalView.getEl().unmask();
					if (success) {
						var deliveryServiceCountries = Ext.decode(response.responseText).data, deliveryServiceCountryRecords = [], countryStoreMap = additionalView.getStore().data.map;
						for (var i = 0; i < deliveryServiceCountries.length; i++) {
							deliveryServiceCountryRecords.push(countryStoreMap[deliveryServiceCountries[i].country_iso]);
						}
						additionalView.getSelectionModel().suspendEvents();
						additionalView.getSelectionModel().select(deliveryServiceCountryRecords);
						additionalView.getSelectionModel().resumeEvents();
					}
				}
			});
		});

		additionalView.on('select', function(selModel, countryRecord, rowIndex) {
			additionalView.DeliveryServiceCountryUpdateCounter++;
			additionalView.getEl().mask(SysX.t.Loading);
			Ext.Ajax.request({
				url : modulePath + '/Delivery-Service-Country',
				method : "POST",
				params : {
					delivery_service_id : record.data.id,
					country_iso : countryRecord.data.iso
				},
				callback : function(options, success, response) {
					additionalView.DeliveryServiceCountryUpdateCounter--;
					if (0 === additionalView.DeliveryServiceCountryUpdateCounter) {
						additionalView.getEl().unmask();
					}
				}
			});
		});

		additionalView.on('deselect', function(selModel, countryRecord, rowIndex) {
			additionalView.DeliveryServiceCountryUpdateCounter++;
			additionalView.getEl().mask(SysX.t.Loading);
			Ext.Ajax.request({
				url : modulePath + '/Delivery-Service-Country/' + record.data.id + '_' + countryRecord.data.iso,
				method : "DELETE",
				callback : function(options, success, response) {
					additionalView.DeliveryServiceCountryUpdateCounter--;
					if (0 === additionalView.DeliveryServiceCountryUpdateCounter) {
						additionalView.getEl().unmask();
					}
				}
			});
		});

		view.down('tabpanel').add(additionalView);
	}
});

Ext.define('SysX.controller.client.Product_InternalShared', {
	extend : 'SysX.controller.abstract.Controller',
	editRecord : function(record) {
		var view = this.callParent(arguments), additionalController, additionalView;
		view.down('displayfield[itemId=sku]').setValue(record.data.client_id + '#' + record.data.id);

		return view;
	},
	onAfterGetGridPanel : function(view) {
		view.getStore().proxy.extraParams.is_shared_internal = 1;
	}
});

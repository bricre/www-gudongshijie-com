Ext.define('SysX.controller.client.ClientCreditLimitTopUpTransaction', {
	extend : 'SysX.controller.abstract.Controller',
	onAfterGetWindow : function(view) {
		if (Ext.isEmpty(view.down('hidden[name=id]').getValue())) {
			Ext.Ajax.request({
				url : modulePath + '/Client/Get-Client-Group-Info',
				method : 'GET',
				success : function(response, opts) {
					var result = Ext.decode(response.responseText);
					if (result.success) {
						view.down('textfield[itemId=AmountDisplayField]').setValue(result.data.credit_limit_top_up_amount);
					}
				}
			});
			Ext.Ajax.request({
				url : modulePath + '/Client/Get-Number-Of-Available-Credit-Limit-Top-Up',
				method : 'GET',
				success : function(response, opts) {
					var result = Ext.decode(response.responseText);
					if (result.success) {
						view.down('[itemId=infoPanel]').update(result);
					}
				}
			});
		}

	}
});

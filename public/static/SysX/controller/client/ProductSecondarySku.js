Ext.define('SysX.controller.client.ProductSecondarySku', {
	extend : 'SysX.controller.abstract.Controller',
	editRecord : function(record) {
		var additionalController, additionalView;
		additionalController = app.getController('SysX.controller.client.Product');
		return additionalController.editRecord(Ext.create('SysX.model.Product', {
			id : record.data.product_id
		}));
	}
});

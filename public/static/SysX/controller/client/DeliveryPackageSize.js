Ext.define('SysX.controller.client.DeliveryPackageSize',{
	extend:'SysX.controller.abstract.Controller',
	init:function(){
		this.control({
			'#DeliveryPackageSizeButton':{
				click:this.showGridPanel
			},
			'DeliveryPackageSizeGrid':{
				itemdblclick:this.onEdit
			},
			'DeliveryPackageSizeWindow button[action=save]':{
				click:this.saveForm
			}
		});
	}
});
Ext.define('SysX.controller.client.ClientDispatchContainer', {
	extend : 'SysX.controller.abstract.Controller',
	editRecord : function(record) {
		var view = this.callParent(arguments), additionalController, additionalView;

		view.down('displayfield[itemId="ClientDispatchContainerBarcode"]').update({
			src : modulePath + '/Printer/Barcode/' + record.data.client_dispatch_id + '-' + record.data.id
		});

		// Add ClientDispatchContainer panel
		additionalController = app.getController('SysX.controller.client.ClientDispatchContainerProduct');
		additionalView = additionalController.getGridPanel();
		additionalView.getStore().proxy.extraParams.client_dispatch_container_id = record.data.id;
		additionalController.onAfterGetWindow = function(view) {
			view.down('form').getForm().setValues({
				client_dispatch_container_id : record.data.id
			});
		};
		view.down('tabpanel').add(additionalView);

	}
}); 
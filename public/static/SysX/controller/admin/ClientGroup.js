Ext.define('SysX.controller.admin.ClientGroup', {
	extend : 'SysX.controller.abstract.Controller',
	editRecord : function(record) {
		var view = this.callParent(arguments), additionalController, additionalView;

		// Add ClientGroupDeliveryPackageSizeHandlingFee Grid
		additionalController = app.getController('SysX.controller.admin.ClientGroupDeliveryPackageSizeHandlingFee');
		additionalView = additionalController.getGridPanel();
		additionalView.getStore().proxy.extraParams.client_group_id = record.data.id;

		view.down('tabpanel').add(additionalView);

		// Add ClientGroupDeliveryPackageSizeHandlingFee Grid
		additionalController = app.getController('SysX.controller.admin.Client');
		additionalView = additionalController.getGridPanel();
		additionalView.getStore().proxy.extraParams.client_group_id = record.data.id;

		view.down('tabpanel').add(additionalView);

		return view;
	}
}); 
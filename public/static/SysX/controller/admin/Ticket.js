Ext.define('SysX.controller.admin.Ticket', {
	extend : 'SysX.controller.abstract.Controller',
	editRecord : function(record) {
		//Only edit a parent id
		if (record.data.parent_id) {
			return false;
		}
		var proxy, ticketGrid = this.getGridPanel({
			region : 'north',
			title : null,
			selModel : null,
			disableTopToolbar : true,
			disableBottomToolbar : true,
			columns : [{
				header : SysX.t.User,
				dataIndex : 'user_name',
				excludeFromSelect : true,
				sortable : false,
				flex : 1
			}, {
				header : SysX.t.Time,
				dataIndex : 'create_time',
				sortable : true,
				flex : 1
			}, {
				header : SysX.t.Note,
				dataIndex : 'content',
				sortable : false,
				flex : 4,
				tpl : Ext.create('Ext.XTemplate', '<div style="white-space:normal;">{content}</div>')
			}]
		});
		ticketGrid.getStore().getProxy().extraParams.parent_id = record.data.id;

		var formConfig = {
			region : 'south',
			height : 200,
			xtype : 'form',
			url : ticketGrid.getStore().proxy.url,
			items : [{
				name : 'parent_id',
				xtype : 'hidden',
				value : record.data.id
			}, {
				name : 'content',
				//fieldLabel : 'Reply to this ticket',
				xtype : 'htmleditor',
				anchor : '200px 100%',
				autoScroll : true
			}],
			bbar : ['->', {
				text : SysX.t.Save,
				xtype : 'button',
				handler : function() {
					this.up('form').getForm().submit({
						success : function(form, action) {
							ticketGrid.getStore().load();
							form.owner.down('htmleditor').reset();
						},
						failure : function(form, action) {
							Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, action.result.message);
						}
					});
				}
			}]
		};
		var windowConfig = {
			title : record.data.subject,
			maximizable : true,
			width : Ext.getBody().getWidth() * 0.75,
			height : Ext.getBody().getHeight() * 0.75,
			modal : true,
			layout : 'border',
			border : true,
			items : [ticketGrid, formConfig],
			tbar : ['->', {
				hiddenName : 'status',
				fieldLabel : SysX.t.Status,
				allowBlank : false,
				xtype : 'combo',
				anchor : '50%',
				value : record.data.status,
				valueField : 'value',
				displayField : 'text',
				mode : 'local',
				forceSelection : true,
				triggerAction : 'all',
				editable : false,
				store : new Ext.data.ArrayStore({
					fields : ['value', 'text', 'attr'],
					data : Ext.Object.getValues(Ext.create('SysX.model.Ticket').status)
				}),
				listeners : {
					scope : Ext.apply(this, {
						record : record
					}),
					select : function(combo, record, index) {
						Ext.Ajax.request({
							url : app.getStore('SysX.store.Ticket').getProxy().url,
							method : 'POST',
							params : {
								id : this.record.data.id,
								status : record[0].data.value
							},
							success : function(response, opts) {
								Ext.Msg.alert(SysX.t.MessageBoxTitle_Success, SysX.t.TicketMessage_StatusUpdateSuccess);
							},
							failure : function(response, opts) {
								Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, SysX.t.TicketMessage_StatusUpdateFailed);
							}
						});
					}
				}
			}]
		};

		if (record.data.sysxtype && record.data.record_id) {
			windowConfig.tbar.unshift({
				xtype : 'button',
				text : record.data.sysxtype + ' ' + record.data.record_id,
				handler : function() {
					var recordController = app.getController('SysX.controller.admin.' + record.data.sysxtype), recordModel = app.getModel('SysX.model.' + record.data.sysxtype);

					recordModel.setProxy(app.getStore('SysX.store.' + record.data.sysxtype).getProxy());
					recordModel.load(record.data.record_id, {
						success : function(record) {
							recordController.editRecord(record);
						}
					});

				}
			});
		}
		ticketWindow = Ext.create('Ext.window.Window', windowConfig);
		ticketWindow.show();
	}
});

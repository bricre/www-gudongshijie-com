Ext.define('SysX.controller.admin.Client', {
	extend : 'SysX.controller.abstract.Controller',
	editRecord : function(record) {
		var view = this.callParent(arguments), additionalController, additionalView;

		view.down('tabpanel').on({
			scope : this,
			'tabchange' : function(tabPanel, panel) {
				if (panel.title === SysX.t.BasicInfo && this.needReload === true) {
					view.down('form').load({
						method : 'get',
						params : {
							id : view.down('form').getForm().getValues().id
						}
					});
					this.needReload = false;
				}
			}
		});

		//Add Product Panel
		additionalController = app.getController('SysX.controller.admin.Product');
		additionalView = additionalController.getGridPanel();
		additionalView.getStore().proxy.extraParams.client_id = record.data.id;
		additionalController.onAfterGetWindow = function(view) {
			view.down('form').getForm().setValues({
				client_id : record.data.id
			});
		};
		view.down('tabpanel').add(additionalView);

		// //Add Consignment panel
		additionalController = app.getController('SysX.controller.admin.Consignment');
		additionalView = additionalController.getGridPanel();
		additionalView.getStore().proxy.extraParams.client_id = record.data.id;
		additionalView.getStore().on({
			scope : this,
			'load' : function() {
				this.needReload = true;
			},
			'remove' : function() {
				this.needReload = true;
			}
		});
		additionalController.onAfterGetWindow = function(view) {
			view.down('form').getForm().setValues({
				client_id : record.data.id
			});
		};
		Ext.Array.each(additionalView.columns, function(item) {
			if ('status' === item.dataIndex) {
				item.filter.value = ['FINISHED'];
			}
		});
		view.down('tabpanel').add(additionalView);

		// //Add ClientDispatch panel
		additionalController = app.getController('SysX.controller.admin.ClientDispatch');
		additionalView = additionalController.getGridPanel();
		additionalView.getStore().proxy.extraParams.client_id = record.data.id;
		additionalView.getStore().on({
			scope : this,
			'load' : function() {
				this.needReload = true;
			},
			'remove' : function() {
				this.needReload = true;
			}
		});
		additionalController.onAfterGetWindow = function(view) {
			view.down('form').getForm().setValues({
				client_id : record.data.id
			});
		};
		view.down('tabpanel').add(additionalView);

		//Add ClientUser panel
		additionalController = app.getController('SysX.controller.admin.ClientUser');
		additionalView = additionalController.getGridPanel();
		additionalView.getStore().proxy.extraParams.client_id = record.data.id;
		additionalController.onAfterGetWindow = function(view) {
			view.down('form').getForm().setValues({
				client_id : record.data.id
			});
		};
		view.down('tabpanel').add(additionalView);

		//Add ClientStatement panel
		additionalController = app.getController('SysX.controller.admin.ClientStatement');
		additionalView = additionalController.getGridPanel();
		additionalView.getStore().proxy.extraParams.client_id = record.data.id;
		additionalView.getStore().on({
			scope : this,
			'load' : function() {
				this.needReload = true;
			},
			'remove' : function() {
				this.needReload = true;
			}
		});
		additionalController.onAfterGetWindow = function(view) {
			view.down('form').getForm().setValues({
				client_id : record.data.id
			});
		};
		view.down('tabpanel').add(additionalView);

		//Add ClientCreditLimitTopUpTransaction panel
		additionalController = app.getController('SysX.controller.admin.ClientCreditLimitTopUpTransaction');
		additionalView = additionalController.getGridPanel();
		additionalView.getStore().proxy.extraParams.client_id = record.data.id;
		additionalView.getStore().on({
			scope : this,
			'load' : function() {
				this.needReload = true;
			},
			'remove' : function() {
				this.needReload = true;
			}
		});
		additionalController.onAfterGetWindow = function(view) {
			view.down('form').getForm().setValues({
				client_id : record.data.id
			});
			if (Ext.isEmpty(view.down('hidden[name=id]').getValue())) {
				Ext.Ajax.request({
					url : modulePath + '/Client-Group/Get-By-Client-Id',
					method : 'GET',
					params : {
						client_id : view.down('hidden[name=client_id]').getValue()
					},
					success : function(response, opts) {
						var result = Ext.decode(response.responseText);
						if (result.success) {
							view.down('textfield[itemId=AmountDisplayField]').setValue(result.data.credit_limit_top_up_amount);
						}
					}
				});
			}

		};
		view.down('tabpanel').add(additionalView);

		//Add Ticket panel
		if (isBS) {
			additionalController = app.getController('SysX.controller.admin.Ticket');
			additionalView = additionalController.getGridPanel();
			additionalView.getStore().proxy.extraParams.client_id = record.data.id;
			additionalController.onAfterGetWindow = function(view) {
				view.down('form').getForm().setValues({
					client_id : record.data.id
				});
			};
			view.down('tabpanel').add(additionalView);
		}

		return view;
	}
});

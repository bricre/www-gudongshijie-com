Ext.define('SysX.controller.admin.CompanyUser', {
	extend : 'SysX.controller.abstract.Controller',
	editRecord : function(record) {
		var view = this.callParent(arguments), additionalController, additionalView;

		view.down('button[itemId=generate_new_api_key_button]').on('click', function(button) {
			Ext.Ajax.request({
				url : modulePath + '/Company-User/Generate-New-Api-Key',
				method : 'POST',
				params : {
					id : record.data.id
				},
				success : function(response, opts) {
					var result = Ext.decode(response.responseText);
					view.down('displayfield[itemId=api_key]').setValue(result.data[0].api_key);
				}
			});
		}, this);

		return view;
	}
});

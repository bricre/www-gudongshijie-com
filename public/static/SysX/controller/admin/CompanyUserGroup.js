Ext.define('SysX.controller.admin.CompanyUserGroup', {
	extend : 'SysX.controller.abstract.Controller',
	editRecord : function(record) {
		var view = this.callParent(arguments), additionalController, additionalView;

		// Add CompanyUserGroupPrivilege panel
		additionalController = app.getController('SysX.controller.admin.CompanyUserGroupPrivilege');
		additionalView = additionalController.getGridPanel({
			sysxtype : 'CompanyUserGroupPrivilege',
			pageSize : 0,
			disableTopToolbar : true,
			disableBottomToolbar : true,
			disableItemDbClick : true
		});
		additionalView.getStore().proxy.extraParams.company_user_group_id = record.data.id;

		view.down('tabpanel').add(additionalView);
		return view;
	}
});

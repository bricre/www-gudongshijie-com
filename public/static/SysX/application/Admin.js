Ext.define('SysX.application.Admin', {
	extend : 'Ext.app.Application',
	name : 'SysX',
	appFolder : '/static/SysX',
	autoCreateViewport : false,

	requires : ['SysX.Locale', 'SysX.view.Viewport', 'SysX.AppManager'],

	onBeforeLaunch : function() {
		var UsersController = this.getController('SysX.controller.admin.Users');
		if (UsersController.checkLoginStatus()) {
			this.createViewPort();
		} else {
			Ext.create('SysX.view.admin.User.Login').show();
		}
	},

	createViewPort : function() {
		if (viewport) {
			viewport.destroy();
		}( function(user) {
				var items = [{
					text : SysX.t.Listing,
					itemId : 'ListingButton',
					module : 'admin.Listing'
				}, {
					text : SysX.t.Vendor,
					itemId : 'VendorButton',
					module : 'admin.Vendor'
				}, {
					text : SysX.t.UserManagement,
					menu : {
						defaults : {
							handler : SysX.AppManager.addModuleGrid
						},
						items : [{
							text : SysX.t.Users,
							itemId : 'CompanyUserButton',
							module : 'admin.CompanyUser'
						}, {
							text : SysX.t.UserGroup,
							itemId : 'CompanyUserGroupButton',
							module : 'admin.CompanyUserGroup'
						}]
					}
				}];

				SysX.view.Viewport.override({
					toolbarItems : items,
					menuPosition : menuPosition || 'top'
				});
			}(userHash));

		viewport = Ext.create('SysX.view.Viewport', {
			homePanel : Ext.create('SysX.view.admin.home.HomePanel')
		});
	}
});

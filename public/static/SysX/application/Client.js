Ext.define('SysX.application.Client', {
	extend : 'Ext.app.Application',
	name : 'SysX',
	appFolder : '/static/SysX',
	autoCreateViewport : false,

	requires : ['SysX.Locale', 'SysX.view.Viewport', 'SysX.AppManager'],

	onBeforeLaunch : function() {
		var UsersController = this.getController('SysX.controller.client.Users');
		if (UsersController.checkLoginStatus()) {
			this.createViewPort();
		} else {
			Ext.create('SysX.view.client.User.Login').show();
		}
	},

	createViewPort : function() {
		if (viewport) {
			viewport.destroy();
		}( function(user) {
				var items = [{
					text : SysX.t.Consignment,
					menu : {
						items : [{
							text : SysX.t.LocalConsignment,
							handler : SysX.AppManager.addModuleGrid,
							module : 'client.Consignment_Local'
						}, {
							text : SysX.t.DirectConsignment,
							handler : SysX.AppManager.addModuleGrid,
							module : 'client.Consignment_Direct'
						}, {
							text : SysX.t.ReturnConsignment,
							handler : SysX.AppManager.addModuleGrid,
							module : 'client.Consignment_Return'
						}, '-', {
							text : SysX.t.BulkOrder,
							handler : SysX.AppManager.addModuleGrid,
							module : 'client.TempConsignmentInfo'
						}]
					}
				}, '-', {
					text : SysX.t.Products,
					menu : {
						items : ( function() {
								var items = [];
								items.push({
									text : SysX.t.MyProduct,
									handler : SysX.AppManager.addModuleGrid,
									module : 'client.Product'
								}, {
									text : SysX.t.ProductSecondarySku,
									handler : SysX.AppManager.addModuleGrid,
									module : 'client.ProductSecondarySku'
								}, '-', {
									text : SysX.t.SharedProduct,
									handler : SysX.AppManager.addModuleGrid,
									module : 'client.Product_InternalShared'
								});

								if (true === user.is_external) {
									items.push({
										text : SysX.t.PublicProduct,
										handler : SysX.AppManager.addModuleGrid,
										module : 'client.Product_ExternalShared'
									});
								}

								return items;
							}())
					}
				}];

				items.push('-', {
					text : SysX.t.Dispatches,
					menu : {
						items : ( function() {
								var items = [];
								items.push({
									text : SysX.t.Dispatches,
									handler : SysX.AppManager.addModuleGrid,
									module : 'client.ClientDispatch'
								});

								if (isBS) {
									items.push({
										text : SysX.t.ECPPDispatches,
										hrefTarget : '_blank',
										href : modulePath + '/Client/Redirect-To-Ecpp'
									});
								}
								items.push('-');
								items.push({
									text : SysX.t.WarehouseDispatchedStock,
									handler : SysX.AppManager.addModuleGrid,
									module : 'client.WarehouseDispatchContainerProduct'
								});
								return items;
							}())
					}
				});

				items.push('-', {
					text : SysX.t.ClientStatement,
					handler : SysX.AppManager.addModuleGrid,
					module : 'client.ClientStatement'
				});

				if (isBS) {
					items.push('-', {
						text : SysX.t.Tickets,
						handler : SysX.AppManager.addModuleGrid,
						module : 'client.Ticket'
					});
				}

				items.push('-', {
					text : SysX.t.Reports,
					handler : SysX.AppManager.openModuleWindow,
					module : 'client.Report'
				}, '-', {
					text : SysX.t.Settings,
					menu : {
						items : [{
							text : SysX.t.Account,
							module : 'client.Client',
							handler : SysX.AppManager.editRecord,
							record : Ext.create('SysX.model.Client', {
								id : userHash.client_id
							})
						}, {
							text : SysX.t.User,
							handler : SysX.AppManager.addModuleGrid,
							module : 'client.ClientUser'
						}, {
							text : SysX.t.UserGroup,
							handler : SysX.AppManager.addModuleGrid,
							module : 'client.ClientUserGroup'
						}, {
							text : SysX.t.ClientCreditLimitTopUpTransaction,
							handler : SysX.AppManager.addModuleGrid,
							module : 'client.ClientCreditLimitTopUpTransaction'
						}, {
							text : SysX.t.EbayAccount,
							handler : SysX.AppManager.addModuleGrid,
							module : 'client.EbayAccount'
						}]
					}
				});

				if (isBS && false) {
					items.push('-', {
						text : SysX.t.TopUp,
						handler : function() {
							Ext.create('Ext.window.Window', {
								autoShow : true,
								width : 300,
								height : 150,
								title : SysX.t.TopUp,
								items : {
									xtype : 'panel',
									layout : 'form',
									items : [{
										xtype : 'box',
										html : SysX.t.TopUpMessage_Default
									}, {
										xtype : 'textfield',
										fieldLabel : SysX.t.Amount,
										name : 'amount',
										value : 0.00
									}],
									bbar : {
										items : [{
											xtype : 'button',
											text : SysX.t.Pay,
											handler : function(button) {
												var amount = button.up('window').down('textfield[name="amount"]').getValue();
												if (amount <= 0) {
													return Ext.Msg.alert(SysX.t.MessageBoxTitle_Notice, SysX.t.TopUpMessage_CorrectValueNeeded);
												}
												var paypalPaymentForm = Ext.create('Ext.form.Panel', {
													id : 'paypalPaymentForm',
													method : "POST",
													standardSubmit : true,
													url : "https://www.paypal.com/cgi-bin/webscr",
													items : [{
														xtype : 'hidden',
														name : 'add',
														value : '1'
													}, {
														xtype : 'hidden',
														name : 'cmd',
														value : '_xclick'
													}, {
														xtype : 'hidden',
														name : 'business',
														value : paypalEmail
													}, {
														xtype : 'hidden',
														name : 'item_name',
														value : 'System Top-up'
													}, {
														xtype : 'hidden',
														name : 'amount',
														value : amount
													}, {
														xtype : 'hidden',
														name : 'no_shipping',
														value : '2'
													}, {
														xtype : 'hidden',
														name : 'no_note',
														value : '1'
													}, {
														xtype : 'hidden',
														name : 'currency_code',
														value : 'GBP'
													}, {
														xtype : 'hidden',
														name : 'custom',
														value : userHash.client_id
													}, {
														xtype : 'hidden',
														name : 'notify_url',
														value : paypalNotificationUrl
													}]
												});
												paypalPaymentForm.getForm().submit({
													target : '_blank'
												});
												button.up('window').destroy();
											}
										}]
									}
								}
							});
						}
					});
				}

				items.push('->', {
					text : SysX.t.Logout,
					handler : function() {
						Ext.Ajax.request({
							url : modulePath + '/public/logout',
							success : function(response) {
								location.reload(false);
							}
						});
					}
				});

				SysX.view.Viewport.override({
					toolbarItems : items,
					menuPosition : menuPosition || 'top'
				});
			}(userHash));

		viewport = Ext.create('SysX.view.Viewport', isBS ? {
			homePanel : Ext.create('SysX.view.client.home.HomePanel')
		} : {});

		// Ext.Ajax.request({
		// url : modulePath + '/Client',
		// method : 'GET',
		// scope : this,
		// success : function(response, opts) {
		// var clientInfo = Ext.JSON.decode(response.responseText).data[0], consignmentSummary = Ext.JSON.decode(response.responseText).consignment_summary, ConsignmentModel =
		// Ext.create('SysX.model.Consignment'), consignmentStatus = ConsignmentModel.status;
		// var html = [userHash.first_name, ' ', userHash.last_name, '<br />', SysX.t.ClientID, ': ', clientInfo.id, '<br />', SysX.t.Balance, ': ', (clientInfo.balance || 0.00), '<br /><br />',
		// SysX.t.ConsignmentSummaryInLastSevenDays, '<br />'].join('');
		//
		// for (var i = 0, length = consignmentSummary.length; i < length; i++) {
		// html += consignmentStatus[consignmentSummary[i]['status']][1] + ' : ' + consignmentSummary[i]['cnt'] + '<br />';
		// }
		//
		// var clientInfoWindow = Ext.create('Ext.window.Window', {
		// title : SysX.t.Account,
		// width : 300,
		// height : 200,
		// modal : true,
		// items : [{
		// xtype : 'box',
		// style : 'padding:15px; ',
		// html : html
		// }]
		// });
		// clientInfoWindow.show();
		// }
		// });
	}
});

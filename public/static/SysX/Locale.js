Ext.define("SysX.Locale", {
	//	override : "SysX.Locale",
	//Global words
	ID : 'ID',
	YES : 'Yes',
	NO : 'No',

	Name : 'Name',
	Group : 'Group',
	Internal : 'Internal',
	External : 'External',
	Status : 'Status',
	FileUpload : 'File Upload',
	Insert : 'Insert',
	Delete : 'Delete',
	Cancel : 'Cancel',
	Save : 'Save',
	Upload : 'Upload',
	Print : 'Print',
	Invoice : 'Invoice',
	Check : 'Check',
	Time : 'Time',
	Type : 'Type',
	Loading : 'Loading...',
	Initializing : 'Initializing...',
	Enter : 'Enter',
	Days : 'Days',
	Generate : 'Generate',
	Download : 'Download',
	Total : 'Total',
	From : 'From',
	To : 'To',
	User : 'User',
	Login : 'Login',
	Logout : 'Logout',
	Browse : 'Browse',
	Fee : 'Fee',
	File : 'File',
	Pay : 'Pay',
	Connect : 'Connect',
	Title : 'Title',
	Date : 'Date',
	Progress : 'Progress',
	Language : 'Language',
	Volume : 'Volume',
	Pallet : 'Pallet',
	DownloadTemplateFile : 'Download template file',
	BatchUpload : 'Batch Upload',
	RecordsPerPage : 'Records per page',
	Submit : 'Submit',
	Staff : 'Staff',
	Action : 'Action',
	LoggedInAs : 'Logged in as : ',
	Confirm : 'Confirm',
	ConfirmDelete : 'Are you sure to delete these records?',
	Url : 'Url',
	View : 'View',

	FileUploadMessage_Success : 'File uploaded successfully!',
	FileUploadMessage_Fail : 'File upload failed!',

	//Status
	Status_ACTIVE : 'Acitve',
	Status_INACTIVE : 'Inactive',
	Status_DELETED : 'Deleted',
	Status_PREPARING : 'Preparing',
	Status_DISPATCHED : 'Dispatched',
	Status_REVIEWING : 'Reviewing',
	Status_FINISHED : 'Finished',
	Status_PENDING : 'Pending',
	Status_PROCESSING : 'Processing',
	Status_PICKING : 'Picking',
	Status_OUT_OF_STOCK : 'Out of stock',
	Status_DEPRECATED : 'Deprecated',
	Status_USED : 'Used',
	Status_CREATED : 'Created',
	Status_DOWNLOADED : 'Downloaded',
	Status_PROBLEM : 'Problem',
	Status_OPEN : 'Open',
	Status_CLOSED : 'Closed',
	Status_RECEIVED : 'Received',
	Status_STOCKED : 'Stocked',
	Status_CANCELLED : 'Cancelled',
	Status_DORMANT : 'Dormant',
	Status_CLEARING : 'Clearing',
	Status_NEW : 'New',
	Status_ENQUIRING : 'Enquiring',
	Status_INSTOCK : 'In-stock',
	Status_SOLD : 'Sold',

	//Message Box Title
	MessageBoxTitle_Notice : 'Notice',
	MessageBoxTitle_Alert : 'Alert',
	MessageBoxTitle_Error : 'Error',
	MessageBoxTitle_Success : 'Success',

	//Global Menu
	Clients : 'Clients',
	Tickets : 'Tickets',
	Reports : 'Reports',
	Settings : 'Settings',
	ClientGroup : 'Client Group',
	UserManagement : 'User Management',
	Users : 'Users',
	UserGroup : 'User Group',
	Misc : 'Misc',
	Country : 'Country',
	Account : 'Account',

	BasicInfo : 'Basic Info',
	BasicDetails : 'Basic Details',
	ConsignmentSettings : 'Consignment Settings',
	Accounting : 'Accounting',

	//Client
	Client_IsInternal : 'Internal User',
	Client_IsExternal : 'External User',
	IsVip : 'VIP',
	CompanyName : 'Company Name',
	Email : 'Email',
	Telephone : 'Telephone',
	AddressLine1 : 'Line 1',
	AddressLine2 : 'Line 2',
	AddressLine3 : 'Line 3',
	City : 'City',
	County : 'County/Province',
	PostCode : 'Post Code',
	CountryISO : 'Country',
	CreditLimit : 'Credit Limit',
	CreateTime : 'Create Time',
	NextChargingDate : 'Next charging date',
	Note : 'Note',
	BulkOrderReferenceField : 'Bulk Order Reference Field',
	Balance : 'Balance',
	ClientGroupName : 'Client Group',
	ConsignmentSummaryInLastSevenDays : 'Consignment summary in last 7 days',

	//ClientStatement
	ClientStatement : 'Statement',
	Amount : 'Amount',
	UpdateTime : 'Update Time',
	AddClientTransferRecord : 'Add Client Transfer Record',

	//ClientStatementType
	ClientStatementType_CONSIGNMENT_FEE : 'Consignment Fee',
	ClientStatementType_WAREHOUSE_DISPATCH_FEE : 'Warehouse Dispatch Fee',
	ClientStatementType_REFUND : 'Refund',
	ClientStatementType_MEMBERSHIP_FEE : 'Membership Fee',
	ClientStatementType_ADJUSTMENT : 'Adjustment',
	ClientStatementType_TOP_UP : 'Top Up',
	ClientStatementType_CLIENT_TRANSFER : 'Client Transfer',

	//ClientUser
	//CompanyUser
	CompanyUser : 'Company User',
	ClientUser : 'Client User',
	Username : 'Username',
	Password : 'Password',
	ReEnterPassword : 'Re-enter password',
	ApiKey : 'API Key',
	FirstName : 'First Name',
	LastName : 'Last Name',
	IsManager : 'Is Manager',
	ClientUserGroupName : 'Client User Group Name',
	CompanyUserGroupName : 'Company User Group Name',
	ClientUserGroup : 'Client User Group',
	CompanyUserGroup : 'Company User Group',
	AdvancedLogin : 'Advanced login',
	LoginAsThisUser : 'Login to Client as this user',
	ClientUserMessage_NoAPIKey : "You don't have an API Key yet, click the button below to get a new key.",
	GenerateNewAPIKey : 'Generate a new API Key',
	CompanyUserMessage_NoAPIKey : "You don't have an API Key yet, click the button below to get a new key.",
	ReturnAddress : 'Return Address',

	//ClientUserGroupPrivilege
	//CompanyUserGroupPrivilege
	UserGroupPrivilege : 'User Group Privilege',
	PrivilegeCode : 'Privilege Code',
	PrivilegeDescription : 'Privilege Description',
	IsDenied : 'Denied',
	IsAllowed : 'Allowed',

	//Country
	PrintableName : 'Printable Name',
	ISO : 'ISO Code',
	ISO3 : 'ISO 3 Code',
	Numcode : 'Numcode',

	//Ticket
	Subject : 'Subject',
	UserName : 'UserName',
	TicketMessage_StatusUpdateSuccess : 'Ticket status updated!',
	TicketMessage_StatusUpdateFailed : 'Error happened while updating status, please retry.',

	//User
	PleaseLogin : 'Please login',
	User_SessionExpired : 'Your session has timed out, you will be redirected to re-login.',

	//Vendor
	Vendor : 'Vendor',

	//Listing
	Description : 'Description',
	Listing : 'Listing',
	Cost : 'Cost',
	Price : 'Price',
	DeliveryFee : 'Delivery Fee',
	RecommendedPrice : 'Recommended Price',

	//HomePanel
	HomePanel : 'Home',
	LastLoginTime : 'Last login time',
	InLastXDays : ' (in last 7 days)',
	AutoReload : 'Auto Reload',

	//Config
	Config : 'Configuration',

	LAST : 'Last'
});

SysX.Locale.locales = {
	'en' : ['en', 'English'],
	'zh_CN' : ['zh_CN', '简体中文']
};

SysX.t = Ext.create('SysX.Locale');

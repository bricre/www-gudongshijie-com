Ext.define('SysX.view.TreeMenu', {
	layout : 'accordion',
	extend : 'Ext.panel.Panel',
	requires : ['Ext.tree.Panel', 'Ext.layout.container.Accordion', 'SysX.store.TreeStore.Menu'],
	defaults : {
		collapseFirst : true
	},
	items : []
});

Ext.define('SysX.view.Viewport', {
	extend : 'Ext.container.Viewport',
	itemId : 'admin.viewport',

	id : 'AppWindow',
	layout : 'fit',
	requires : ['SysX.view.TreeMenu'],

	toolbarItems : [],
	menuPosition : 'top',

	initComponent : function() {
		var homePanel = this.homePanel || Ext.create('Ext.panel.Panel', {
			title : this.homeText,
			html : Ext.getDom('HomePanelContent').innerHTML
		});

		homePanel.addDocked({
			dock : 'bottom',
			xtype : 'toolbar',
			enableOverflow : true,
			items : [{
				xtype : 'tbtext',
				text : SysX.t.LoggedInAs + userHash.first_name + ' ' + userHash.last_name + ' (' + userHash.username + ')'
			}, '->', {
				name : 'locale',
				fieldLabel : SysX.t.Language,
				labelAlign : 'right',
				allowBlank : false,
				xtype : 'combo',
				valueField : 'value',
				displayField : 'text',
				mode : 'local',
				forceSelection : true,
				triggerAction : 'all',
				editable : false,
				value : locale, //Global Variable defined in index.phtml
				store : Ext.Object.getValues(SysX.Locale.locales),
				listeners : {
					change : function(combo, newValue, oldValue) {
						Ext.Ajax.request({
							url : modulePath + '/Public/Change-Locale',
							method : 'GET',
							params : {
								locale : newValue
							},
							success : function(response, opts) {
								location.reload(false);
							}
						});
					}
				}
			}]
		});

		this.items = {
			layout : {
				type : 'fit'
			},
			items : [{
				id : 'mainPanel',
				xtype : 'tabpanel',
				activeTab : 0,
				items : [homePanel]
			}]
		};

		switch(this.menuPosition) {
			case 'top':
				this.createToolbarMenu(this.menuPosition);
				break;
			case 'bottom':
				this.createToolbarMenu(this.menuPosition);
				break;
			case 'left':
				this.createTreeMenu(this.menuPosition);
				break;
			case 'right':
				this.createTreeMenu(this.menuPosition);
				break;
			default:
				this.createToolbarMenu('top');
				break;
		}

		this.callParent(arguments);
	},
	createToolbarMenu : function(menuPosition) {
		this.items.dockedItems = [{
			dock : menuPosition,
			xtype : 'toolbar',
			items : this.toolbarItems,
			enableOverflow : true,
			defaults : {
				handler : SysX.AppManager.addModuleGrid
			}
		}];
	},

	createTreeMenu : function(menuPosition) {
		var mainPanel = this.items.items.pop();
		mainPanel.region = 'center';

		this.layout = 'border';
		delete this.items.layout;
		this.items = [Ext.create('SysX.view.TreeMenu', {
			region : 'left' === menuPosition ? 'west' : 'east',
			items : this.convertToolbarToTree(this.toolbarItems, {
				handler : SysX.AppManager.addModuleGrid
			}, false),
			collapsible : true,
			width : 150
		}), mainPanel];
	},
	convertToolbarToTree : function(toolbarItems, defaults, isSubMenu) {
		var items = [], item;
		if (undefined === defaults) {
			defaults = {};
		}
		if (undefined === isSubMenu) {
			isSubMenu = false;
		}
		for (var i = 0, j = toolbarItems.length; i < j; i++) {
			item = toolbarItems[i];
			if (Ext.Array.contains([' ', '-', '->'], item)) {
				continue;
			}
			item = Ext.applyIf(item, defaults);

			if (false === isSubMenu) {
				items.push(Ext.create('Ext.tree.Panel', {
					title : item.text,
					rootVisible : false,
					store : Ext.create('SysX.store.TreeStore.Menu', {
						root : {
							expanded : true,
							children : item.menu ? this.convertToolbarToTree(item.menu.items, item.menu.defaults, true) : [Ext.Object.merge(item, {
								leaf : true
							})]
						}
					}),
					listeners : {
						itemclick : function(tree, node) {
							node.get('handler')(node.data);
						}
					}
				}));
			} else {
				item.title = item.text;
				if (item.menu) {
					item.children = this.convertToolbarToTree(item.menu.items, item.menu.defaults, true);
				} else {
					item.leaf = true;
				}
				items.push(item);
			}
		}
		return items;
	}
});

Ext.define("Ext.view.AbstractView.LoadMask", {
	override : "Ext.view.AbstractView",
	onRender : function() {
		this.callParent();
		if (this.loadMask && Ext.isObject(this.store)) {
			this.setMaskBind(this.store);
		}
	}
});

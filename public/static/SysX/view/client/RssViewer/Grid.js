Ext.require('SysX.model.RssRecord');
Ext.define('SysX.view.client.RssViewer.Grid', {
	extend : 'Ext.grid.Panel',
	columns : [{
		header : SysX.t.Title,
		dataIndex : 'title',
		flex : 5
	}, {
		header : SysX.t.Date,
		dataIndex : 'pubDate',
		flex : 1
	}],
	initComponent : function() {
		this.store = new Ext.data.XmlStore({
			model : SysX.model.RssRecord,
			proxy : {
				type : 'ajax',
				noCache : false,
				url : '/feed-proxy.php',
				extraParams : {
					feed : this.feed
				},
				reader : Ext.create('Ext.data.reader.Xml', {
					record : 'item',
					idProperty : 'guid',
					successProperty : 'title',
					root : 'channel'
				})
			},
			autoLoad : true
		});
		this.callParent();
	},
	feed : 'http://www.birdsystem.co.uk/blog/feed/'
});

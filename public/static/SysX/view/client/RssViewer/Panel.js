Ext.define('SysX.view.client.RssViewer.Panel', {
	extend : 'Ext.panel.Panel',
	layout : {
		type : 'border'
	},
	title : SysX.t.HomePanel_LatestNews,
	initComponent : function() {
		var listGrid = Ext.create('SysX.view.client.RssViewer.Grid', {
			region : 'north',
			itemId : 'listGrid',
			height : 100,
			split : true,
			listeners : {
				select : function(rowModel, record) {
					listGrid.nextSibling('container[itemId=detailPanel]').update(record.data);
				}
			}
		});
		listGrid.getStore().on('load', function(store, record) {
			listGrid.getSelectionModel().select(0);
		});
		this.items = [listGrid, {
			xtype : 'container',
			region : 'center',
			itemId : 'detailPanel',
			autoScroll : true,
			anchor : '100%',
			tpl : SysX.t.HomePanel_PanelTpl
		}];
		this.callParent();
	}
});

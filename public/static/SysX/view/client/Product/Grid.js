Ext.define('SysX.view.client.Product.Grid', {
	extend : 'SysX.view.abstract.Grid',
	itemId : 'client.Product.Grid',
	store : 'SysX.store.Product',
	title : SysX.t.Product,
	columns : [{
		header : SysX.t.ID,
		dataIndex : 'id',
		sortable : true,
		flex : 1,
		filter : true
	}, {
		header : SysX.t.Name,
		dataIndex : 'name',
		sortable : true,
		filter : true,
		flex : 3
	}, {
		header : SysX.t.Note,
		dataIndex : 'note',
		filter : true,
		flex : 2
	}, {
		header : SysX.t.ClientRef,
		dataIndex : 'client_ref',
		sortable : true,
		filter : true,
		flex : 2
	}, {
		header : SysX.t.CommodityCode,
		dataIndex : 'commodity_code',
		sortable : true,
		filter : true,
		flex : 2
	}, {
		header : SysX.t.Price,
		dataIndex : 'price',
		sortable : true,
		align : 'right',
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			return Ext.util.Format.number(value, '0,0.00');
		},
		flex : 1,
		filter : true
	}, {
		header : SysX.t.ProductDimension,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			return record.data.length + '*' + record.data.width + '*' + record.data.depth;
		},
		flex : 1.5
	}, {
		header : SysX.t.Weight,
		dataIndex : 'weight',
		sortable : true,
		align : 'right',
		flex : 1
	}, {
		header : SysX.t.LiveStock,
		dataIndex : 'live_stock',
		sortable : true,
		align : 'right',
		flex : 0.5,
		filter : true

	}, {
		header : SysX.t.LowStockLevel,
		dataIndex : 'low_stock_level',
		sortable : true,
		align : 'right',
		flex : 0.5,
		filter : true,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			if (value > 0 && value >= record.data.live_stock) {
				metaData.tdAttr = 'style="color:red"';
			}
			return value;
		}
	}, {
		header : SysX.t.LastInStockTime,
		dataIndex : 'last_in_stock_time',
		sortable : true,
		alignt : 'right',
		filter : {
			type : 'date',
			phpMode : true,
			dateFormat : 'Y-m-d'
		},
		flex : 1
	}, {
		header : SysX.t.LastOutStockTime,
		dataIndex : 'last_out_stock_time',
		sortable : true,
		alignt : 'right',
		filter : {
			type : 'date',
			phpMode : true,
			dateFormat : 'Y-m-d'
		},
		flex : 1
	}, {
		header : SysX.t.Status,
		dataIndex : 'status',
		sortable : true,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			var status = SysX.model.Product.prototype.status;
			metaData.tdAttr = status[value][2];
			return status[value][1];
		},
		filter : {
			type : 'list',
			options : Ext.Object.getValues(Ext.create('SysX.model.Product').status),
			active : true,
			value : ['ACTIVE']
		},
		flex : 1
	}, {
		dataIndex : 'length',
		hidden : true
	}, {
		dataIndex : 'width',
		hidden : true
	}, {
		dataIndex : 'depth',
		hidden : true
	}],
	defaultSorters : [{
		dataIndex : 'id',
		order : 'DESC'
	}],
	topToolbarExtraItems : [{
		xtype : 'button',
		text : SysX.t.Download,
		hrefTarget : '_blank',
		href : modulePath + '/Export/Product/',
		listeners : {
			'click' : function(button, e) {
				var records = button.up('grid').getSelectionModel().getSelection();
				if (records.length < 1) {
					Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, SysX.t.SelectRecordFirst);
					e.preventDefault();
					return false;
				}

				var selectedIds = [];
				for (var i = records.length - 1; i >= 0; i--) {
					selectedIds.push(records.shift().data.id);
				}

				button.setParams({
					ids : selectedIds.join('_'),
					toDownload : true
				});
			}
		},
		scope : this
	}, {
		xtype : 'button',
		text : SysX.t.BatchUpload,
		handler : function(button) {
			Ext.create('SysX.view.abstract.UploadWindow', {
				title : SysX.t.BatchUpload,
				url : modulePath + '/Upload/' + 'Product',
				grid : button.up('gridpanel'),
				extraItems : [{
					xtype : 'button',
					text : SysX.t.DownloadTemplateFile,
					href : '/download/' + 'ProductUploadTemplate.xlsx',
					hrefTarget : '_blank'
				}]
			});

		}
	}]
});

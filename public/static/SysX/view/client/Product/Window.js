Ext.define('SysX.view.client.Product.Window', {
	extend : 'SysX.view.abstract.Window',
	itemId : 'client.Product.Window',
	initComponent : function() {
		this.formItems = {
			title : SysX.t.BasicInfo,
			items : [{
				title : SysX.t.ProductDetails,
				collapsible : true,
				layout : 'column',
				defaults : {
					columnWidth : 0.5,
					border : false,
					bodyStyle : 'padding:5px'
				},
				items : [{
					// Fieldsets
					defaults : {
						xtype : 'fieldset',
						layout : 'form',
						autoHeight : true
					},
					items : [{
						title : SysX.t.BasicDetails,
						defaults : {
							xtype : 'textfield'
						},
						items : [{
							name : 'id',
							xtype : 'hidden'
						}, {
							itemId : 'clientProductBarcode',
							fieldLabel : SysX.t.Barcode,
							xtype : 'displayfield',
							tpl : new Ext.XTemplate('<tpl for=".">' + '<a href="{src}" target="_blank"><img src="{src}" title="Barcode-{id}" width="70" height="35" /></a>' + '</tpl>')
						}, {
							name : 'client_id',
							id : 'ClientProductApp_client_id',
							xtype : 'hidden'
						}, {
							name : 'name',
							fieldLabel : SysX.t.Name,
							allowBlank : false
						}, {
							name : 'name_customs',
							fieldLabel : SysX.t.NameCustoms,
							allowBlank : false
						}, {
							name : 'note',
							fieldLabel : SysX.t.Note
						}, {
							name : 'client_ref',
							fieldLabel : SysX.t.ClientRef
						}, {
							name : 'commodity_code',
							fieldLabel : SysX.t.CommodityCode
						}]
					}, {
						title : SysX.t.WeightAndDimension,
						defaults : {
							xtype : 'textfield'
						},
						items : [{
							name : 'weight',
							fieldLabel : SysX.t.Weight,
							xtype : 'displayfield'
						}, {
							name : 'length',
							fieldLabel : SysX.t.Length,
							xtype : 'displayfield'
						}, {
							name : 'width',
							fieldLabel : SysX.t.Width,
							xtype : 'displayfield'
						}, {
							name : 'depth',
							fieldLabel : SysX.t.Depth,
							xtype : 'displayfield'
						}]
					}, {
						title : SysX.t.SharedProduct,
						defaults : {
							xtype : 'textfield'
						},
						items : [{
							xtype : 'fieldcontainer',
							items : [{
								name : 'is_shared_internal',
								fieldLabel : SysX.t.IsSharedInternal,
								xtype : 'checkbox'
							}, {
								name : 'is_vip',
								fieldLabel : SysX.t.IsVip,
								xtype : 'checkbox',
								disabled : true
							}]
						}]
					}]
				}, {
					// Fieldsets
					defaults : {
						xtype : 'fieldset',
						layout : 'form',
						anchor : '100%',
						autoHeight : true
					},
					items : [{
						title : SysX.t.PriceAndStock,
						defaults : {
							xtype : 'textfield'
						},
						items : [{
							name : 'price',
							fieldLabel : SysX.t.Price
						}, {
							name : 'cost',
							fieldLabel : SysX.t.Cost
						}, {
							name : 'price_customs_import',
							fieldLabel : SysX.t.PriceCustomsImport
						}, {
							name : 'price_customs_export',
							fieldLabel : SysX.t.PriceCustomsExport
						}, {
							name : 'handling_fee',
							fieldLabel : SysX.t.HandlingFee,
							xtype : 'displayfield'
						}, {
							name : 'pending_stock',
							fieldLabel : SysX.t.PendingStock,
							xtype : 'displayfield'
						}, {
							name : 'live_stock',
							fieldLabel : SysX.t.LiveStock,
							xtype : 'displayfield'
						}, {
							name : 'low_stock_level',
							fieldLabel : SysX.t.LowStockLevel
						}, {
							xtype : 'fieldcontainer',
							fieldLabel : SysX.t.InventoryTime,
							layout : 'hbox',
							anchor : '100%',
							items : [{
								name : 'inventory_time',
								xtype : 'textfield',
								flex : 1
							}, {
								xtype : 'displayfield',
								value : 'Days',
								flex : 1
							}]
						}, {
							xtype : 'displayfield',
							name : 'last_stock_update_time',
							fieldLabel : SysX.t.LastStockUpdateTime
						}, {
							xtype : 'displayfield',
							name : 'last_in_stock_time',
							fieldLabel : SysX.t.LastInStockTime
						}, {
							xtype : 'displayfield',
							name : 'last_out_stock_time',
							fieldLabel : SysX.t.LastOutStockTime
						}, {
							name : 'update_time',
							fieldLabel : SysX.t.LastUpdate,
							xtype : 'displayfield',
							anchor : '100%',
							value : 'None'
						}, {
							name : 'status',
							fieldLabel : SysX.t.Status,
							allowBlank : false,
							xtype : 'combo',
							width : 100,
							id : 'clientProductStatus',
							valueField : 'value',
							displayField : 'text',
							queryMode : 'local',
							forceSelection : true,
							triggerAction : 'all',
							editable : false,
							value : 'ACTIVE',
							store : Ext.Object.getValues(Ext.create('SysX.model.Product').status)
						}]
					}]
				}]
			}, {
				title : SysX.t.Description,
				layout : 'fit',
				collapsible : true,
				height : 500,
				items : [{
					name : 'description',
					xtype : 'htmleditor',
					hideLabel : true,
					anchor : '100% 100%'
				}]
			}]
		};

		this.callParent(arguments);
	}
});

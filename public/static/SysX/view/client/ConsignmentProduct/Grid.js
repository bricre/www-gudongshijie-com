Ext.define('SysX.view.client.ConsignmentProduct.Grid', {
	extend : 'SysX.view.abstract.Grid',
	itemId : 'client.ConsignmentProduct.Grid',
	store : 'SysX.store.ConsignmentProduct',
	title : SysX.t.ConsignmentProduct,
	columns : [{
		dataIndex : 'id',
		hidden : true,
		flex : 1
	}, {
		header : SysX.t.Product,
		dataIndex : 'product_id',
		sortable : true,
		flex : 1
	}, {
		header : SysX.t.ClientRef,
		dataIndex : 'client_ref',
		sortable : true,
		excludeFromSelect : true,
		flex : 1
	}, {
		header : SysX.t.CompanyRef,
		dataIndex : 'company_ref',
		sortable : true,
		excludeFromSelect : true,
		flex : 1
	}, {
		header : SysX.t.Name,
		dataIndex : 'product_name',
		excludeFromSelect : true,
		flex : 3
	}, {
		header : SysX.t.UnitProductPrice,
		dataIndex : 'unit_product_price',
		align : 'right',
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			return Ext.util.Format.number(value, '0,0.00');
		},
		flex : 1
	}, {
		header : SysX.t.Quantity,
		dataIndex : 'quantity',
		align : 'right',
		flex : 1
	}]
});

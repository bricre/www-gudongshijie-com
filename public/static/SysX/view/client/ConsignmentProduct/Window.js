Ext.define('SysX.view.client.ConsignmentProduct.Window', {
	extend : 'SysX.view.abstract.Window',
	itemId : 'client.ConsignmentProduct.Window',
	width : 500,
	height : 250,
	initComponent : function() {
		this.formItems = {
			title : SysX.t.ConsignmentProduct,
			layout : 'form',
			anchor : '100%',
			autoHeight : true,
			defaults : {
				xtype : 'textfield',
				anchor : '100%',
				bodyStyle : 'padding:5px'
			},
			items : [{
				name : 'id',
				xtype : 'hidden'
			}, {
				name : 'consignment_id',
				xtype : 'hidden'
			}, {
				name : 'product_id',
				xtype : 'hidden',
				itemId : 'ConsignmentItemProductId'
			}, {
				fieldLabel : SysX.t.Name,
				allowBlank : false,
				xtype : 'combo',
				itemId : 'ConsignmentItemNameCombo',
				submitValue : false,
				pageSize : 10,
				valueField : 'id',
				displayField : 'name',
				mode : 'remote',
				forceSelection : true,
				triggerAction : 'all',
				listeners : {
					'select' : function(combo, record, options) {
						combo.up('window').down('hidden[itemId=ConsignmentItemProductId]').setRawValue(record[0].data.id);
						combo.up('window').down('combo[itemId=ConsignmentItemClientRefCombo]').setRawValue(record[0].data.client_ref);
						combo.up('window').down('combo[itemId=ConsignmentItemCompanyRefCombo]').setRawValue(record[0].data.company_ref);
					},
					'keypress' : {
						buffer : 100,
						fn : function() {
							if (!this.getRawValue()) {
								this.doQuery('', true);
							}
						}
					}
				},
				store : Ext.create('SysX.store.Product', {
					proxy : {
						url : modulePath + '/Product',
						extraParams : {
							field : 'name',
							selectedFields : 'id,name,client_ref,company_ref'
						},
						type : 'rest',
						reader : {
							type : 'json',
							root : 'data'
						}
					}
				})
			}, {
				fieldLabel : SysX.t.ClientRef,
				allowBlank : true,
				xtype : 'combo',
				itemId : 'ConsignmentItemClientRefCombo',
				submitValue : false,
				pageSize : 10,
				valueField : 'id',
				displayField : 'client_ref',
				mode : 'remote',
				forceSelection : true,
				triggerAction : 'all',
				listeners : {
					'select' : function(combo, record, options) {
						combo.up('window').down('hidden[itemId=ConsignmentItemProductId]').setRawValue(record[0].data.id);
						combo.up('window').down('combo[itemId=ConsignmentItemNameCombo]').setRawValue(record[0].data.name);
						combo.up('window').down('combo[itemId=ConsignmentItemCompanyRefCombo]').setRawValue(record[0].data.company_ref);
					},
					'keypress' : {
						buffer : 100,
						fn : function() {
							if (!this.getRawValue()) {
								this.doQuery('', true);
							}
						}
					}
				},
				store : Ext.create('SysX.store.Product', {
					proxy : {
						url : modulePath + '/Product',
						extraParams : {
							field : 'client_ref',
							selectedFields : 'id,name,client_ref,company_ref'
						},
						type : 'rest',
						reader : {
							type : 'json',
							root : 'data'
						}
					}
				})
			}, {
				fieldLabel : SysX.t.CompanyRef,
				allowBlank : true,
				xtype : 'combo',
				itemId : 'ConsignmentItemCompanyRefCombo',
				submitValue : false,
				pageSize : 10,
				valueField : 'id',
				displayField : 'company_ref',
				mode : 'remote',
				forceSelection : true,
				triggerAction : 'all',
				listeners : {
					'select' : function(combo, record, options) {
						combo.up('window').down('hidden[itemId=ConsignmentItemProductId]').setRawValue(record[0].data.id);
						combo.up('window').down('combo[itemId=ConsignmentItemNameCombo]').setRawValue(record[0].data.name);
						combo.up('window').down('combo[itemId=ConsignmentItemClientRefCombo]').setRawValue(record[0].data.client_ref);
					},
					'keypress' : {
						buffer : 100,
						fn : function() {
							if (!this.getRawValue()) {
								this.doQuery('', true);
							}
						}
					}
				},
				store : Ext.create('SysX.store.Product', {
					proxy : {
						url : modulePath + '/Product',
						extraParams : {
							field : 'company_ref',
							selectedFields : 'id,name,client_ref,company_ref'
						},
						type : 'rest',
						reader : {
							type : 'json',
							root : 'data'
						}
					}
				})
			}, {
				name : 'quantity',
				fieldLabel : SysX.t.Quantity,
				anchor : '50%',
				xtype : 'numberfield'
			}]
		};

		this.callParent(arguments);
	}
});

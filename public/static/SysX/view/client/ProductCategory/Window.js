Ext.define('SysX.view.client.ProductCategory.Window', {
	extend : 'SysX.view.abstract.Window',
	itemId : 'client.ProductCategory.Window',
	initComponent : function() {
		this.formItems = {};

		this.callParent(arguments);
	}
}); 
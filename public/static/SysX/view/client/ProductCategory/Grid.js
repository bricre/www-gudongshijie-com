Ext.define('SysX.view.client.ProductCategory.Grid', {
	extend : 'Ext.tree.Panel',
	itemId : 'client.ProductCategory.Grid',
	title : 'Product Category',
	columns : [],
	disableTopToolbar : true,
	rootVisible : true,
	withCheckBox : false,
	viewConfig : {
		forceFit : true
	},
	layout : 'fit',
	loadMask : true,
	useArrows : true,
	root : {
		nodeType : 'async',
		id : '0',
		text : 'Root'
	},
	rootVisible : false,
	initComponent : function() {
		this.callParent(arguments);
		this.loader = Ext.create('Ext.ComponentLoader', {
			url : this.getStore().getProxy().url,
			requestMethod : 'GET',
			baseParams : {
				withCheckBox : this.withCheckBox !== undefined ? this.withCheckBox : false
			}
		});
	},
	listener : {
		render : function() {
			this.expandAll();

			this.on('containercontextMenu', function(view, node, e) {
				var myContextMenu = new Ext.menu.Menu({
					shadow : 'frame',
					items : [{
						iconCls : "button-add",
						text : "Add Sub-Category",
						scope : this,
						handler : function() {
							myContextMenu.hide();
							Ext.MessageBox.prompt("Add Sub-Category", "Name : ", function(button, text) {
								if (button == "ok") {
									if (Ext.util.Format.trim(text) != "") {
										addCategory(node, text);
									}
								}
							});
						}
					}, {
						iconCls : "button-edit",
						text : "Edit",
						handler : function() {
							myContextMenu.hide();
							Ext.MessageBox.prompt("Edit catetory", "Name : ", function(button, text) {
								if (button == "ok") {
									if (Ext.util.Format.trim(text) != "") {
										if (node.text != text) {
											editCategory(node, text);
										}
									}
								}
							}, this, false, node.text);
						}
					}, {
						iconCls : "button-delete",
						text : "Delete",
						handler : function() {
							myContextMenu.hide();
							Ext.MessageBox.confirm("Confirm Delete", "Are you sure to delete ?", function(button, text) {
								if (button == "yes") {
									removeCategory(node);
								}
							});

						}
					}]
				});

				if (node.parentNode == null) {
					myContextMenu.items.get(1).setDisabled(true);
					myContextMenu.items.get(2).setDisabled(true);
				} else {
					// if (!node.isLeaf()) {
					// 	//myContextMenu.items.itemAt(2).setDisabled(true);
					// } else {
					// 	//myContextMenu.items.itemAt(0).setDisabled(true);
					// }

					myContextMenu.items.itemAt(1).setDisabled(false);

				}
				e.preventDefault();

				node.select();
				myContextMenu.showAt(e.getPoint());
			}, this);

			var addCategory = Ext.bind(function(parentNode, text) {
				var parentId = parentNode.id;

				Ext.Ajax.request({
					url : this.url,
					method : "POST",
					params : {
						name : text,
						parent_id : parentId
					},
					success : function(response, option) {
						var response = Ext.util.JSON.decode(response.responseText);
						if (response.success) {
							createNode(parentNode, response.data, text);
						} else {
							Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, response.message);
						}
					}
				});
			}, this);

			var editCategory = Ext.bind(function(currentNode, text) {
				Ext.Ajax.request({
					url : this.url,
					method : "POST",
					params : {
						id : currentNode.id,
						name : text
					},
					success : function(response, option) {
						var response = Ext.util.JSON.decode(response.responseText);
						if (response.success) {
							resetNode(currentNode, text);
						} else {
							Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, response.message);
						}
					}
				});
			}, this);

			var removeCategory = Ext.bind(function(currentNode) {
				Ext.Ajax.request({
					url : this.url + '/' + currentNode.id,
					method : "DELETE",
					success : function(response, option) {
						var response = Ext.util.JSON.decode(response.responseText);
						if (response.success) {
							removeNode(currentNode);
						} else {
							Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, response.message);
						}
					}
				});
			}, this);

			var createNode = function(node, data, text) {
				var newNode = new Ext.create('Ext.data.NodeInterface', {
					id : data.id,
					text : text,
					draggable : false,
					leaf : true
				});

				node.appendChild(newNode);
				node.leaf = false;
				//node.setIconCls('button-tree-folder-open');
				node.expand();
			};

			var resetNode = function(node, text) {
				node.setText(text);
			};

			var removeNode = function(node) {
				var pNode = node.parentNode;
				node.remove();
				var l = pNode.childNodes.length;
				if (l == 0) {
					pNode.leaf = true;
					//pNode.setIconCls('button-tree-leaf');
				}
			};
		}
	},
	getStore : function(storeCfg) {
		if (!Ext.isObject(this.store)) {
			storeCfg = Ext.isObject(storeCfg) ? storeCfg : {};
			this.store = Ext.create(this.store, Ext.apply(storeCfg));
		}
		return this.store;
	},

	getModuleName : function() {
		return Ext.getClassName(this).replace(/^SysX\.view\./, '').replace(/\.Grid$/, '');
	}
});

Ext.define('SysX.view.client.EbayAccount.Grid', {
	extend : 'SysX.view.abstract.Grid',
	itemId : 'client.EbayAccount.Grid',
	store : 'SysX.store.EbayAccount',
	title : SysX.t.EbayAccount,
	columns : [{
		dataIndex : 'id',
		hidden : true
	}, {
		header : SysX.t.EbaySiteName,
		dataIndex : 'ebay_site_name',
		sortable : true,
		excludeFromSelect : true,
		flex : 2
	}, {
		header : SysX.t.EbayUserID,
		dataIndex : 'ebay_user_id',
		sortable : true,
		flex : 2
	}, {
		header : SysX.t.TokenExpirationTime,
		dataIndex : 'token_expiration_time',
		flex : 1
	}, {
		header : SysX.t.UpdateTime,
		dataIndex : 'update_time',
		flex : 1
	}]
});

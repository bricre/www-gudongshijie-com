Ext.define('SysX.view.client.EbayAccount.Window', {
	extend : 'Ext.window.Window',
	itemId : 'client.EbayAccount.Window',
	title : SysX.t.EbayAccount,
	width : 400,
	height : 300,
	maximizable : false,
	modal : true,
	autoShow : true,
	initComponent : function() {
		this.items = [{
			xtype : 'container',
			layout : 'fit',
			style : {
				padding : '5px'
			},
			items : [{
				xtype : 'container',
				html : SysX.t.EbayAccountMessage_ConnectPanel
			}, {
				xtype : 'container',
				layout : 'hbox',
				items : [{
					xtype : 'hidden',
					itemId : 'SessionIdHiddelField',
					value : null
				}, {
					xtype : 'combo',
					itemId : 'EbaySiteIdCombobox',
					emptyText : SysX.t.EbayAccountMessage_SelectEbaySite,
					width : 200,
					allowBlank : false,
					valueField : 'id',
					displayField : 'name',
					queryMode : 'local',
					forceSelection : true,
					triggerAction : 'all',
					store : Ext.create('SysX.store.EbaySite')
				}, {
					xtype : 'button',
					text : SysX.t.Connect,
					style : {
						paddingLeft : '5px'
					},
					scope : this,
					handler : function() {
						if (this.down('combo[itemId="EbaySiteIdCombobox"]').getValue()) {
							this.getEl().mask('Loading');
							Ext.Ajax.request({
								url : modulePath + '/Ebay-Service/Get-Sign-In-Url',
								method : 'GET',
								params : {
									site_id : this.down('combo[itemId="EbaySiteIdCombobox"]').getValue()
								},
								scope : this,
								success : function(response, opts) {
									var result = Ext.decode(response.responseText);
									if (result.success) {
										this.getEl().unmask();
										this.down('hidden[itemId="SessionIdHiddelField"]').setValue(result.data.session_id);
										SysX.openNewWindow(result.data.sign_in_url);
									}
								}
							});
						}
					}
				}]
			}]
		}];

		this.listeners = {
			scope : this,
			close : function() {
				if (this.down('hidden[itemId=SessionIdHiddelField]').getValue()) {
					Ext.Ajax.request({
						url : modulePath + '/Ebay-Account/Fetch-Token/',
						params : {
							fetch_token : true,
							site_id : this.down('combo[itemId=EbaySiteIdCombobox]').getValue(),
							session_id : this.down('hidden[itemId=SessionIdHiddelField]').getValue()
						},
						scope : this
					});

				}
			}
		};

		this.callParent(arguments);
	}
});

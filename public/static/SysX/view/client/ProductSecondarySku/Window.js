Ext.define('SysX.view.client.ProductSecondarySku.Window', {
	extend : 'SysX.view.abstract.Window',
	itemId : 'client.ProductSecondarySku.Window',
	height : 400,
	width : 400,
	singlePanel : true,
	initComponent : function() {
		this.formItems = {
			title : SysX.t.BasicDetails,
			items : [{
				name : 'id',
				xtype : 'hidden'
			}, {
				name : 'product_id',
				xtype : 'hidden'
			}, {
				name : 'product-name',
				xtype : 'displayfield',
				fieldLabel : SysX.t.ProductName
			}, {
				name : 'sku',
				xtype : 'textfield',
				anchor : '100%',
				fieldLabel : SysX.t.ProductSecondarySku
			}]
		};

		this.callParent(arguments);
	}
});

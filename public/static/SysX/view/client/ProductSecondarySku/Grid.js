Ext.define('SysX.view.client.ProductSecondarySku.Grid', {
	extend : 'SysX.view.abstract.Grid',
	itemId : 'client.ProducSecondarySkut.Grid',
	store : 'SysX.store.ProductSecondarySku',
	title : SysX.t.ProductSecondarySku,
	disableTopToolbar : true,
	columns : [{
		dataIndex : 'id',
		hidden : true
	}, {
		header : SysX.t.ProductID,
		dataIndex : 'product_id',
		sortable : true,
		filter : true,
		flex : 2
	}, {
		header : SysX.t.ProductName,
		dataIndex : 'product-name',
		filter : true,
		flex : 3,
		excludeFromSelect : true
	}, {
		header : SysX.t.ProductSecondarySku,
		dataIndex : 'sku',
		sortable : true,
		filter : true,
		flex : 2,
		editor : {
			xtype : 'textfield',
			allowBlank : false
		}
	}],
	defaultSorters : [{
		dataIndex : 'sku',
		order : 'ASC'
	}]
});

Ext.define('SysX.view.client.Ticket.Window', {
	extend : 'SysX.view.abstract.Window',
	itemId : 'client.Ticket.Window',
	initComponent : function() {
		this.formItems = {
			title : SysX.t.Ticket,

			items : [{
				name : 'id',
				xtype : 'hidden'
			}, {
				name : 'parent_id',
				xtype : 'hidden'
			}, {
				name : 'company_id',
				xtype : 'hidden'
			}, {
				name : 'company_user_id',
				xtype : 'hidden'
			}, {
				name : 'client_id',
				xtype : 'hidden'
			}, {
				name : 'client_user_id',
				xtype : 'hidden'
			}, {
				name : 'sysxtype',
				xtype : 'hidden'
			}, {
				name : 'record_id',
				xtype : 'hidden'
			}, {
				name : 'subject',
				fieldLabel : SysX.t.Subject,
				xtype : 'textfield',
				allowBlank : false,
				anchor : '100%'
			}, {
				name : 'content',
				fieldLabel : SysX.t.Note,
				xtype : 'htmleditor',
				anchor : '100%'
			}, {
				name : 'create_time',
				xtype : 'hidden',
				value : Ext.Date.format(Date(), "Y-m-d H:i:s")
			}, {
				name : 'status',
				fieldLabel : SysX.t.Status,
				allowBlank : false,
				xtype : 'combo',
				value : 'OPENED',
				valueField : 'value',
				displayField : 'text',
				queryMode : 'local',
				forceSelection : true,
				triggerAction : 'all',
				editable : false,
				store : new Ext.data.ArrayStore({
					fields : ['value', 'text', 'attr'],
					data : Ext.Object.getValues(Ext.create('SysX.model.Ticket').status)
				})
			}]
		};

		this.callParent(arguments);
	}
});

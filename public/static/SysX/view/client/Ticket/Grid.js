Ext.define('SysX.view.client.Ticket.Grid', {
	extend : 'SysX.view.abstract.Grid',
	itemId : 'client.Ticket.Grid',
	store : 'SysX.store.Ticket',
	title : SysX.t.Tickets,
	columns : [{
		header : SysX.t.ID,
		dataIndex : 'id',
		sortable : true,
		flex : 0.5,
		filter : true
	}, {
		header : SysX.t.User,
		dataIndex : 'user_name',
		excludeFromSelect : true,
		flex : 0.5
	}, {
		header : SysX.t.Subject,
		dataIndex : 'subject',
		flex : 2,
		filter : true
	}, {
		header : SysX.t.CreateTime,
		dataIndex : 'create_time',
		sortable : true,
		flex : 1,
		filter : {
			type : 'date',
			phpMode : true,
			dateFormat : 'Y-m-d',
			active : true,
			value : {
				after : Ext.Date.add(new Date(), Ext.Date.MONTH, -1)
			}
		}
	}, {
		header : SysX.t.Status,
		dataIndex : 'status',
		sortable : true,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			var status = SysX.model.Ticket.prototype.status;
			metaData.tdAttr = status[value][2];
			return status[value][1];
		},
		filter : {
			type : 'list',
			options : Ext.Object.getValues(Ext.create('SysX.model.Ticket').status)
		},
		flex : 1
	}, {
		dataIndex : 'sysxtype',
		hidden : true
	}, {
		dataIndex : 'record_id',
		hidden : true
	}],
	defaultSorters : [{
		dataIndex : 'id',
		order : 'DESC'
	}]
});

Ext.define('SysX.view.client.TempConsignmentInfo.Grid', {
	extend : 'SysX.view.abstract.Grid',
	itemId : 'client.TempConsignmentInfo.Grid',
	store : 'SysX.store.TempConsignmentInfo',
	title : SysX.t.BulkOrder,
	disableInsertButton : true,
	columns : [{
		header : SysX.t.ID,
		dataIndex : 'id',
		sortable : true,
		flex : 1,
		filter : true
	}, {
		header : SysX.t.DeliveryService,
		dataIndex : 'delivery_service_name',
		filter : true,
		flex : 3
	}, {
		header : SysX.t.Contact,
		dataIndex : 'contact',
		filter : true,
		flex : 1
	}, {
		header : SysX.t.ProductName,
		dataIndex : 'product_name',
		filter : true,
		flex : 3
	}, {
		header : SysX.t.SalesReference,
		dataIndex : 'sales_reference',
		filter : true,
		flex : 2
	}, {
		header : SysX.t.ConsignmentProductReference,
		dataIndex : 'product_company_ref',
		filter : true,
		flex : 2
	}, {
		header : SysX.t.PostCode,
		dataIndex : 'post_code',
		filter : true,
		flex : 1.5
	}, {
		header : SysX.t.ErrorDescription,
		dataIndex : 'error_description',
		flex : 3
	}, {
		header : SysX.t.SpecialInstruction,
		dataIndex : 'special_instruction',
		flex : 3
	}, {
		header : SysX.t.ImportTime,
		dataIndex : 'import_time',
		xtype : 'datecolumn',
		filter : {
			active : true,
			value : {
				after : Ext.Date.add(new Date(), Ext.Date.MONTH, -1)
			}
		},
		flex : 1.5
	}, {
		header : SysX.t.Status,
		dataIndex : 'status',
		sortable : true,
		flex : 1,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			var status = SysX.model.TempConsignmentInfo.prototype.status;
			metaData.tdAttr = status[value][2];
			return status[value][1];
		},
		filter : {
			type : 'list',
			options : Ext.Object.getValues(Ext.create('SysX.model.TempConsignmentInfo').status),
			active : true,
			value : ['PENDING', 'PROBLEM', 'FINISHED']
		}
	}],
	defaultSorters : [{
		dataIndex : 'status',
		order : 'ASC'
	}],
	onAfterInitComponent : function(view) {
		var topToolbar = view.down('toolbar[dock="top"]');

		topToolbar.insert(0, {
			text : SysX.t.Upload,
			menu : {
				items : [{
					xtype : 'button',
					text : SysX.t.SystemFormatFile,
					handler : function() {
						Ext.create('SysX.view.abstract.UploadWindow', {
							title : SysX.t.SystemFormatFile,
							url : modulePath + '/Upload/' + 'Ebay-Sales-History',
							grid : view,
							extraItems : [{
								xtype : 'button',
								text : SysX.t.DownloadTemplateFile,
								href : '/download/' + 'BulkOrderTemplate.xls',
								hrefTarget : '_blank'
							}]
						});
					}
				}, {
					xtype : 'button',
					text : SysX.t.EbaySalesHistoryFile,
					handler : function() {
						Ext.create('SysX.view.abstract.UploadWindow', {
							title : SysX.t.EbaySalesHistoryFile,
							url : modulePath + '/Upload/' + 'Ebay-Sales-History',
							grid : view,
							extraItems : [{
								xtype : 'button',
								text : SysX.t.DownloadTemplateFile,
								href : '/download/' + 'BulkOrderTemplate.xls',
								hrefTarget : '_blank'
							}]
						});
					}
				}, {
					xtype : 'button',
					text : SysX.t.AmazonOrderReportFile,
					handler : function() {
						Ext.create('SysX.view.abstract.UploadWindow', {
							title : SysX.t.AmazonOrderReportFile,
							url : modulePath + '/Upload/' + 'Amazon-Order-Report',
							grid : view
						});
					}
				}]
			}
		});

		topToolbar.insert(1, {
			xtype : 'button',
			text : SysX.t.Download,
			hrefTarget : '_blank',
			href : modulePath + '/Export/Temp-Consignment-Info/',
			listeners : {
				'click' : function(button, e) {
					var records = view.getSelectionModel().getSelection();
					if (records.length < 1) {
						Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, SysX.t.SelectRecordFirst);
						e.preventDefault();
						return false;
					}

					var selectedIds = [];
					for (var i = records.length - 1; i >= 0; i--) {
						selectedIds.push(records.shift().data.id);
					}

					button.setParams({
						ids : selectedIds.join('_'),
						toDownload : true
					});
				}
			},
			scope : this
		});
		
		topToolbar.insert(3, {
			xtype : 'button',
			text : SysX.t.BatchChangeStatus,
			handler : function() {
				var status = Ext.getCmp(this.id + 'BatchChangeStatusCombo').getValue();
				if ('' === status) {
					Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, SysX.t.BatchChangeStatusMessage_SelectStatusFirst);
					return false;
				}

				var records = view.getSelectionModel().getSelection();
				if (records.length < 1) {
					Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, SysX.t.BatchChangeStatusMessage_SelectConsignmentFirst);
					return false;
				}

				var selectedIds = [];
				for (var i = records.length - 1; i >= 0; i--) {
					selectedIds.push(records.shift().data.id);
				}

				Ext.Ajax.request({
					url : modulePath + '/Temp-Consignment-Info/Batch-Update-Status',
					method : 'GET',
					scope : this,
					params : {
						ids : selectedIds.join('_'),
						'status' : status
					},
					success : function(response, opts) {
						Ext.Msg.alert(SysX.t.MessageBoxTitle_Success, SysX.t.BatchChangeStatusMessage_AllConsignmentsUpdated);
						view.getStore().load();
					},
					failure : function(response, opts) {
						Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, Ext.decode(response.responseText).message);
						return false;
					}
				});
			},
			scope : this
		});

		topToolbar.insert(4, {
			xtype : 'combo',
			submitValue : false,
			id : this.id + 'BatchChangeStatusCombo',
			valueField : 'value',
			displayField : 'text',
			mode : 'local',
			forceSelection : true,
			triggerAction : 'all',
			editable : false,
			store : new Ext.data.ArrayStore({
				fields : ['value', 'text', 'attr'],
				data : Ext.Object.getValues(Ext.create('SysX.model.TempConsignmentInfo').status)
			})
		});

		topToolbar.insert(5, ['->', {
			xtype : 'button',
			text : SysX.t.Process,
			listeners : {
				click : function(button) {
					var gridPanel = button.up('gridpanel'), records = gridPanel.getSelectionModel().getSelection(), consignmentIdString = '';
					for (var i = 0; i < records.length; i++) {
						consignmentIdString += records[i].data.id + '_';
					}

					var progressBar = Ext.create('Ext.ProgressBar', {
						id : 'TempConsignmentInfoConvertProgressBar',
						text : SysX.t.Initializing
					});

					var progressWindow = Ext.create('Ext.window.Window', {
						id : 'TempConsignmentInfoConvertWindow',
						height : 100,
						width : Ext.getBody().getWidth() * 0.75,
						layout : 'fit',
						modal : true,
						title : SysX.t.Progress,
						items : [{
							xtype : 'panel',
							layout : 'fit',
							bodyStyle : 'padding:10px',
							items : [progressBar],
							bbar : {
								buttonAlign : 'right',
								items : [{
									xtype : 'button',
									text : SysX.t.Cancel
								}]
							}
						}]
					});
					progressWindow.show();
					progressBar.wait();

					Ext.Ajax.request({
						url : modulePath + '/Temp-Consignment-Info/Convert-Into-Consignment',
						method : 'GET',
						params : {
							ids : consignmentIdString
						},
						success : function(response, opts) {
							response = Ext.decode(response.responseText);
							progressBar.reset();
							progressBar.updateProgress(1);
							if (response.success) {
								Ext.Msg.alert(SysX.t.MessageBoxTitle_Success, SysX.t.TempConsignmentInfoMessage_ProcessSuccess);
							} else {
								Ext.Msg.alert(SysX.t.MessageBoxTitle_Alert, response.message);
							}
							view.down('pagingtoolbar').doRefresh();
							progressWindow.close();
						},
						failure : function(response, opts) {
							Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, SysX.t.TempConsignmentInfoMessage_ProcessFail);
						}
					});
					return true;
				}
			}
		}]);

		topToolbar.insert(7, {
			xtype : 'button',
			text : SysX.t.ChangeDeliveryService,
			listeners : {
				click : function(button) {
					var gridPanel = button.up('gridpanel'), records = gridPanel.getSelectionModel().getSelection(), consignmentIdString = '', deliveryServiceCombo = gridPanel.down('toolbar[dock=top]').down('combo[itemId=BatchChangeDeliveryServiceCombo]'), deliveryServiceName = deliveryServiceCombo.getValue();

					if (0 == records.length) {
						return Ext.Msg.alert(SysX.t.MessageBoxTitle_Notice, SysX.t.TempConsignmentInfoMessage_SelectRecord);
					}

					if (!deliveryServiceCombo.isValid()) {
						return Ext.Msg.alert(SysX.t.MessageBoxTitle_Notice, SysX.t.TempConsignmentInfoMessage_SelectDeliveryService);
					}

					for (var i = 0; i < records.length; i++) {
						consignmentIdString += records[i].data.id + '_';
					}

					var progressBar = Ext.create('Ext.ProgressBar', {
						id : 'TempConsignmentInfoConvertProgressBar',
						text : SysX.t.Initializing
					});

					var progressWindow = Ext.create('Ext.window.Window', {
						id : 'TempConsignmentInfoConvertWindow',
						height : 100,
						width : Ext.getBody().getWidth() * 0.75,
						layout : 'fit',
						modal : true,
						title : SysX.t.Progress,
						items : [{
							xtype : 'panel',
							layout : 'fit',
							bodyStyle : 'padding:10px',
							items : [progressBar],
							bbar : {
								buttonAlign : 'right',
								items : [{
									xtype : 'button',
									text : SysX.t.Cancel
								}]
							}
						}]
					});
					progressWindow.show();
					progressBar.wait();

					Ext.Ajax.request({
						url : modulePath + '/Temp-Consignment-Info/Batch-Change-Delivery-Service',
						method : 'GET',
						params : {
							ids : consignmentIdString,
							delivery_service_name : deliveryServiceName
						},
						success : function(response, opts) {
							response = Ext.decode(response.responseText);
							progressBar.reset();
							progressBar.updateProgress(1);
							if (response.success) {
								Ext.Msg.alert(SysX.t.MessageBoxTitle_Success, SysX.t.TempConsignmentInfoMessage_DeliveryServiceChanged);
							} else {
								Ext.Msg.alert(SysX.t.MessageBoxTitle_Alert, response.message);
							}
							view.down('pagingtoolbar').doRefresh();
							progressWindow.close();
						},
						failure : function(response, opts) {
							Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, SysX.t.TempConsignmentInfoMessage_ProcessFail);
						}
					});
					return true;
				}
			}
		});

		topToolbar.insert(8, {
			xtype : 'combo',
			width : 250,
			submitValue : false,
			itemId : 'BatchChangeDeliveryServiceCombo',
			valueField : 'name',
			displayField : 'name',
			mode : 'local',
			forceSelection : true,
			triggerAction : 'all',
			editable : false,
			allowBlank : false,
			store : Ext.create('SysX.store.DeliveryService', {
				filters : [{
					property : 'status',
					value : 'ACTIVE'
				}]
			})
		});
	}
});

Ext.define('SysX.view.client.TempConsignmentInfo.Window', {
	extend : 'SysX.view.abstract.Window',
	itemId : 'client.TempConsignmentInfo.Window',
	initComponent : function() {
		this.formItems = {
			id : 'TempConsignmentInfoPanel',
			title : SysX.t.BulkOrder,
			layout : 'column',
			defaults : {
				layout : 'form',
				columnWidth : 0.5,
				border : false,
				bodyStyle : 'padding:5px'
			},
			// Columns
			items : [{
				// Fieldsets
				defaults : {
					xtype : 'fieldset',
					layout : 'form',
					anchor : '100%',
					autoHeight : true
				},
				items : [{
					title : SysX.t.BasicDetails,
					defaults : {
						xtype : 'textfield',
						anchor : '50%'
					},
					items : [{
						name : 'id',
						xtype : 'hidden',
						id : 'ConsignmentID'
					}, {
						name : 'client_id',
						xtype : 'hidden'
					}, {
						name : 'delivery_service_name',
						fieldLabel : SysX.t.DeliveryService,
						allowBlank : false,
						xtype : 'combo',
						anchor : '100%',
						valueField : 'name',
						displayField : 'name',
						queryMode : 'local',
						forceSelection : true,
						triggerAction : 'all',
						store : Ext.create('SysX.store.DeliveryService', {
							autoLoad : true,
							filters : [{
								property : 'status',
								value : 'ACTIVE'
							}]
						}),
						validator : function(value) {
							if (this.store.findExact('name', value) === -1) {
								return false;
							}
							return true;

						}
					}, {
						name : 'product_company_ref',
						fieldLabel : SysX.t.Product,
						allowBlank : true,
						anchor : '100%',
						xtype : 'textarea'
					}, {
						name : 'sales_reference',
						fieldLabel : SysX.t.SalesReference,
						anchor : '100%'
					}, {
						name : 'payment_reference',
						fieldLabel : SysX.t.PaymentReference,
						anchor : '100%'
					}]
				}, {
					title : SysX.t.Note,
					defaults : {
						xtype : 'textarea',
						anchor : '100%'
					},
					items : [{
						name : 'special_instruction',
						fieldLabel : SysX.t.SpecialInstruction
					}, {
						name : 'error_description',
						fieldLabel : SysX.t.ErrorDescription,
						value : 'N/A',
						xtype : 'displayfield'
					}]
				}]
			}, {
				// Fieldsets
				defaults : {
					xtype : 'fieldset',
					layout : 'form',
					anchor : '100%',
					autoHeight : true
				},
				items : [{
					title : SysX.t.Delivery,
					defaults : {
						xtype : 'textfield'
					},
					items : [{
						name : 'contact',
						fieldLabel : SysX.t.Contact,
						allowBlank : false
					}, {
						name : 'business_name',
						fieldLabel : SysX.t.BusinessName,
						allowBlank : true
					}, {
						name : 'address_line_1',
						fieldLabel : SysX.t.AddressLine1,
						allowBlank : true,
						anchor : '100%'
					}, {
						name : 'address_line_2',
						fieldLabel : SysX.t.AddressLine2,
						allowBlank : true,
						anchor : '100%'
					}, {
						name : 'address_line_3',
						fieldLabel : SysX.t.AddressLine3,
						allowBlank : true,
						anchor : '100%'
					}, {
						name : 'city',
						fieldLabel : SysX.t.City,
						allowBlank : true
					}, {
						name : 'county',
						fieldLabel : SysX.t.County,
						allowBlank : true
					}, {
						name : 'post_code',
						fieldLabel : SysX.t.PostCode,
						allowBlank : true,
						anchor : '50%'
					}, {
						name : 'country_name',
						fieldLabel : SysX.t.Country,
						allowBlank : false,
						xtype : 'combo',
						valueField : 'printable_name',
						displayField : 'printable_name',
						queryMode : 'local',
						triggerAction : 'all',
						typeAhead : true,
						store : Ext.create('SysX.store.Country', {
							autoLoad : true,
							remoteFilter : false
						}),
						validator : function(value) {
							if (this.store.findExact('printable_name', value) === -1) {
								return false;
							}
							return true;
						}
					}, {
						name : 'telephone',
						fieldLabel : SysX.t.Telephone,
						allowBlank : true
					}, {
						name : 'email',
						fieldLabel : SysX.t.Email,
						allowBlank : true
					}]
				}, {
					title : SysX.t.Status,
					items : [{
						name : 'status',
						fieldLabel : 'Status',
						allowBlank : false,
						xtype : 'combo',
						value : 'ACTIVE',
						valueField : 'value',
						displayField : 'text',
						queryMode : 'local',
						forceSelection : true,
						triggerAction : 'all',
						editable : false,
						store : Ext.Object.getValues(Ext.create('SysX.model.TempConsignmentInfo').status)
					}]
				}]
			}]
		};

		this.callParent();
	}
});

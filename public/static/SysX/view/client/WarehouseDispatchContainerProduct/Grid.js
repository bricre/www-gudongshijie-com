Ext.define('SysX.view.client.WarehouseDispatchContainerProduct.Grid', {
	extend : 'SysX.view.abstract.Grid',
	store : 'SysX.store.WarehouseDispatchContainerProduct',
	title : SysX.t.WarehouseDispatchedStock,
	disableTopToolbar : true,
	requires : ['SysX.model.WarehouseDispatch'],
	columns : [{
		header : SysX.t.Product,
		dataIndex : 'product_id',
		flex : 1,
		filter : true
	}, {
		header : SysX.t.ClientRef,
		dataIndex : 'product-client_ref',
		flex : 1,
		excludeFromSelect : true,
		filter : true
	}, {
		header : SysX.t.FinalFreightMethod,
		dataIndex : 'warehouse_dispatch-final_freight_method',
		flex : 1,
		excludeFromSelect : true,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			if (value) {
				var status = Ext.create('SysX.model.WarehouseDispatch').finalFreightMethod;
				return status[value][1];
			}
		},
		filter : {
			type : 'list',
			options : Ext.Object.getValues(Ext.create('SysX.model.WarehouseDispatch').finalFreightMethod)
		}
	}, {
		header : SysX.t.Reference,
		dataIndex : 'warehouse_dispatch-reference',
		excludeFromSelect : true,
		flex : 3,
		filter : true
	}, {
		header : SysX.t.Quantity,
		dataIndex : 'quantity',
		flex : 1,
		filter : true
	}, {
		header : SysX.t.UpdateTime,
		dataIndex : 'warehouse_dispatch-delivery_dispatch_time',
		excludeFromSelect : true,
		flex : 1,
		alignt : 'right',
		filter : {
			type : 'date',
			phpMode : true,
			dateFormat : 'Y-m-d',
			active : true
		}
	}, {
		header : SysX.t.WarehouseDispatch,
		dataIndex : 'warehouse_dispatch-status',
		excludeFromSelect : true,
		sortable : true,
		flex : 1,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			var status = SysX.model.WarehouseDispatch.prototype.status;
			metaData.tdAttr = status[value][2];
			return status[value][1];
		},
		filter : {
			type : 'list',
			options : Ext.Object.getValues(Ext.create('SysX.model.WarehouseDispatch').status)
		}
	}],
	defaultSorters : [{
		dataIndex : 'warehouse_dispatch-delivery_dispatch_time',
		order : 'DESC'
	}]
});

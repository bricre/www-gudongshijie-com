Ext.define('SysX.view.client.Product_InternalShared.Window', {
	extend : 'SysX.view.abstract.Window',
	itemId : 'client.Product_InternalShared.Window',
	initComponent : function() {
		this.formItems = {
			title : SysX.t.BasicDetails,
			defaults : {
				xtype : 'displayfield'
			},
			items : [{
				name : 'name',
				fieldLabel : SysX.t.Name
			}, {
				fieldLabel : SysX.t.ClientRef,
				itemId : 'sku'
			}, {
				name : 'price',
				fieldLabel : SysX.t.Price
			}, {
				name : 'weight',
				fieldLabel : SysX.t.Weight
			}, {
				name : 'length',
				fieldLabel : SysX.t.Length
			}, {
				name : 'width',
				fieldLabel : SysX.t.Width
			}, {
				name : 'depth',
				fieldLabel : SysX.t.Depth
			}, {
				name : 'live_stock',
				fieldLabel : SysX.t.LiveStock
			}, {
				fieldLabel : SysX.t.Description
			}, {
				name : 'description',
				xtype : 'displayfield',
				hideLabel : true
			}]
		};

		this.callParent(arguments);
	}
});

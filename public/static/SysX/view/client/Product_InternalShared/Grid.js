Ext.define('SysX.view.client.Product_InternalShared.Grid', {
	extend : 'SysX.view.client.Product.Grid',
	itemId : 'client.Product_InternalShared.Grid',
	store : 'SysX.store.Product_InternalShared',
	title : SysX.t.SharedProduct,
	disableTopToolbar : true,
	columns : [{
		header : SysX.t.ID,
		dataIndex : 'id',
		sortable : true,
		flex : 1,
		filter : true
	}, {
		header : SysX.t.ClientID,
		dataIndex : 'client_id',
		sortable : true,
		flex : 1,
		filter : true
	}, {
		header : SysX.t.Name,
		dataIndex : 'name',
		sortable : true,
		filter : true,
		flex : 3
	}, {
		header : SysX.t.Note,
		dataIndex : 'note',
		filter : true,
		flex : 2
	}, {
		header : SysX.t.ClientRef,
		dataIndex : 'client_ref',
		sortable : true,
		filter : true,
		flex : 2
	}, {
		header : SysX.t.Price,
		dataIndex : 'price',
		sortable : true,
		align : 'right',
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			return Ext.util.Format.number(value, '0,0.00');
		},
		flex : 1,
		filter : true
	}, {
		header : SysX.t.ProductDimension,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			return record.data.length + '*' + record.data.width + '*' + record.data.depth;
		},
		flex : 2
	}, {
		header : SysX.t.Weight,
		dataIndex : 'weight',
		sortable : true,
		align : 'right',
		flex : 1
	}, {
		header : SysX.t.LiveStock,
		dataIndex : 'live_stock',
		sortable : true,
		align : 'right',
		flex : 1,
		filter : true
	}, {
		header : SysX.t.LowStockLevel,
		dataIndex : 'low_stock_level',
		sortable : true,
		align : 'right',
		flex : 1,
		filter : true,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			if (value > 0 && value >= record.data.live_stock) {
				metaData.tdAttr = 'style="color:red"';
			}
			return value;
		}
	}, {
		header : SysX.t.Status,
		dataIndex : 'status',
		sortable : true,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			var status = SysX.model.Product.prototype.status;
			metaData.tdAttr = status[value][2];
			return status[value][1];
		},
		filter : {
			type : 'list',
			options : Ext.Object.getValues(Ext.create('SysX.model.Product').status),
			active : true,
			value : ['ACTIVE']
		},
		flex : 2
	}, {
		dataIndex : 'length',
		hidden : true
	}, {
		dataIndex : 'width',
		hidden : true
	}, {
		dataIndex : 'depth',
		hidden : true
	}]
});

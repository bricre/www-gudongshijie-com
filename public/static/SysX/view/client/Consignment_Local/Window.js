Ext.define('SysX.view.client.Consignment_Local.Window', {
	extend : 'SysX.view.client.Consignment.Window',
	title : SysX.t.LocalConsignment,
	onAfterInitComponent : function() {
		this.down('form').getForm().setValues({
			type : 'LOCAL'
		});

		this.down('form').down('combo[name=delivery_service_id]').getStore().addFilter({
			property : 'consignment_type',
			value : 'LOCAL'
		}, false);
	}
});

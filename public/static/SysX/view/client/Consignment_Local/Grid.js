Ext.define('SysX.view.client.Consignment_Local.Grid', {
	extend : 'SysX.view.client.Consignment.Grid',
	itemId : 'client.Consignment_Local.Grid',
	title : SysX.t.LocalConsignment,
	onBeforeInitComponent : function() {
		this.getStore().proxy.extraParams.type = 'LOCAL';

		Ext.Array.findBy(this.columns, function(item) {
			return item.dataIndex === 'delivery_service_id' ? true : false;
		}).filter.store = Ext.create('SysX.store.DeliveryService', {
			storeId : Ext.id(),
			filters : [{
				property : 'status',
				value : 'ACTIVE'
			}, {
				property : 'consignment_type',
				value : 'LOCAL'
			}]
		});
	},
	onAfterInitComponent : function() {
		this.callParent(arguments);
		this.down('toolbar[dock=top]').down('combo[itemId=BatchChangeDeliveryServiceCombo]').getStore().addFilter({
			property : 'consignment_type',
			value : 'LOCAL'
		}, false);
	}
});

Ext.define('SysX.view.client.WarehouseDispatchClientDispatch.Window', {
	extend : 'SysX.view.abstract.Window',
	itemId : 'client.WarehouseDispatchClientDispatch.Window',
	height : 400,
	width : 400,
	initComponent : function() {
		this.formItems = {};

		this.callParent(arguments);
	}
}); 
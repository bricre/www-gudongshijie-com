Ext.define('SysX.view.client.WarehouseDispatchClientDispatch.Grid', {
	extend : 'SysX.view.abstract.Grid',
	alias : 'widget.WarehouseDispatchClientDispatch.Grid',
	store : 'SysX.store.WarehouseDispatchClientDispatch',
	title : SysX.t.Fee,
	disableTopToolbar : true,
	disableItemDbClick : true,
	columns : [{
		dataIndex : 'id',
		hidden : true,
		excludeFromSelect : true
	}, {
		header : SysX.t.WarehouseDispatch,
		dataIndex : 'warehouse_dispatch_id',
		flex : 1
	}, {
		header : SysX.t.ClientDispatch,
		dataIndex : 'client_dispatch_id',
		flex : 1
	}, {
		header : SysX.t.DeliveryFee,
		dataIndex : 'delivery_fee',
		flex : 1
	}, {
		header : SysX.t.CustomsFee,
		dataIndex : 'customs_fee',
		flex : 1
	}, {
		header : SysX.t.WarehouseDispatchDeliveryDispatchTime,
		dataIndex : 'warehouse_dispatch_delivery_dispatch_time',
		excludeFromSelect : true,
		flex : 1
	}, {
		header : SysX.t.WarehouseDispatchDeliveryArriveTime,
		dataIndex : 'warehouse_dispatch_delivery_arrive_time',
		excludeFromSelect : true,
		flex : 1
	}]
});

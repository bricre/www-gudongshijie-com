Ext.define('SysX.view.client.Consignment_Direct.Grid', {
	extend : 'SysX.view.client.Consignment.Grid',
	itemId : 'client.Consignment_Direct.Grid',
	title : SysX.t.DirectConsignment,
	onBeforeInitComponent : function() {
		this.getStore().proxy.extraParams.type = 'DIRECT';

		Ext.Array.findBy(this.columns, function(item) {
			return item.dataIndex === 'delivery_service_id' ? true : false;
		}).filter.store = Ext.create('SysX.store.DeliveryService', {
			storeId : Ext.id(),
			filters : [{
				property : 'status',
				value : 'ACTIVE'
			}, {
				property : 'consignment_type',
				value : 'DIRECT'
			}]
		});
	},
	onAfterInitComponent : function(view) {
		this.callParent(arguments);

		this.down('toolbar[dock=top]').down('combo[itemId=BatchChangeDeliveryServiceCombo]').getStore().addFilter({
			property : 'consignment_type',
			value : 'DIRECT'
		}, false);

		this.down('toolbar[dock=top]').insert(7, {
			text : SysX.t.Print,
			listeners : {
				'click' : function() {
					var records = view.getSelectionModel().getSelection();
					if (records.length < 1) {
						Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, SysX.t.PrintPackingSlipMessage_SelectConsignmentFirst);
						return false;
					}

					var selectedIds = [];
					for (var i = records.length - 1; i >= 0; i--) {
						selectedIds.push(records.shift().data.id);
					}
					SysX.openNewWindow(modulePath + '/Printer/Packing-Slip/' + selectedIds.join('_') + '?type=AddressLabel');
				}
			}
		});
	}
});

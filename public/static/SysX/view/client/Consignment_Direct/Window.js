Ext.define('SysX.view.client.Consignment_Direct.Window', {
	extend : 'SysX.view.client.Consignment.Window',
	title : SysX.t.DirectConsignment,
	onAfterInitComponent : function() {
		this.down('form').getForm().setValues({
			type : 'DIRECT'
		});

		this.down('form').down('combo[name=delivery_service_id]').getStore().addFilter({
			property : 'consignment_type',
			value : 'DIRECT'
		}, false);
	}
});

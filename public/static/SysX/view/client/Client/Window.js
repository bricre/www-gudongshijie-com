Ext.define('SysX.view.client.Client.Window', {
	extend : 'SysX.view.abstract.Window',
	itemId : 'client.Client.Window',
	initComponent : function() {
		this.formItems = {
			title : SysX.t.BasicDetails,
			layout : 'column',
			defaults : {
				layout : 'form',
				columnWidth : 0.5,
				border : false,
				bodyStyle : 'padding:5px'
			},
			// Columns
			items : [{
				// Fieldsets
				defaults : {
					xtype : 'fieldset',
					layout : 'form',
					anchor : '100%',
					autoHeight : true
				},
				items : [{
					title : SysX.t.BasicDetails,
					defaults : {
						xtype : 'textfield',
						anchor : '100%'
					},
					items : [{
						name : 'id',
						xtype : 'hidden'
					}, {
						name : 'name',
						fieldLabel : SysX.t.Name,
						allowBlank : false
					}, {
						name : 'email',
						fieldLabel : SysX.t.Email,
						allowBlank : false,
						vtype : 'email'
					}, {
						name : 'telephone',
						fieldLabel : SysX.t.Telephone,
						allowBlank : true
					}, {
						name : 'address_line_1',
						fieldLabel : SysX.t.AddressLine1,
						allowBlank : true
					}, {
						name : 'address_line_2',
						fieldLabel : SysX.t.AddressLine2,
						allowBlank : true
					}, {
						name : 'address_line_3',
						fieldLabel : SysX.t.AddressLine3,
						allowBlank : true
					}, {
						name : 'city',
						fieldLabel : SysX.t.City,
						allowBlank : true
					}, {
						name : 'county',
						fieldLabel : SysX.t.County,
						allowBlank : true
					}, {
						name : 'post_code',
						fieldLabel : SysX.t.PostCode,
						allowBlank : true,
						anchor : '50%'
					}, {
						name : 'country_iso',
						fieldLabel : SysX.t.Country,
						allowBlank : false,
						xtype : 'combo',
						valueField : 'iso',
						displayField : 'printable_name',
						queryMode : 'local',
						triggerAction : 'all',
						typeAhead : true,
						store : Ext.create('SysX.store.Country', {
							autoLoad : true,
							remoteFilter : false
						})
					}]
				}]
			}, {
				// Fieldsets
				defaults : {
					xtype : 'fieldset',
					layout : 'form',
					anchor : '100%',
					autoHeight : true
				},
				items : [{
					title : SysX.t.ConsignmentSettings,
					defaults : {
						xtype : 'textfield'
					},
					items : [{
						name : 'bulk_order_reference_field',
						fieldLabel : SysX.t.BulkOrderReferenceField,
						allowBlank : false,
						xtype : 'combo',
						value : 'ID',
						valueField : 'value',
						displayField : 'text',
						queryMode : 'local',
						forceSelection : true,
						triggerAction : 'all',
						editable : false,
						store : new Ext.data.ArrayStore({
							fields : ['value', 'text'],
							data : Ext.Object.getValues(Ext.create('SysX.model.Client').bulkOrderReferenceField)
						})
					}]
				}, {
					title : SysX.t.Account,
					defaults : {
						xtype : 'textfield'
					},
					items : [{
						name : 'is_vip',
						fieldLabel : SysX.t.IsVip,
						xtype : 'checkbox',
						disabled : true
					}, {
						name : 'client_group_name',
						fieldLabel : SysX.t.ClientGroup,
						xtype : 'displayfield'
					}, {
						name : 'credit_limit',
						fieldLabel : SysX.t.CreditLimit,
						xtype : 'displayfield',
						allowBlank : false,
						value : 0.00,
						size : '9'
					}, {
						name : 'balance',
						xtype : 'displayfield',
						fieldLabel : SysX.t.Balance,
						value : 0.00
					}, {
						name : 'create_time',
						xtype : 'displayfield',
						fieldLabel : SysX.t.CreateTime
					}]
				}]
			}]
		};

		this.callParent(arguments);
	}
});

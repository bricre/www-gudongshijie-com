Ext.define('SysX.view.client.Client.Grid', {
	extend : 'SysX.view.abstract.Grid',
	itemId : 'client.Client.Grid',
	store : 'SysX.store.Client',
	title : SysX.t.Client,
	columns : [{
		header : SysX.t.ID,
		dataIndex : 'id',
		sortable : true,
		filter : {
			type : 'numeric'
		},
		flex : 1
	}, {
		header : SysX.t.Name,
		dataIndex : 'name',
		sortable : true,
		flex : 2
	}, {
		header : SysX.t.ClientGroup,
		dataIndex : 'client_group_id',
		sortable : true,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			return record.data.client_group_name;
		},
		flex : 2
	}, {
		header : SysX.t.IsInternal,
		dataIndex : 'is_internal',
		sortable : true,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			return record.data.is_internal ? 'Yes' : 'No';
		},
		filter : true,
		flex : 0.5
	}, {
		header : SysX.t.IsExternal,
		dataIndex : 'is_external',
		sortable : true,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			return record.data.is_external ? 'Yes' : 'No';
		},
		filter : true,
		flex : 0.5
	}, {
		header : SysX.t.Status,
		dataIndex : 'status',
		sortable : true,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			var status = SysX.model.Client.prototype.status;
			metaData.tdAttr = status[value][2];
			return status[value][1];
		},
		filter : {
			type : 'list',
			options : Ext.Object.getValues(Ext.create('SysX.model.Client').status)
		},
		flex : 1
	}]
});

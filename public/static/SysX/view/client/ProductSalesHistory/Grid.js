Ext.define('SysX.view.client.ProductSalesHistory.Grid', {
	extend : 'SysX.view.abstract.Grid',
	itemId : 'client.ProductSalesHistory.Grid',
	store : 'SysX.store.ProductSalesHistory',
	title : SysX.t.SalesHistory,
	requires : ['SysX.model.Consignment'],
	columns : [{
		header : SysX.t.ID,
		dataIndex : 'id',
		sortable : true,
		flex : 1
	}, {
		header : SysX.t.ConsignmentID,
		dataIndex : 'consignment-id',
		sortable : true,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			return record.data['consignment-client_id'] + '-' + record.data['consignment-id'];
		},
		filter : true,
		flex : 1
	}, {
		header : SysX.t.LastUpdate,
		dataIndex : 'consignment-update_time',
		xtype : 'datecolumn',
		sortable : true,
		filter : {
			dateFormat : 'Y-m-d',
			active : true,
			value : {
				after : Ext.Date.add(new Date(), Ext.Date.MONTH, -1)
			}
		},
		flex : 2
	}, {
		header : SysX.t.Quantity,
		dataIndex : 'consignment_product-quantity',
		sortable : false,
		flex : 1
	}, {
		header : SysX.t.Status,
		dataIndex : 'consignment-status',
		sortable : true,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			var status = SysX.model.Consignment.prototype.status;
			metaData.tdAttr = status[value][2];
			return status[value][1];
		},
		filter : {
			type : 'list',
			options : Ext.Object.getValues(Ext.create('SysX.model.Consignment').status),
			active : true,
			value : ['FINISHED']
		},
		flex : 1
	}],
	defaultSorters : [{
		dataIndex : 'consignment-update_time',
		order : 'DESC'
	}]
});

Ext.define('SysX.view.client.ClientUserGroup.Grid', {
	extend : 'SysX.view.abstract.Grid',
	itemId : 'client.ClientUserGroup.Grid',
	store : 'SysX.store.ClientUserGroup',
	title : SysX.t.UserGroup,
	columns : [{
		header : SysX.t.ID,
		dataIndex : 'id',
		sortable : true,
		flex : 1,
		hidden : true
	}, {
		header : SysX.t.Name,
		dataIndex : 'name',
		sortable : true,
		flex : 3
	}],
	defaultSorters : [{
		dataIndex : 'name',
		order : 'DESC'
	}]

});

Ext.define('SysX.view.client.Consignment.Grid', {
	extend : 'SysX.view.abstract.Grid',
	itemId : 'client.Consignment.Grid',
	store : 'SysX.store.Consignment',
	title : SysX.t.Consignment,
	disableDeleteButton : true,
	columns : [{
		header : SysX.t.ID,
		dataIndex : 'id',
		sortable : true,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			return record.data.client_id + '-' + record.data.id;
		},
		filter : true,
		flex : 1
	}, {
		header : SysX.t.Products,
		dataIndex : 'consignment_product_reference',
		flex : 1,
		filter : true
	}, {
		header : SysX.t.ClientID,
		dataIndex : 'client_id',
		hidden : true
	}, {
		header : SysX.t.SalesReference,
		dataIndex : 'sales_reference',
		filter : true
	}, {
		header : SysX.t.Contact,
		dataIndex : 'contact',
		flex : 1,
		filter : true
	}, {
		header : SysX.t.PostCode,
		dataIndex : 'post_code',
		flex : 1,
		filter : true
	}, {
		header : SysX.t.DeliveryReference,
		dataIndex : 'delivery_reference',
		flex : 1,
		filter : true
	}, {
		header : SysX.t.TotalPrice,
		dataIndex : 'total_price',
		align : 'right',
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			return Ext.util.Format.number(value, '0,0.00');
		},
		flex : 0.5
	}, {
		header : SysX.t.TotalDeliveryFee,
		dataIndex : 'total_delivery_fee',
		align : 'right',
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			return Ext.util.Format.number(value, '0,0.00');
		},
		flex : 0.5
	}, {
		header : SysX.t.TotalHandlingFee,
		dataIndex : 'total_handling_fee',
		align : 'right',
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			return Ext.util.Format.number(value, '0,0.00');
		},
		flex : 0.5
	}, {
		header : SysX.t.TotalWeight,
		dataIndex : 'total_weight',
		align : 'right',
		flex : 0.5
	}, {
		header : SysX.t.DeliveryService,
		dataIndex : 'delivery_service_id',
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			return record.data.delivery_service_name;
		},
		sortable : true,
		flex : 1.5,
		filter : {
			type : 'list',
			labelField : 'name',
			overflowY : 'auto',
			store : Ext.create('SysX.store.DeliveryService', {
				pageSize : -1,
				filters : [{
					property : 'status',
					value : 'ACTIVE'
				}],
				listeners : {
					beforeload : function(store) {
						store.getProxy().extraParams.is_internal = 0;
					}
				}
			})
		}
	}, {
		header : SysX.t.CreateTime,
		dataIndex : 'create_time',
		sortable : true,
		alignt : 'right',
		flex : 1
	}, {
		header : SysX.t.LastUpdate,
		dataIndex : 'update_time',
		sortable : true,
		alignt : 'right',
		filter : {
			type : 'date',
			phpMode : true,
			dateFormat : 'Y-m-d',
			active : true,
			value : {
				after : Ext.Date.add(new Date(), Ext.Date.MONTH, -1)
			}
		},
		flex : 1
	}, {
		header : SysX.t.Status,
		dataIndex : 'status',
		sortable : true,
		width : 80,
		fixed : true,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			var status = SysX.model.Consignment.prototype.status;
			metaData.tdAttr = status[value][2];
			return status[value][1];
		},
		filter : {
			type : 'list',
			options : Ext.Object.getValues(Ext.create('SysX.model.Consignment').status)
		},
		flex : 1
	}],
	defaultSorters : [{
		dataIndex : 'update_time',
		order : 'DESC'
	}],
	onAfterInitComponent : function(view) {
		var topToolbar = view.getDockedItems('toolbar[dock="top"]')[0];
		topToolbar.insert(2, {
			xtype : 'button',
			text : SysX.t.DuplicateConsignment,
			handler : function(button) {
				var records = button.up('gridpanel').getSelectionModel().getSelection();
				if (records.length < 1) {
					Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, SysX.t.DuplicateConsignment_SelectConsignmentFirst);
					return false;
				}

				var selectedIds = [];
				for (var i = records.length - 1; i >= 0; i--) {
					var r = records.shift();
					selectedIds.push(r.data.id);
				}
				Ext.Ajax.request({
					url : modulePath + '/Consignment/Batch-Duplicate',
					method : 'POST',
					scope : this,
					params : {
						ids : selectedIds.join('_')
					},
					success : function(response, opts) {
						var response = Ext.decode(response.responseText), msg;
						msg = response.message;

						Ext.Msg.alert(SysX.t.MessageBoxTitle_Success, msg);
						view.getStore().load();
					},
					failure : function(response, opts) {
						var msg = Ext.decode(response.responseText).message;
						Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, msg);
						return false;
					}
				});
			},
			scope : this
		});

		topToolbar.insert(3, {
			xtype : 'button',
			text : SysX.t.BatchChangeStatus,
			handler : function(button) {
				var status = button.up('toolbar').down('combo[itemId=BatchChangeStatusCombo]').getValue();
				if ('' === status) {
					Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, SysX.t.BatchChangeStatusMessage_SelectStatusFirst);
					return false;
				}

				var records = button.up('gridpanel').getSelectionModel().getSelection();
				if (records.length < 1) {
					Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, SysX.t.BatchChangeStatusMessage_SelectConsignmentFirst);
					return false;
				}

				var selectedIds = [];
				var unchangableIds = [];
				for (var i = records.length - 1; i >= 0; i--) {
					var r = records.shift();
					if (r.data.status === 'PREPARING' || r.data.status == 'REVIEWING' || r.data.status == 'CANCELLED' || r.data.status == 'PENDING' || r.data.status == 'PROCESSING') {
						selectedIds.push(r.data.id);
					} else {
						unchangableIds.push(r.data.id);
					}
				}
				this.unchangableIds = unchangableIds;
				if (selectedIds.length === 0) {
					Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, SysX.t.BatchChangeStatusMessage_ConsignmentStatusPrevented);
					return false;
				}
				Ext.Ajax.request({
					url : modulePath + '/Consignment/Batch-Update-Status',
					method : 'POST',
					scope : this,
					params : {
						ids : selectedIds.join('_'),
						'status' : status
					},
					success : function(response, opts) {
						var response = Ext.decode(response.responseText), msg;
						if (response.success) {
							msg = SysX.t.BatchChangeStatusMessage_AllConsignmentsUpdated;
						} else {
							msg = response.message;
						}

						this.unchangableIds = undefined;
						Ext.Msg.alert(SysX.t.MessageBoxTitle_Success, msg);
						view.getStore().load();
					},
					failure : function(response, opts) {
						var msg = Ext.decode(response.responseText).message;
						this.unchangableIds = undefined;
						Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, msg);
						return false;
					}
				});
			},
			scope : this
		});

		topToolbar.insert(4, {
			xtype : 'combo',
			submitValue : false,
			itemId : 'BatchChangeStatusCombo',
			valueField : 'value',
			displayField : 'text',
			mode : 'local',
			forceSelection : true,
			triggerAction : 'all',
			editable : false,
			store : new Ext.data.ArrayStore({
				fields : ['value', 'text', 'attr'],
				data : [['PREPARING', SysX.t.Status_PREPARING, 'style="color:peru"'], ['PENDING', SysX.t.Status_PENDING, 'style="color:red"'], ['CANCELLED', SysX.t.Status_CANCELLED, 'style="color:orange"'], ['DELETED', SysX.t.Status_DELETED, 'style="color:grey"']]
			})
		});

		topToolbar.insert(5, {
			xtype : 'button',
			text : SysX.t.ChangeDeliveryService,
			listeners : {
				click : function(button) {
					var gridPanel = button.up('gridpanel'), records = gridPanel.getSelectionModel().getSelection(), consignmentIdString = '', deliveryServiceCombo = gridPanel.down('toolbar[dock=top]').down('combo[itemId=BatchChangeDeliveryServiceCombo]'), deliveryServiceName = deliveryServiceCombo.getValue();

					if (0 == records.length) {
						return Ext.Msg.alert(SysX.t.MessageBoxTitle_Notice, SysX.t.TempConsignmentInfoMessage_SelectRecord);
					}

					if (!deliveryServiceCombo.isValid()) {
						return Ext.Msg.alert(SysX.t.MessageBoxTitle_Notice, SysX.t.TempConsignmentInfoMessage_SelectDeliveryService);
					}

					for (var i = 0; i < records.length; i++) {
						consignmentIdString += records[i].data.id + '_';
					}

					var progressBar = Ext.create('Ext.ProgressBar', {
						id : 'BatchChangeDeliveryServiceProgressBar',
						text : SysX.t.Initializing
					});

					var progressWindow = Ext.create('Ext.window.Window', {
						id : 'BatchChangeDeliveryServiceWindow',
						height : 100,
						width : Ext.getBody().getWidth() * 0.75,
						layout : 'fit',
						modal : true,
						title : SysX.t.Progress,
						items : [{
							xtype : 'panel',
							layout : 'fit',
							bodyStyle : 'padding:10px',
							items : [progressBar],
							bbar : {
								buttonAlign : 'right',
								items : [{
									xtype : 'button',
									text : SysX.t.Cancel
								}]
							}
						}]
					});
					progressWindow.show();
					progressBar.wait();

					Ext.Ajax.request({
						url : modulePath + '/Consignment/Batch-Change-Delivery-Service',
						method : 'GET',
						params : {
							ids : consignmentIdString,
							delivery_service_name : deliveryServiceName
						},
						success : function(response, opts) {
							response = Ext.decode(response.responseText);
							progressBar.reset();
							progressBar.updateProgress(1);
							if (response.success) {
								Ext.Msg.alert(SysX.t.MessageBoxTitle_Success, SysX.t.TempConsignmentInfoMessage_DeliveryServiceChanged);
							} else {
								Ext.Msg.alert(SysX.t.MessageBoxTitle_Alert, response.message);
							}
							view.down('pagingtoolbar').doRefresh();
							progressWindow.close();
						},
						failure : function(response, opts) {
							Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, SysX.t.TempConsignmentInfoMessage_ProcessFail);
						}
					});
					return true;
				}
			}
		});

		topToolbar.insert(6, {
			xtype : 'combo',
			width : 250,
			submitValue : false,
			itemId : 'BatchChangeDeliveryServiceCombo',
			valueField : 'name',
			displayField : 'name',
			queryMode : 'local',
			forceSelection : true,
			triggerAction : 'all',
			editable : false,
			allowBlank : false,
			store : Ext.create('SysX.store.DeliveryService', {
				filters : [{
					property : 'status',
					value : 'ACTIVE'
				}]
			})
		});
	}
});

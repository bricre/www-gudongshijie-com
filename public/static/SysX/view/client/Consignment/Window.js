Ext.define('SysX.view.client.Consignment.Window', {
	extend : 'SysX.view.abstract.Window',
	initComponent : function() {
		this.formItems = {
			title : SysX.t.ConsignmentInfo,
			layout : 'column',
			defaults : {
				columnWidth : 0.5,
				border : false,
				bodyStyle : 'padding:5px'
			},
			//Columns
			items : [{
				//Fieldsets
				defaults : {
					xtype : 'fieldset',
					anchor : '100%',
					autoHeight : true
				},
				items : [{
					title : SysX.t.BasicDetails,
					defaults : {
						xtype : 'textfield',
						anchor : '50%'
					},
					items : [{
						name : 'client_id',
						xtype : 'hidden'
					}, {
						name : 'id',
						fieldLabel : SysX.t.ID,
						readOnly : true,
						hidden : true
					}, {
						fieldLabel : SysX.t.Price,
						xtype : 'fieldcontainer',
						itemId : 'price_fieldcontainer',
						hidden : true,
						items : [{
							xtype : 'button',
							text : SysX.t.ReCalculateFee,
							itemId : 'calculate_fee_button'
						}]
					}, {
						xtype : 'displayfield',
						name : 'total_price',
						fieldLabel : SysX.t.TotalPrice,
						emptyText : '0'
					}, {
						xtype : 'displayfield',
						name : 'total_delivery_fee',
						fieldLabel : SysX.t.TotalDeliveryFee,
						emptyText : '0'
					}, {
						xtype : 'displayfield',
						name : 'total_handling_fee',
						fieldLabel : SysX.t.HandlingFee,
						emptyText : '0'
					}, {
						xtype : 'displayfield',
						name : 'total_weight',
						fieldLabel : SysX.t.TotalWeight,
						emptyText : '0'
					}, {
						name : 'delivery_service_id',
						fieldLabel : SysX.t.DeliveryService,
						allowBlank : false,
						xtype : 'combo',
						anchor : '100%',
						valueField : 'id',
						displayField : 'name',
						queryMode : 'local',
						forceSelection : true,
						triggerAction : 'all',
						editable : false,
						store : Ext.create('SysX.store.DeliveryService', {
							autoLoad : true,
							pageSize : -1,
							filters : [{
								property : 'status',
								value : 'ACTIVE'
							}, {
								property : 'is_internal',
								value : 'false'
							}]
						})
					}, {
						name : 'sales_reference',
						fieldLabel : SysX.t.SalesReference,
						anchor : '100%'
					}, {
						xtype : 'displayfield',
						emptyText : 'N/A',
						name : 'delivery_reference',
						fieldLabel : SysX.t.DeliveryReference,
						anchor : '100%'
					}, {
						name : 'create_time',
						fieldLabel : SysX.t.CreateTime,
						xtype : 'displayfield',
						anchor : '100%',
						value : 'Now'
					}, {
						name : 'update_time',
						fieldLabel : SysX.t.LastUpdate,
						xtype : 'displayfield',
						anchor : '100%',
						value : 'None'
					}, {
						name : 'finish_time',
						fieldLabel : SysX.t.FinishTime,
						xtype : 'displayfield',
						anchor : '100%',
						value : 'None',
						hidden : true
					}, {
						name : 'arrive_time',
						fieldLabel : SysX.t.ArriveTime,
						format : 'Y-m-d',
						xtype : 'datetimefield',
						hidden : true
					}, {
						name : 'update_time',
						xtype : 'hidden',
						value : Ext.Date.format(new Date(), "Y-m-d H:i:s")
					}]
				}]
			}, {
				//Fieldsets
				defaults : {
					xtype : 'fieldset',
					anchor : '100%',
					autoHeight : true
				},
				items : [{
					title : SysX.t.DeliveryDetail,
					defaults : {
						xtype : 'textfield',
						anchor : '100%'
					},
					items : [{
						name : 'contact',
						fieldLabel : SysX.t.Contact,
						allowBlank : false
					}, {
						name : 'business_name',
						fieldLabel : SysX.t.BusinessName,
						allowBlank : true
					}, {
						name : 'address_line_1',
						fieldLabel : SysX.t.AddressLine1,
						allowBlank : true,
						anchor : '100%'
					}, {
						name : 'address_line_2',
						fieldLabel : SysX.t.AddressLine2,
						allowBlank : true,
						anchor : '100%'
					}, {
						name : 'address_line_3',
						fieldLabel : SysX.t.AddressLine3,
						allowBlank : true,
						anchor : '100%'
					}, {
						name : 'city',
						fieldLabel : SysX.t.City,
						allowBlank : true
					}, {
						name : 'county',
						fieldLabel : SysX.t.County,
						allowBlank : true
					}, {
						name : 'post_code',
						fieldLabel : SysX.t.PostCode,
						allowBlank : true
					}, {
						name : 'country_iso',
						fieldLabel : SysX.t.Country,
						allowBlank : false,
						xtype : 'combo',
						valueField : 'iso',
						displayField : 'printable_name',
						queryMode : 'local',
						triggerAction : 'all',
						store : Ext.create('SysX.store.Country', {
							autoLoad : true,
							remoteFilter : false
						})
					}, {
						name : 'telephone',
						fieldLabel : SysX.t.Telephone,
						allowBlank : true
					}, {
						name : 'email',
						fieldLabel : SysX.t.Email,
						allowBlank : true
					}]
				}, {
					title : SysX.t.Note,
					defaults : {
						xtype : 'textarea',
						anchor : '100%'
					},
					items : [{
						name : 'special_instruction',
						fieldLabel : SysX.t.SpecialInstruction
					}, {
						name : 'neighbour_instruction',
						fieldLabel : SysX.t.NeighbourInstruction
					}]
				}, {
					title : 'Status',
					items : [{
						name : 'status',
						fieldLabel : SysX.t.Status,
						allowBlank : false,
						xtype : 'combo',
						value : 'PREPARING',
						valueField : 'value',
						displayField : 'text',
						queryMode : 'local',
						forceSelection : true,
						triggerAction : 'all',
						editable : false,
						disabled : true,
						store : new Ext.data.ArrayStore({
							fields : ['value', 'text', 'attr'],
							data : Ext.Object.getValues(Ext.create('SysX.model.Consignment').status)
						})
					}]
				}, {
					name : 'type',
					xtype : 'hidden',
					value : 'LOCAL'
				}]
			}]
		};

		this.callParent(arguments);

		//For some unkown reasons 'value' option for combobox cannot select default value
		//So we have to do it after store is loaded.
		var countryCombo = this.down('combo[name=country_iso]');
		countryCombo.getStore().on('load', function(store, records) {
			if (!countryCombo.getValue()) {
				countryCombo.setValue('GB');
			}
		});
	}
});

Ext.define('SysX.view.client.ClientCreditLimitTopUpTransaction.Grid', {
	extend : 'SysX.view.abstract.Grid',
	alias : 'widget.ClientCreditLimitTopUpTransaction.Grid',
	store : 'SysX.store.ClientCreditLimitTopUpTransaction',
	title : SysX.t.ClientCreditLimitTopUpTransaction,
	disableDeleteButton : true,
	disableItemDbClick : true,
	columns : [{
		dataIndex : 'id',
		hidden : true
	}, {
		header : SysX.t.Amount,
		dataIndex : 'amount',
		flex : 2
	}, {
		header : SysX.t.Time,
		dataIndex : 'update_time',
		sortable : true,
		xtype : 'datecolumn',
		format : 'Y-m-d H:i:s',
		flex : 1,
		filter : {
			type : 'date',
			phpMode : true,
			dateFormat : 'Y-m-d',
			active : true,
			value:{
				after : Ext.Date.add(new Date(), Ext.Date.MONTH, -1)
			}
		}
	}],
	defaultSorters : [{
		dataIndex : 'update_time',
		order : 'DESC'
	}]
});

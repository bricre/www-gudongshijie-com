Ext.define('SysX.view.client.ClientCreditLimitTopUpTransaction.Window', {
	extend : 'SysX.view.abstract.Window',
	itemId : 'client.ClientCreditLimitTopUpTransaction.Window',
	width : 300,
	height : 300,
	initComponent : function() {
		this.formItems = {
			title : SysX.t.BasicDetails,
			layout : 'form',
			items : [{
				name : 'id',
				xtype : 'hidden'
			}, {
				name : 'client_id',
				allowBlank : false,
				xtype : 'hidden'
			}, {
				name : 'amount',
				fieldLabel : SysX.t.Amount,
				itemId : 'AmountDisplayField',
				xtype : 'textfield',
				readOnly : true
			}, {
				name : 'update_time',
				fieldLabel : SysX.t.UpdateTime,
				xtype : 'displayfield',
				value : Ext.Date.format(new Date(), 'Y-m-d H:i:s')
			}, {
				itemId : 'infoPanel',
				fieldLabel : SysX.t.Note,
				xtype : 'box',
				tpl : SysX.t.CreditLimitTopUpInfoPanel
			}]
		};

		this.callParent(arguments);
	}
});

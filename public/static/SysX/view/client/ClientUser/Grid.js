Ext.define('SysX.view.client.ClientUser.Grid', {
	extend : 'SysX.view.abstract.Grid',
	alias : 'widget.ClientUser.Grid',
	store : 'SysX.store.ClientUser',
	title : SysX.t.ClientUser,
	columns : [{
		header : SysX.t.ID,
		dataIndex : 'id',
		sortable : true,
		flex : 1
	}, {
		header : SysX.t.UserGroup,
		dataIndex : 'client_user_group_id',
		sortable : true,
		flex : 2,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			return record.data.client_user_group_name;
		}
	}, {
		header : SysX.t.Username,
		dataIndex : 'username',
		sortable : true,
		flex : 2
	}, {
		header : SysX.t.FirstName,
		dataIndex : 'first_name',
		sortable : true,
		flex : 2
	}, {
		header : SysX.t.LastName,
		dataIndex : 'last_name',
		sortable : true,
		flex : 2
	}, {
		header : SysX.t.Status,
		dataIndex : 'status',
		sortable : true,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			var status = SysX.model.ClientUser.prototype.status;
			metaData.tdAttr = status[value][2];
			return status[value][1];
		},
		filter : {
			type : 'list',
			options : Ext.Object.getValues(Ext.create('SysX.model.ClientUser').status),
			active : true,
			value : ['ACTIVE']
		},
		flex : 1
	}]
});

Ext.define('SysX.view.client.ClientUser.Window', {
	extend : 'SysX.view.abstract.Window',
	itemId : 'client.ClientUser.Window',
	height : 400,
	width : 400,
	initComponent : function() {
		this.formItems = {
			title : SysX.t.BasicDetails,
			defaults : {
				xtype : 'textfield',
				anchor : '100%'
			},
			items : [{
				name : 'id',
				xtype : 'hidden',
				id : 'clientUserIdField'
			}, {
				name : 'client_id',
				xtype : 'hidden'
			}, {
				name : 'username',
				fieldLabel : SysX.t.Username,
				allowBlank : false
			}, {
				name : 'new_password',
				fieldLabel : SysX.t.Password
			}, {
				name : 're_password',
				fieldLabel : SysX.t.ReEnterPassword
			}, {
				name : 'email',
				fieldLabel : SysX.t.Email
			}, {
				name : 'first_name',
				fieldLabel : SysX.t.FirstName
			}, {
				name : 'last_name',
				fieldLabel : SysX.t.LastName
			}, {
				name : 'client_user_group_id',
				fieldLabel : SysX.t.UserGroup,
				allowBlank : true,
				xtype : 'combo',
				valueField : 'id',
				displayField : 'name',
				queryMode : 'local',
				forceSelection : true,
				triggerAction : 'all',
				store : Ext.create('SysX.store.ClientUserGroup', {
					autoLoad : true,
					pageSize : -1
				})
			}, {
				hiddenName : 'status',
				fieldLabel : SysX.t.Status,
				allowBlank : false,
				xtype : 'combo',
				anchor : '50%',
				value : 'ACTIVE',
				valueField : 'value',
				displayField : 'text',
				queryMode : 'local',
				forceSelection : true,
				triggerAction : 'all',
				editable : false,
				store : new Ext.data.ArrayStore({
					fields : ['value', 'text', 'attr'],
					data : Ext.Object.getValues(Ext.create('SysX.model.ClientUser').status)
				})
			}, {
				xtype : 'displayfield',
				fieldLabel : SysX.t.ApiKey,
				name : 'api_key',
				itemId : 'api_key',
				emptyText : SysX.t.ClientUserMessage_NoAPIKey
			}, {
				xtype : 'button',
				text : SysX.t.GenerateNewAPIKey,
				handler : function() {
					Ext.Ajax.request({
						url : modulePath + '/Client-User/Generate-New-Api-Key',
						method : 'POST',
						scope : this,
						success : function(response, opts) {
							var result = Ext.decode(response.responseText);
							this.up('form').down('displayfield[itemId=api_key]').setValue(result.data[0].api_key);
						}
					});
				}
			}]
		};

		this.callParent(arguments);
	}
});

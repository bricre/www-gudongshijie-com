Ext.define('SysX.view.client.Product_ExternalShared.Grid', {
	extend : 'SysX.view.client.Product.Grid',
	itemId : 'client.Product_ExternalShared.Grid',
	store : 'SysX.store.Product_ExternalShared',
	title : SysX.t.PublicProduct,
	disableTopToolbar:true
});

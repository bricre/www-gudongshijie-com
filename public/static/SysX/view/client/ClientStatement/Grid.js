Ext.define('SysX.view.client.ClientStatement.Grid', {
	extend : 'SysX.view.abstract.Grid',
	itemId : 'client.ClientStatement.Grid',
	store : 'SysX.store.ClientStatement',
	title : SysX.t.ClientStatement,
	disableItemDbClick : true,
	disableInsertButton : true,
	disableDeleteButton : true,
	selModel : null,
	columns : [{
		header : SysX.t.Time,
		dataIndex : 'update_time',
		sortable : true,
		xtype : 'datecolumn',
		format : 'Y-m-d H:i:s',
		flex : 1,
		filter : {
			type : 'date',
			phpMode : true,
			dateFormat : 'Y-m-d',
			active : true,
			value : {
				after : Ext.Date.add(new Date(), Ext.Date.MONTH, -1)
			}
		}
	}, {
		header : SysX.t.Type,
		dataIndex : 'type',
		flex : 1,
		filter : {
			type : 'list',
			options : Ext.Object.getValues(Ext.create('SysX.model.ClientStatement').type)
		},
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			if (undefined === SysX.model.ClientStatement) {
				Ext.create('SysX.model.ClientStatement');
			}
			return SysX.model.ClientStatement.prototype.type[record.data.type][1];
		}
	}, {
		header : SysX.t.Note,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			var data = null;
			switch(record.data.type) {
				case 'CONSIGNMENT_FEE':
					data = SysX.t.ClientStatementType_CONSIGNMENT_FEE + ' ' + record.data.record_id;
					break;
				case 'WAREHOUSE_DISPATCH_FEE':
					data = SysX.t.ClientStatementType_WAREHOUSE_DISPATCH_FEE + ' ' + record.data.record_id;
					break;
				case 'MEMBERSHIP_FEE':
					data = SysX.t.ClientStatementType_MEMBERSHIP_FEE;
					break;
				case 'TOP_UP':
					data = SysX.t.ClientStatementType_TOP_UP;
					break;
				case 'ADJUSTMENT':
					data = SysX.t.ClientStatementType_ADJUSTMENT;
					break;
				case 'REFUND':
					data = SysX.t.ClientStatementType_REFUND + record.data.record_id;
					break;
				case 'CLIENT_TRANSFER':
					data = (record.data.amount > 0 ? SysX.t.From : SysX.t.To) + ' ' + SysX.t.ClientID + ' ' + record.data.record_id;
					break;
				default:
					data = record.data.note;
					break;
			}

			if (record.data.note.length > 0) {
				data += ' (' + record.data.note + ')';
			}

			return data;
		},
		flex : 4
	}, {
		header : SysX.t.Amount,
		align : 'right',
		dataIndex : 'amount',
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			if (value > 0) {
				metaData.tdAttr = 'style="color:green;"';
			} else {
				metaData.tdAttr = 'style="color:red;"';
			}
			return Ext.util.Format.number(value, '0,0.00');
		},
		flex : 1,
		filter : true
	}, {
		dataIndex : 'id',
		hidden : true
	}, {
		dataIndex : 'client_id',
		hidden : true
	}, {
		dataIndex : 'record_id',
		hidden : true
	}, {
		dataIndex : 'note',
		hidden : true
	}],
	defaultSorters : [{
		dataIndex : 'update_time',
		order : 'DESC'
	}],
	topToolbarExtraItems : [{
		xtype : 'button',
		text : SysX.t.AddClientTransferRecord,
		handler : function(button) {
			var ClientTransferRecordWindow = Ext.create('SysX.view.abstract.ClientStatement.ClientTransferRecordWindow'), grid = button.up('grid');

			ClientTransferRecordWindow.on('close', function(view) {
				grid.getDockedComponent('pagingtoolbar').doRefresh();
			});
		}
	}]
});

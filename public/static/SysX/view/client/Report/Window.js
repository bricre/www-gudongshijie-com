Ext.define('SysX.view.client.Report.Window', {
	extend : 'SysX.view.abstract.ReportWindow',
	reports : [{
		leaf : false,
		expanded : true,
		text : SysX.t.Account,
		children : [{
			leaf : true,
			report : 'clientDispatch',
			text : SysX.t.Report_ClientDispatch,
			criterias : [{
				xtype : 'tbtext',
				text : SysX.t.From
			}, {
				xtype : 'datetimefield',
				name : 'dateFrom',
				value : Ext.Date.add(new Date(), Ext.Date.MONTH, -1),
				format : 'Y-m-d'
			}, {
				xtype : 'tbtext',
				text : SysX.t.To
			}, {
				xtype : 'datetimefield',
				name : 'dateTo',
				value : Ext.Date.add(new Date(), Ext.Date.DAY, 1),
				format : 'Y-m-d'
			}]
		}, {
			leaf : true,
			report : 'clientStatement',
			text : SysX.t.Report_ClientStatement,
			criterias : [{
				xtype : 'tbtext',
				text : SysX.t.From
			}, {
				xtype : 'datetimefield',
				name : 'dateFrom',
				value : Ext.Date.add(new Date(), Ext.Date.MONTH, -1),
				format : 'Y-m-d'
			}, {
				xtype : 'tbtext',
				text : SysX.t.To
			}, {
				xtype : 'datetimefield',
				name : 'dateTo',
				value : Ext.Date.add(new Date(), Ext.Date.DAY, 1),
				format : 'Y-m-d'
			}, '-', {
				xtype : 'tbtext',
				text : SysX.t.Type
			}, {
				xtype : 'combo',
				name : 'type',
				store : Ext.Object.getValues(Ext.Object.merge({
					'NULL' : [null, 'N/A']
				}, Ext.create('SysX.model.ClientStatement').type))
			}]
		}, {
			leaf : true,
			report : 'clientConsumption',
			text : SysX.t.Report_ClientConsumption,
			criterias : [{
				xtype : 'tbtext',
				text : SysX.t.From
			}, {
				xtype : 'datetimefield',
				name : 'dateFrom',
				value : Ext.Date.clearTime(Ext.Date.add(new Date(), Ext.Date.MONTH, -1)),
				format : 'Y-m-d'
			}, {
				xtype : 'tbtext',
				text : SysX.t.To
			}, {
				xtype : 'datetimefield',
				name : 'dateTo',
				value : Ext.Date.clearTime(Ext.Date.add(new Date(), Ext.Date.DAY, 0)),
				format : 'Y-m-d'
			}]
		}]
	}, {
		leaf : false,
		expanded : true,
		text : SysX.t.Consignment,
		children : [{
			leaf : true,
			report : 'consignmentAnalysis',
			text : SysX.t.Report_ConsignmentAnalysis,
			criterias : [{
				xtype : 'tbtext',
				text : SysX.t.From
			}, {
				xtype : 'datetimefield',
				name : 'dateFrom',
				value : Ext.Date.add(new Date(), Ext.Date.MONTH, -1),
				format : 'Y-m-d'
			}, {
				xtype : 'tbtext',
				text : SysX.t.To
			}, {
				xtype : 'datetimefield',
				name : 'dateTo',
				value : Ext.Date.add(new Date(), Ext.Date.DAY, 1),
				format : 'Y-m-d'
			}]
		}, {
			leaf : true,
			report : 'consignmentAnalysisByService',
			text : SysX.t.Report_ConsignmentAnalysisByService,
			criterias : [{
				xtype : 'tbtext',
				text : SysX.t.From
			}, {
				xtype : 'datetimefield',
				name : 'dateFrom',
				value : Ext.Date.add(new Date(), Ext.Date.DAY, 0),
				format : 'Y-m-d'
			}, {
				xtype : 'tbtext',
				text : SysX.t.To
			}, {
				xtype : 'datetimefield',
				name : 'dateTo',
				value : Ext.Date.add(new Date(), Ext.Date.DAY, +1),
				format : 'Y-m-d'
			}]
		}, {
			leaf : true,
			report : 'consignmentAnalysisByDeliveryPackageSize',
			text : SysX.t.Report_ConsignmentAnalysisByDeliveryPackageSize,
			criterias : [{
				xtype : 'tbtext',
				text : SysX.t.Report_From
			}, {
				xtype : 'datetimefield',
				name : 'dateFrom',
				value : Ext.Date.clearTime(Ext.Date.add(new Date(), Ext.Date.DAY, 0)),
				format : 'Y-m-d'
			}, {
				xtype : 'tbtext',
				text : SysX.t.Report_To
			}, {
				xtype : 'datetimefield',
				name : 'dateTo',
				value : Ext.Date.clearTime(Ext.Date.add(new Date(), Ext.Date.DAY, +1)),
				format : 'Y-m-d'
			}]
		}, {
			leaf : true,
			report : 'consignmentDetail',
			text : SysX.t.Report_ConsignmentDetails,
			criterias : [{
				xtype : 'tbtext',
				text : SysX.t.ProductID
			}, {
				xtype : 'numberfield',
				name : 'product_id',
				width : 100
			}, {
				xtype : 'tbtext',
				text : SysX.t.From
			}, {
				xtype : 'datetimefield',
				name : 'dateFrom',
				value : Ext.Date.add(new Date(), Ext.Date.DAY, 0),
				format : 'Y-m-d'
			}, {
				xtype : 'tbtext',
				text : SysX.t.To
			}, {
				xtype : 'datetimefield',
				name : 'dateTo',
				value : Ext.Date.add(new Date(), Ext.Date.DAY, +1),
				format : 'Y-m-d'
			}]
		}]
	}, {
		leaf : false,
		expanded : true,
		text : SysX.t.Product,
		children : [{
			leaf : true,
			report : 'stockStatus',
			text : SysX.t.Report_StockStatus,
			criterias : [{
				xtype : 'tbtext',
				text : SysX.t.Report_OrderBy
			}, {
				xtype : 'radiogroup',
				name : 'orderBy',
				width : 200,
				items : [{
					boxLabel : SysX.t.ID,
					name : 'orderBy',
					inputValue : 'ID',
					checked : true
				}, {
					boxLabel : SysX.t.Name,
					name : 'orderBy',
					inputValue : 'Name'
				}, {
					boxLabel : SysX.t.Quantity,
					name : 'orderBy',
					width : 100,
					inputValue : 'Live Stock'
				}]
			}, {
				xtype : 'tbtext',
				text : SysX.t.IdRange
			}, {
				name : 'idFrom',
				xtype : 'numberfield'
			}, {
				xtype : 'tbtext',
				text : '-'
			}, {
				name : 'idTo',
				xtype : 'numberfield'
			}]
		}, {
			leaf : true,
			report : 'productDetail',
			text : SysX.t.Report_ProductDetail,
			criterias : [{
				xtype : 'tbtext',
				text : SysX.t.IdRange
			}, {
				name : 'id_from',
				xtype : 'numberfield'
			}, {
				xtype : 'tbtext',
				text : '-'
			}, {
				name : 'id_to',
				xtype : 'numberfield'
			}, {
				xtype : 'tbtext',
				text : SysX.t.LiveStock
			}, {
				name : 'live_stock_from',
				xtype : 'numberfield'
			}, {
				xtype : 'tbtext',
				text : '-'
			}, {
				name : 'live_stock_to',
				xtype : 'numberfield'
			}]
		}, {
			leaf : true,
			report : 'productInStock',
			text : SysX.t.Report_ProductInStock,
			criterias : [{
				xtype : 'tbtext',
				text : SysX.t.Report_GroupBy
			}, {
				xtype : 'radiogroup',
				name : 'groupBy',
				width : 300,
				items : [{
					boxLabel : SysX.t.Report_DoNotGroup,
					name : 'groupBy',
					inputValue : 0,
					checked : true
				}, {
					boxLabel : SysX.t.ContainerID,
					name : 'groupBy',
					inputValue : 'container_id'
				}, {
					boxLabel : SysX.t.ProductID,
					name : 'groupBy',
					inputValue : 'product_id'
				}]
			}, {
				name : 'type',
				fieldLabel : SysX.t.Type,
				allowBlank : true,
				xtype : 'combo',
				valueField : 'value',
				displayField : 'text',
				queryMode : 'local',
				forceSelection : true,
				triggerAction : 'all',
				editable : false,
				labelWidth : 50,
				store : new Ext.data.ArrayStore({
					fields : ['value', 'text'],
					data : ( function() {
							Ext.require('SysX.model.ContainerProductTransaction');
							return Ext.Object.getValues(SysX.model.ContainerProductTransaction.type);
						}())
				})
			}, '---', {
				xtype : 'tbtext',
				text : SysX.t.From
			}, {
				xtype : 'datetimefield',
				name : 'dateFrom',
				value : Ext.Date.add(new Date(), Ext.Date.MONTH, -1),
				format : 'Y-m-d'
			}, {
				xtype : 'tbtext',
				text : SysX.t.To
			}, {
				xtype : 'datetimefield',
				name : 'dateTo',
				value : Ext.Date.add(new Date(), Ext.Date.DAY, 1),
				format : 'Y-m-d'
			}, {
				xtype : 'tbtext',
				text : SysX.t.IdRange
			}, {
				name : 'idFrom',
				xtype : 'numberfield'
			}, {
				xtype : 'tbtext',
				text : '-'
			}, {
				name : 'idTo',
				xtype : 'numberfield'
			}]
		}, {
			leaf : true,
			report : 'productOutStock',
			text : SysX.t.Report_ProductOutStock,
			criterias : [{
				xtype : 'tbtext',
				text : SysX.t.Report_GroupBy
			}, {
				xtype : 'radiogroup',
				name : 'groupBy',
				width : 300,
				items : [{
					boxLabel : SysX.t.Report_DoNotGroup,
					name : 'groupBy',
					inputValue : 0,
					checked : true
				}, {
					boxLabel : SysX.t.ContainerID,
					name : 'groupBy',
					inputValue : 'container_id'
				}, {
					boxLabel : SysX.t.ProductID,
					name : 'groupBy',
					inputValue : 'product_id'
				}]
			}, {
				name : 'type',
				fieldLabel : SysX.t.Type,
				allowBlank : true,
				xtype : 'combo',
				valueField : 'value',
				displayField : 'text',
				queryMode : 'local',
				forceSelection : true,
				triggerAction : 'all',
				editable : false,
				labelWidth : 50,
				store : new Ext.data.ArrayStore({
					fields : ['value', 'text'],
					data : ( function() {
							Ext.require('SysX.model.ContainerProductTransaction');
							return Ext.Object.getValues(SysX.model.ContainerProductTransaction.type);
						}())
				})
			}, '---', {
				xtype : 'tbtext',
				text : SysX.t.From
			}, {
				xtype : 'datetimefield',
				name : 'dateFrom',
				value : Ext.Date.add(new Date(), Ext.Date.MONTH, -1),
				format : 'Y-m-d'
			}, {
				xtype : 'tbtext',
				text : SysX.t.To
			}, {
				xtype : 'datetimefield',
				name : 'dateTo',
				value : Ext.Date.add(new Date(), Ext.Date.DAY, 1),
				format : 'Y-m-d'
			}, {
				xtype : 'tbtext',
				text : SysX.t.IdRange
			}, {
				name : 'idFrom',
				xtype : 'numberfield'
			}, {
				xtype : 'tbtext',
				text : '-'
			}, {
				name : 'idTo',
				xtype : 'numberfield'
			}]
		}, {
			leaf : true,
			report : 'productPerformance',
			text : SysX.t.Report_ProductPerformance,
			criterias : [{
				xtype : 'tbtext',
				text : SysX.t.From
			}, {
				xtype : 'datetimefield',
				name : 'dateFrom',
				value : Ext.Date.add(new Date(), Ext.Date.MONTH, -1),
				format : 'Y-m-d'
			}, {
				xtype : 'tbtext',
				text : SysX.t.To
			}, {
				xtype : 'datetimefield',
				name : 'dateTo',
				value : Ext.Date.add(new Date(), Ext.Date.DAY, 1),
				format : 'Y-m-d'
			}, {
				xtype : 'tbtext',
				text : SysX.t.IdRange
			}, {
				name : 'idFrom',
				xtype : 'numberfield'
			}, {
				xtype : 'tbtext',
				text : '-'
			}, {
				name : 'idTo',
				xtype : 'numberfield'
			}]
		}]
	}]
});

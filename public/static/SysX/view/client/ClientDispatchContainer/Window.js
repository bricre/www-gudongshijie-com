Ext.define('SysX.view.client.ClientDispatchContainer.Window', {
	extend : 'SysX.view.abstract.Window',
	itemId : 'client.ClientDispatchContainer.Window',
	initComponent : function() {
		this.formItems = {
			title : SysX.t.BasicDetails,
			layout : 'form',
			items : [{
				name : 'id',
				fieldLabel : SysX.t.ID,
				allowBlank : true,
				xtype : 'hidden'
			}, {
				itemId : 'ClientDispatchContainerBarcode',
				fieldLabel : SysX.t.Barcode,
				xtype : 'displayfield',
				tpl : Ext.create('Ext.XTemplate','<tpl for=".">' + '<a href="{src}" target="_blank"><img src="{src}" title="Barcode-{id}" width="140" height="70" /></a>' + '</tpl>')
			}, {
				name : 'client_dispatch_id',
				fieldLabel : SysX.t.ClientDispatch,
				allowBlank : true,
				xtype : 'hidden'
			}, {
				name : 'reference',
				fieldLabel : SysX.t.Reference,
				allowBlank : true,
				xtype : 'textfield'
			}, {
				name : 'length',
				fieldLabel : SysX.t.Length,
				allowBlank : true,
				xtype : 'textfield'
			}, {
				name : 'width',
				fieldLabel : SysX.t.Width,
				allowBlank : true,
				xtype : 'textfield'
			}, {
				name : 'depth',
				fieldLabel : SysX.t.Depth,
				allowBlank : true,
				xtype : 'textfield'
			}, {
				name : 'weight',
				fieldLabel : SysX.t.Weight,
				allowBlank : true,
				xtype : 'textfield'
			}]
		};

		this.callParent(arguments);
	}
});

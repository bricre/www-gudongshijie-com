Ext.define('SysX.view.client.ClientDispatchContainer.Grid', {
	extend : 'SysX.view.abstract.Grid',
	alias : 'widget.ClientDispatchContainer.Grid',
	store : 'SysX.store.ClientDispatchContainer',
	title : SysX.t.Container,
	columns : [{
		dataIndex : 'client_dispatch_id',
		hidden : true
	}, {
		header : SysX.t.ID,
		dataIndex : 'id',
		flex : 1
	}, {
		header : SysX.t.Reference,
		dataIndex : 'reference',
		flex : 2
	}, {
		header : SysX.t.Length,
		dataIndex : 'length',
		flex : 1,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			if (record.data.length !== record.data.length_check) {
				metaData.tdAttr = 'style="color:red;"';
				return record.data.length + ' (' + record.data.length_check + ')';
			} else {
				return record.data.length;
			}
		}
	}, {
		header : SysX.t.Width,
		dataIndex : 'width',
		flex : 1,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			if (record.data.width !== record.data.width_check) {
				metaData.tdAttr = 'style="color:red;"';
				return record.data.width + ' (' + record.data.width_check + ')';
			} else {
				return record.data.width;
			}
		}
	}, {
		header : SysX.t.Depth,
		dataIndex : 'depth',
		flex : 1,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			if (record.data.depth !== record.data.depth_check) {
				metaData.tdAttr = 'style="color:red;"';
				return record.data.depth + ' (' + record.data.depth_check + ')';
			} else {
				return record.data.depth;
			}
		}
	}, {
		header : SysX.t.Weight,
		dataIndex : 'weight',
		flex : 1,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			if (record.data.weight !== record.data.weight_check) {
				metaData.tdAttr = 'style="color:red;"';
				return record.data.weight + ' (' + record.data.weight_check + ')';
			} else {
				return record.data.weight;
			}
		}
	}, {
		dataIndex : 'length_check',
		hidden : true
	}, {
		dataIndex : 'width_check',
		hidden : true
	}, {
		dataIndex : 'depth_check',
		hidden : true
	}, {
		dataIndex : 'weight_check',
		hidden : true
	}]
});

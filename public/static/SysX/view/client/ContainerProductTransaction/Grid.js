Ext.define('SysX.view.client.ContainerProductTransaction.Grid', {
	extend : 'SysX.view.abstract.Grid',
	itemId : 'client.ContainerProductTransaction.Grid',
	store : 'SysX.store.ContainerProductTransaction',
	title : SysX.t.InventoryHistory,
	disableItemDbClick : true,
	disableTopToolbar : true,
	columns : [{
		header : SysX.t.ID,
		dataIndex : 'id',
		sortable : true,
		flex : 1
	}, {
		header : SysX.t.Container,
		dataIndex : 'container_id',
		sortable : true,
		flex : 1,
		filter : true
	}, {
		header : SysX.t.Product,
		dataIndex : 'product_id',
		sortable : true,
		flex : 1,
		filter : true
	}, {
		header : SysX.t.Quantity,
		dataIndex : 'quantity',
		sortable : true,
		filter : true,
		flex : 0.5
	}, {
		header : SysX.t.Note,
		dataIndex : 'note',
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			var data = '';
			if (record.data.consignment_id) {
				data = SysX.t.Consignment + ' - ' + record.data.consignment_id;
			}

			if (record.data.warehouse_dispatch_id) {
				data = SysX.t.WarehouseDispatch + ' - ' + record.data.warehouse_dispatch_id;
			}

			if (record.data.note.length > 0) {
				data += ' (' + record.data.note + ')';
			}

			return data;
		},
		flex : 3
	}, {
		header : SysX.t.Type,
		dataIndex : 'type',
		flex : 1,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			var type = SysX.model.ContainerProductTransaction.type;
			metaData.tdAttr = type[value][2];
			return type[value][1];
		}
	}, {
		header : SysX.t.LastUpdate,
		dataIndex : 'update_time',
		xtype : 'datecolumn',
		sortable : true,
		align : 'right',
		filter : {
			dateFormat : 'Y-m-d',
			active : true,
			value : {
				after : Ext.Date.add(new Date(), Ext.Date.MONTH, -1)
			}
		},
		flex : 1
	}, {
		dataIndex : 'consignment_id',
		hidden : true
	}, {
		dataIndex : 'warehouse_dispatch_id',
		hidden : true
	}],
	defaultSorters : [{
		dataIndex : 'update_time',
		order : 'DESC'
	}]
});

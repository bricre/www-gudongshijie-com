Ext.define('SysX.view.client.ContainerProductTransaction.Window', {
	extend : 'SysX.view.abstract.Window',
	itemId : 'client.ContainerProductTransaction.Window',
	height:250,
	width:300,
	initComponent : function() {
		this.formItems = {
			title : 'Details',

			items : [{
				name : 'id',
				xtype : 'hidden'
			}, {
				name : 'container_id',
				fieldLabel : 'Container',
				xtype : 'numberfield',
				id : 'ContainerProductTransaction_container_id',
				allowBlank : false
			}, {
				name : 'product_id',
				fieldLabel : 'Product',
				xtype : 'numberfield',
				id : 'ContainerProductTransaction_product_id',
				allowBlank : false
			}, {
				name : 'quantity',
				fieldLabel : 'Quantity',
				xtype : 'numberfield',
				allowBlank : false
			}, {
				name : 'client_id',
				fieldLabel : 'Client ID',
				xtype : 'numberfield'
			}]
		};

		this.callParent(arguments);
	}
}); 
Ext.define('SysX.view.client.Consignment_Return.Window', {
	extend : 'SysX.view.client.Consignment.Window',
	title : SysX.t.ReturnConsignment,
	onAfterInitComponent : function() {
		this.down('form').getForm().setValues({
			type : 'RETURN'
		});

		this.down('form').down('combo[name=delivery_service_id]').getStore().addFilter({
			property : 'consignment_type',
			value : 'RETURN'
		}, false);
	}
});

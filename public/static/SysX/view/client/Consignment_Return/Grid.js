Ext.define('SysX.view.client.Consignment_Return.Grid', {
	extend : 'SysX.view.client.Consignment.Grid',
	itemId : 'client.Consignment_Return.Grid',
	title : SysX.t.ReturnConsignment,
	onBeforeInitComponent : function() {
		this.getStore().proxy.extraParams.type = 'RETURN';

		Ext.Array.findBy(this.columns, function(item) {
			return item.dataIndex === 'delivery_service_id' ? true : false;
		}).filter.store = Ext.create('SysX.store.DeliveryService', {
			storeId : Ext.id(),
			filters : [{
				property : 'status',
				value : 'ACTIVE'
			}, {
				property : 'consignment_type',
				value : 'RETURN'
			}]
		});
	},
	onAfterInitComponent : function(view) {
		this.callParent(arguments);

		this.down('toolbar[dock=top]').down('combo[itemId=BatchChangeDeliveryServiceCombo]').getStore().addFilter({
			property : 'consignment_type',
			value : 'RETURN'
		}, false);
	}
});

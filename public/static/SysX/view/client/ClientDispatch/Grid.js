Ext.define('SysX.view.client.ClientDispatch.Grid', {
	extend : 'SysX.view.abstract.Grid',
	alias : 'widget.ClientDispatch.Grid',
	store : 'SysX.store.ClientDispatch',
	title : SysX.t.ClientDispatch,
	columns : [{
		header : SysX.t.ID,
		dataIndex : 'id',
		sortable : true,
		flex : 1,
		filter : true
	}, {
		header : SysX.t.Warehouse,
		dataIndex : 'warehouse_id',
		sortable : true,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			return record.data.warehouse_name;
		},
		flex : 2
	}, {
		header : SysX.t.Reference,
		dataIndex : 'reference',
		flex : 2,
		filter : true
	}, {
		header : SysX.t.FinalFreightMethod,
		dataIndex : 'final_freight_method',
		flex : 1,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			if (value) {
				var status = SysX.model.ClientDispatch.prototype.finalFreightMethod;
				return status[value][1];
			}
		},
		filter : {
			type : 'list',
			options : Ext.Object.getValues(Ext.create('SysX.model.ClientDispatch').finalFreightMethod)
		}
	}, {
		header : SysX.t.FreightBatch,
		dataIndex : 'warehouse_dispatch_freight_id',
		flex : 1,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			return record.data.warehouse_dispatch_freight_reference;
		},
		filter : {
			type : 'list',
			labelField : 'reference',
			store : Ext.create('SysX.store.WarehouseDispatchFreight', {
				sorters : [{
					property : 'id',
					direction : 'DESC'
				}],
				filters : [{
					property : 'status',
					value : ['PREPARING', 'DISPATCHED', 'RECEIVED']
				}]
			})
		}
	}, {
		header : SysX.t.DeliveryReference,
		dataIndex : 'delivery_reference',
		flex : 2,
		filter : true
	}, {
		header : SysX.t.DeliveryTime,
		dataIndex : 'delivery_time',
		flex : 2
	}, {
		header : SysX.t.ReceiveTime,
		dataIndex : 'receive_time',
		flex : 2
	}, {
		header : SysX.t.Note,
		dataIndex : 'note',
		flex : 2
	}, {
		header : SysX.t.Status,
		dataIndex : 'status',
		sortable : true,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			var status = SysX.model.ClientDispatch.prototype.status;
			metaData.tdAttr = status[value][2];
			return status[value][1];
		},
		flex : 1
	}],
	defaultSorters : [{
		dataIndex : 'id',
		order : 'DESC'
	}]
});

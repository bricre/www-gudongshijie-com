Ext.define('SysX.view.client.ClientDispatch.Window', {
	extend : 'SysX.view.abstract.Window',
	itemId : 'client.ClientDispatch.Window',
	initComponent : function() {
		this.formItems = {
			title : SysX.t.BasicDetails,
			layout : 'form',
			defaults : {
				labelWidth : 200
			},
			items : [{
				name : 'id',
				fieldLabel : SysX.t.ID,
				allowBlank : true,
				xtype : 'hidden',
				id : 'ClientDispatchId'
			}, {
				xtype : 'button',
				text : SysX.t.UploadDispatchFileCSV,
				hidden : true,
				itemId : 'ClientDispatchFileUploadButton',
				listeners : {
					scope : this,
					'click' : function(button) {
						var form = button.up('form'), config = {
							id : this.id + 'FileUploadWindow',
							title : SysX.t.FileUpload,
							width : 350,
							height : 120,
							maximizable : false,
							modal : true,
							items : [{
								id : this.id + 'FileUploadFormPanel',
								xtype : 'form',
								fileUpload : true,
								bodyStyle : 'padding:5px',
								items : [{
									name : 'client_dispatch_id',
									xtype : 'hidden',
									id : 'ClientDispatchFileUploadWindowClientDispatchId'
								}, {
									name : 'userfile',
									xtype : 'fileuploadfield',
									fieldLabel : 'File',
									buttonText : SysX.t.Browse
								}],
								buttons : [{
									text : SysX.t.Cancel,
									scope : this,
									handler : function(button) {
										button.up('window').destroy();
									}
								}, {
									text : SysX.t.Upload,
									scope : this,
									handler : function(button) {
										var form = button.up('form');
										if (form.getForm().isValid()) {
											form.getForm().submit({
												scope : this,
												url : modulePath + '/Upload/Client-Dispatch',
												success : function(form, action) {
													Ext.Msg.alert(SysX.t.MessageBoxTitle_Success, SysX.t.FileUploadMessage_Success);
													form.owner.up('window').destroy();
												},
												failure : function(form, action) {
													Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, action.result.message);
												}
											});
										}
									}
								}]
							}],
							listeners : {
								afterrender : function(component) {
									component.down('hidden[name=client_dispatch_id]').setValue(form.down('hidden[name=id]').getValue());
								}
							}
						};
						FileUploadWindow = new Ext.Window(config);
						FileUploadWindow.show();
					}
				}
			}, {
				name : 'warehouse_id',
				fieldLabel : SysX.t.Warehouse,
				allowBlank : false,
				xtype : 'combo',
				valueField : 'id',
				displayField : 'name',
				queryMode : 'local',
				editable : false,
				store : Ext.create('SysX.store.Warehouse', {
					autoLoad : true
				})
			}, {
				name : 'warehouse_id_final',
				fieldLabel : SysX.t.FinalWarehouse,
				allowBlank : false,
				xtype : 'combo',
				valueField : 'id',
				displayField : 'name',
				queryMode : 'local',
				editable : false,
				store : Ext.create('SysX.store.Warehouse', {
					autoLoad : true
				})
			}, {
				name : 'reference',
				fieldLabel : SysX.t.Reference,
				xtype : 'textfield',
				anchor : '100%'
			}, {
				name : 'final_freight_method',
				fieldLabel : SysX.t.FinalFreightMethod,
				allowBlank : true,
				xtype : 'combo',
				value : 'EXPRESS',
				valueField : 'value',
				displayField : 'text',
				queryMode : 'local',
				forceSelection : true,
				triggerAction : 'all',
				editable : false,
				store : new Ext.data.ArrayStore({
					fields : ['value', 'text'],
					data : Ext.Object.getValues(Ext.create('SysX.model.WarehouseDispatch').finalFreightMethod)
				}),
				listeners : {
					select : function(combo, records) {
						var record = records[0], warehouseDispatchFreightIdCombo = combo.up('form').down('combo[name=warehouse_dispatch_freight_id]');
						if ('EXPRESS' === record.data.value) {
							warehouseDispatchFreightIdCombo.setVisible(false);
							warehouseDispatchFreightIdCombo.setRawValue('NULL');
						} else {
							warehouseDispatchFreightIdCombo.setVisible(true);
							warehouseDispatchFreightIdCombo.getStore().load();
						}
					}
				}
			}, {
				name : 'warehouse_dispatch_freight_id',
				fieldLabel : SysX.t.FreightBatch,
				allowBlank : true,
				hidden : true,
				xtype : 'combo',
				valueField : 'id',
				displayField : 'reference',
				queryMode : 'remote',
				forceSelection : true,
				triggerAction : 'all',
				editable : false,
				pageSize : true,
				store : Ext.create('SysX.store.WarehouseDispatchFreight', {
					autoLoad : true,
					pageSize : 20,
					sorters : [{
						property : 'id',
						direction : 'DESC'
					}]
				}),
				listeners : {
					beforeshow : function() {
						this.getStore().on('beforeload', function(store, operation) {
							store.proxy.extraParams.method = this.up('form').down('combo[name=final_freight_method]').getValue();
						}, this);
					}
				}
			}, {
				name : 'delivery_reference',
				fieldLabel : SysX.t.DeliveryReference,
				xtype : 'textfield',
				anchor : '100%'
			}, {
				name : 'delivery_time',
				fieldLabel : SysX.t.DeliveryTime,
				format : 'Y-m-d',
				xtype : 'datetimefield'
			}, {
				name : 'receive_time',
				fieldLabel : SysX.t.ReceiveTime,
				xtype : 'displayfield'
			}, {
				name : 'warehouse_dispatch_delivery_reference',
				fieldLabel : SysX.t.WarehouseDispatchDeliveryReference,
				xtype : 'displayfield'
			}, {
				name : 'warehouse_dispatch_delivery_dispatch_time',
				fieldLabel : SysX.t.WarehouseDispatchDeliveryDispatchTime,
				xtype : 'displayfield'
			}, {
				name : 'warehouse_dispatch_delivery_arrive_time',
				fieldLabel : SysX.t.WarehouseDispatchDeliveryArriveTime,
				xtype : 'displayfield'
			}, {
				name : 'warehouse_dispatch_stock_enter_time',
				fieldLabel : SysX.t.WarehouseDispatchStockEnterTime,
				xtype : 'displayfield'
			}, {
				name : 'importer_number',
				fieldLabel : SysX.t.ImporterNumber,
				xtype : 'displayfield'
			}, {
				name : 'vat_fee',
				fieldLabel : SysX.t.VatFee,
				xtype : 'displayfield'
			}, {
				name : 'import_duty_fee',
				fieldLabel : SysX.t.ImportDutyFee,
				xtype : 'displayfield'
			}, {
				name : 'note',
				fieldLabel : SysX.t.Note,
				xtype : 'htmleditor',
				anchor : '100%'
			}, {
				name : 'status',
				fieldLabel : SysX.t.Status,
				allowBlank : false,
				xtype : 'combo',
				value : 'PREPARING',
				valueField : 'value',
				displayField : 'text',
				queryMode : 'local',
				forceSelection : true,
				triggerAction : 'all',
				editable : false,
				store : new Ext.data.ArrayStore({
					fields : ['value', 'text', 'attr'],
					data : Ext.Object.getValues(Ext.create('SysX.model.ClientDispatch').status)
				})
			}, {
				xtype : 'button',
				hidden : true,
				text : SysX.t.PrintPackingList,
				itemId : 'ClientDispatchPrintPackingListButton',
				handler : function() {
					SysX.openNewWindow(modulePath + '/Printer/Client-Dispatch-Packing-List/' + Ext.getCmp('ClientDispatchId').getValue());
				}
			}]
		};

		this.callParent(arguments);
	}
});

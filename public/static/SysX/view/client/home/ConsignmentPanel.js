Ext.define('SysX.view.client.home.ConsignmentPanel', {
	extend : 'Ext.panel.Panel',
	title : SysX.t.Consignment + SysX.t.InLastXDays,
	animate : true,
	layout : 'fit',
	minHeight : 200,
	requires : ['SysX.model.Consignment'],
	initComponent : function() {
		this.items = [{
			xtype : 'chart',
			store : Ext.create('Ext.data.JsonStore', {
				autoLoad : true,
				fields : [{
					name : 'status',
					type : 'string',
					convert : function(value, record) {
						var status = SysX.model.Consignment.prototype.status;
						return status[value][1];
					}
				}, {
					name : 'quantity',
					type : 'int'
				}],
				proxy : {
					url : modulePath + '/Client/Consignment-Summary',
					type : 'rest',
					reader : {
						type : 'json',
						root : 'data',
						idProperty : 'status'
					}
				}
			}),
			axes : [{
				type : 'Numeric',
				position : 'bottom',
				fields : ['quantity'],
				label : {
					renderer : Ext.util.Format.numberRenderer('0,0')
				},
				grid : true,
				minimum : 0
			}, {
				type : 'Category',
				position : 'left',
				fields : ['status']
			}],
			series : [{
				type : 'bar',
				axis : 'bottom',
				highlight : true,
				label : {
					display : 'insideEnd',
					field : 'quantity',
					renderer : Ext.util.Format.numberRenderer('0'),
					orientation : 'horizontal'
				},
				xField : 'status',
				yField : 'quantity'
			}]
		}];
		this.callParent();
	}
});


Ext.define('SysX.view.client.home.NewsPanel', {
	extend : 'Ext.panel.Panel',
	layout : {
		// layout-specific configs go here
		type : 'accordion',
		titleCollapse : true,
		animate : false,
		activeOnTop : false
	},
	title : SysX.t.HomePanel_LatestNews,
	initComponent : function() {
		var lists = [];
		this.feeds.forEach(function(element, index, array) {
			lists.push(Ext.create('SysX.view.client.RssViewer.Grid', {
				title : element.title,
				feed : element.url,
				listeners : {
					itemdblclick : function(rowModel, record) {
						console.log(record);
						window.open(record.get('id'));
					}
				}
			}));
		});
		this.items = lists;
		this.callParent();
	},
	feeds : [{
		title : '飞鸟公告',
		url : 'http://www.birdsystem.co.uk/blog/category/announcement/feed/'
	}, {
		title : '紧急通知（内部系统、外部投递）',
		url : 'http://www.birdsystem.co.uk/blog/category/special-notification/feed/'
	}, {
		title : '操作指南',
		url : 'http://www.birdsystem.co.uk/blog/category/tutorial/feed/'
	}, {
		title : '英仓订单操作说明',
		url : 'http://www.birdsystem.co.uk/blog/category/uk-warehouse-tutorial/feed/'
	}, {
		title : '清关申报要求和货物安全标准',
		url : 'http://www.birdsystem.co.uk/blog/category/product-standard/feed/'
	}, {
		title : '服务类型增加和改动',
		url : 'http://www.birdsystem.co.uk/blog/category/service-update/feed/'
	}, {
		title : '服务费用',
		url : 'http://www.birdsystem.co.uk/blog/category/service-fee/feed/'
	}, {
		title : '服务类型增加和改动',
		url : 'http://www.birdsystem.co.uk/blog/category/service-type/feed/'
	}, {
		title : '业务推广',
		url : 'http://www.birdsystem.co.uk/blog/category/service-promotion/feed/'
	}, {
		title : '专线服务',
		url : 'http://www.birdsystem.co.uk/blog/category/direct-mail-service/feed/'
	}, {
		title : '退货处理',
		url : 'http://www.birdsystem.co.uk/blog/category/return-service/feed/'
	}, {
		title : '系统论坛讨论帖',
		url : 'http://www.birdsystem.co.uk/blog/category/forum-post/feed/'
	}, {
		title : '放假通知',
		url : 'http://www.birdsystem.co.uk/blog/category/holiday-notification/feed/'
	}]
});

Ext.define('SysX.view.client.home.HomePanel', {
	extend : 'Ext.panel.Panel',
	title : SysX.t.HomePanel,
	layout : 'border',
	initComponent : function() {
		if (environment === 'development') {
			this.items = [];
		} else {
			this.items = [Ext.create('SysX.view.client.home.NewsPanel', {
				region : 'center'
			}), Ext.create('Ext.panel.Panel', {
				autoScroll : true,
				region : 'east',
				split : true,
				layout : 'anchor',
				width : 400,
				items : [Ext.create('SysX.view.client.home.AccountPanel', {
					anchor : '100% 25%'
				}), Ext.create('SysX.view.client.home.ProductPanel', {
					anchor : '100% 20%'
				}), Ext.create('SysX.view.client.home.ConsignmentPanel', {
					anchor : '100% 30%'
				}), Ext.create('SysX.view.client.home.ConsignmentPerformancePanel', {
					anchor : '100% 25%'
				})]
			})];
		}
		this.callParent();
	}
});

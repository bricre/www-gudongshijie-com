Ext.define('SysX.view.client.home.ProductPanel', {
	extend : 'Ext.panel.Panel',
	title : SysX.t.Product,
	animate : true,
	layout : 'fit',
	minHeight : 200,
	initComponent : function() {
		this.items = [{
			xtype : 'chart',
			store : Ext.create('Ext.data.JsonStore', {
				autoLoad : true,
				fields : [{
					name : 'name',
					type : 'string',
					convert : function(value, record) {
						var output;
						switch(value) {
							case 'live_stock':
								output = SysX.t.LiveStock;
								break;
							case 'client_dispatched_stock':
								output = SysX.t.ClientDispatchedStock;
								break;
							case 'warehouse_dispatched_stock':
								output = SysX.t.WarehouseDispatchedStock;
								break;
							case 'warehouse_received_stock':
								output = SysX.t.WarehouseReceivedStock;
								break;
							default:
								output = value;
								break;
						}
						return output;
					}
				}, {
					name : 'quantity',
					type : 'int'
				}],
				proxy : {
					url : modulePath + '/Client/Product-Summary',
					type : 'rest',
					reader : {
						type : 'json',
						root : 'data',
						idProperty : 'name'
					}
				}
			}),
			axes : [{
				type : 'Numeric',
				position : 'bottom',
				fields : ['quantity'],
				label : {
					renderer : Ext.util.Format.numberRenderer('0,0')
				},
				grid : true,
				minimum : 0
			}, {
				type : 'Category',
				position : 'left',
				fields : ['name']
			}],
			series : [{
				type : 'bar',
				axis : 'bottom',
				highlight : true,
				label : {
					display : 'insideEnd',
					field : 'quantity',
					renderer : Ext.util.Format.numberRenderer('0'),
					orientation : 'horizontal'
				},
				xField : 'name',
				yField : 'quantity'
			}]
		}];
		this.callParent();
	}
});

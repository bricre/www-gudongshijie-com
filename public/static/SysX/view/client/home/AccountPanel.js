Ext.define('SysX.view.client.home.AccountPanel', {
	extend : 'Ext.panel.Panel',
	title : SysX.t.Account,
	layout : 'fit',
	minHeight : 180,
	items : [{
		xtype : 'form',
		url : modulePath + '/Client/Account-Summary',
		method : 'GET',
		style : 'padding:5px',
		labelWidth : 100,
		frame : true,
		items : [{
			xtype : 'fieldcontainer',
			fieldLabel : SysX.t.HomePanel_WelcomeBack,
			layout : 'hbox',
			items : [{
				xtype : 'displayfield',
				name : 'first_name'
			}, {
				xtype : 'displayfield',
				html : ' '
			}, {
				xtype : 'displayfield',
				name : 'last_name'
			}]
		}, {
			xtype : 'displayfield',
			fieldLabel : SysX.t.ClientID,
			name : 'client_id'
		}, {
			xtype : 'displayfield',
			fieldLabel : SysX.t.CompanyName,
			name : 'name'
		}, {
			xtype : 'displayfield',
			fieldLabel : SysX.t.Balance,
			name : 'balance'
		}, {
			xtype : 'displayfield',
			fieldLabel : SysX.t.CreditLimit,
			name : 'credit_limit'
		}, {
			xtype : 'displayfield',
			fieldLabel : SysX.t.LastLoginTime,
			name : 'last_login_time'
		}]
	}],
	initComponent : function() {
		this.callParent();

		var form = this.down('form');
		form.on({
			beforeaction : function(form, action, options) {
				form.owner.setLoading(true, true);
			},
			actioncomplete : function(form, action, options) {
				form.owner.setLoading(false);
			},
			actionfailed : function(form, action, options) {
				form.owner.setLoading(false);
			}
		});
		form.load();
	}
});

Ext.define('SysX.view.client.ClientUserGroupPrivilege.Grid', {
	extend : 'SysX.view.abstract.Grid',
	itemId : 'client.ClientUserGroupPrivilege.Grid',
	store : 'SysX.store.ClientUserGroupPrivilege',
	title : SysX.t.UserGroupPrivilege,
	disableTopToolbar : true,
	disableItemDbClick : true,
	requires : ['Ext.ux.CheckColumn'],
	columns : [{
		dataIndex : 'id',
		hidden : true
	}, {
		dataIndex : 'client_user_group_id',
		hidden : true
	}, {
		header : SysX.t.PrivilegeCode,
		dataIndex : 'privilege_code',
		sortable : true,
		filter : true,
		flex : 3,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			var result;
			if (record.data.privilege_description.length > 0) {
				result = record.data.privilege_description;
			} else {
				result = Ext.String.capitalize(record.data.privilege_code.replace(/-/gi, ' ').replace(/\./gi, ' -> '));
			}
			return result;
		}
	}, {
		header : SysX.t.IsDenied,
		dataIndex : 'is_denied',
		xtype : 'checkcolumn',
		type : 'boolean',
		listeners : {
			checkchange : function(column, rowIndex, checked, eOpts) {
				var grid = this.ownerCt.ownerCt, store = grid.store, record = store.getAt(rowIndex);
				record.beginEdit();
				//Must use string '1' and '0' here, not number 1 and 0
				//This is because ConvertMapping from Model ClientUserGroupPrivilege only accept string returned by server
				if (checked) {
					record.set('is_allowed', '0');
				}
				record.set(column.dataIndex, checked ? '1' : '0');
				record.endEdit();
			}
		}
	}, {
		header : SysX.t.IsAllowed,
		dataIndex : 'is_allowed',
		xtype : 'checkcolumn',
		type : 'boolean',
		listeners : {
			checkchange : function(column, rowIndex, checked, eOpts) {
				var grid = this.ownerCt.ownerCt, store = grid.store, record = store.getAt(rowIndex);
				record.beginEdit();
				if (checked) {
					record.set('is_denied', '0');
				}
				record.set(column.dataIndex, checked ? '1' : '0');
				record.endEdit();
			}
		}
	}]
});

Ext.define('SysX.view.client.ClientDispatchContainerProduct.Grid', {
	extend : 'SysX.view.abstract.Grid',
	alias : 'widget.ClientDispatchContainerProduct.Grid',
	store : 'SysX.store.ClientDispatchContainerProduct',
	title : SysX.t.Products,
	columns : [{
		header : SysX.t.Container,
		dataIndex : 'client_dispatch_container_id',
		hidden : true
	}, {
		header : SysX.t.Product,
		dataIndex : 'product_id',
		flex : 1
	}, {
		header : SysX.t.Quantity,
		dataIndex : 'quantity',
		flex : 1,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			if (record.data.quantity != record.data.quantity_check) {
				metaData.tdAttr = 'style="color:red;"';
				return record.data.quantity + ' (' + record.data.quantity_check + ')';
			} else {
				return record.data.quantity;
			}
		}
	}, {
		header : SysX.t.Length,
		dataIndex : 'length',
		flex : 1,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			if (record.data.length !== record.data.length_check) {
				metaData.tdAttr = 'style="color:red;"';
				return record.data.length + ' (' + record.data.length_check + ')';
			} else {
				return record.data.length;
			}
		}
	}, {
		header : SysX.t.Width,
		dataIndex : 'width',
		flex : 1,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			if (record.data.width !== record.data.width_check) {
				metaData.tdAttr = 'style="color:red;"';
				return record.data.width + ' (' + record.data.width_check + ')';
			} else {
				return record.data.width;
			}
		}
	}, {
		header : SysX.t.Depth,
		dataIndex : 'depth',
		flex : 1,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			if (record.data.depth !== record.data.depth_check) {
				metaData.tdAttr = 'style="color:red;"';
				return record.data.depth + ' (' + record.data.depth_check + ')';
			} else {
				return record.data.depth;
			}
		}
	}, {
		header : SysX.t.Weight,
		dataIndex : 'weight',
		flex : 1,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			if (record.data.weight !== record.data.weight_check) {
				metaData.tdAttr = 'style="color:red;"';
				return record.data.weight + ' (' + record.data.weight_check + ')';
			} else {
				return record.data.weight;
			}
		}
	}, {
		dataIndex : 'quantity_check',
		hidden : true
	}, {
		dataIndex : 'length_check',
		hidden : true
	}, {
		dataIndex : 'width_check',
		hidden : true
	}, {
		dataIndex : 'depth_check',
		hidden : true
	}, {
		dataIndex : 'weight_check',
		hidden : true
	}]
});

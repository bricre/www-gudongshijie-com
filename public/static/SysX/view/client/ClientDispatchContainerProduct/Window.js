Ext.define('SysX.view.client.ClientDispatchContainerProduct.Window', {
	extend : 'SysX.view.abstract.Window',
	itemId : 'client.ClientDispatchContainerProduct.Window',
	width:400,
	height:400,
	initComponent : function() {
		this.formItems = {
			title : SysX.t.BasicDetails,
			layout : 'form',
			items : [{
				name : 'client_dispatch_container_id',
				fieldLabel : SysX.t.Container,
				allowBlank : true,
				xtype : 'hidden'
			}, {
				name : 'product_id',
				fieldLabel : SysX.t.Product,
				allowBlank : true,
				xtype : 'textfield'
			}, {
				name : 'quantity',
				fieldLabel : SysX.t.Quantity,
				allowBlank : true,
				xtype : 'textfield'
			}, {
				name : 'length',
				fieldLabel : SysX.t.Length,
				allowBlank : true,
				xtype : 'textfield'
			}, {
				name : 'width',
				fieldLabel : SysX.t.Width,
				allowBlank : true,
				xtype : 'textfield'
			}, {
				name : 'depth',
				fieldLabel : SysX.t.Depth,
				allowBlank : true,
				xtype : 'textfield'
			}, {
				name : 'weight',
				fieldLabel : SysX.t.Weight,
				allowBlank : true,
				xtype : 'textfield'
			}, {
				name : 'price',
				fieldLabel : SysX.t.Price,
				allowBlank : true,
				xtype : 'textfield'
			}]
		};

		this.callParent(arguments);
	}
});

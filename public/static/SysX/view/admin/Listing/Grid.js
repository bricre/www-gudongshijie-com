Ext.define('SysX.view.admin.Listing.Grid', {
	extend : 'SysX.view.abstract.Grid',
	itemId : 'admin.Listing.Grid',
	store : 'SysX.store.Listing',
	title : SysX.t.Listing,
	columns : [{
		header : SysX.t.ID,
		dataIndex : 'id',
		sortable : true,
		flex : 1,
		filter : true
	}, {
		header : SysX.t.Vendor,
		dataIndex : 'vendor_id',
		sortable : true,
		flex : 2,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			return record.get('vendor-name');
		}
	}, {
		header : SysX.t.Title,
		dataIndex : 'title',
		sortable : true,
		flex : 5
	}, {
		header : SysX.t.Cost,
		dataIndex : 'cost',
		sortable : true,
		flex : 2,
		filter : true,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			return record.get('cost_currency-symbol') + ' ' + value;
		}
	}, {
		header : SysX.t.Price,
		dataIndex : 'price',
		sortable : true,
		flex : 2,
		filter : true,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			return record.get('price_currency-symbol') + ' ' + value;
		}
	}, {
		header : SysX.t.Status,
		dataIndex : 'status',
		sortable : true,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			var status = SysX.model.Listing.prototype.status;
			metaData.tdAttr = status[value][2];
			return status[value][1];
		},
		filter : {
			type : 'list',
			options : Ext.Object.getValues(Ext.create('SysX.model.Listing').status),
			value : ['NEW', 'ENQUIRING', 'INSTOCK', 'SOLD']
		},
		flex : 1
	}]
});

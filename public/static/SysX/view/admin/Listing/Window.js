Ext.define('SysX.view.admin.Listing.Window', {
	extend : 'SysX.view.abstract.Window',
	itemId : 'admin.Listing.Window',
	initComponent : function() {
		this.formItems = {
			title : SysX.t.BasicDetails,

			defaults : {
				xtype : 'textfield',
				anchor : '100%'
			},
			items : [{
				name : 'id',
				xtype : 'hidden'
			}, {
				name : 'vendor_id',
				fieldLabel : SysX.t.Vendor,
				allowBlank : false,
				xtype : 'combo',
				anchor : '100%',
				valueField : 'id',
				displayField : 'name',
				queryMode : 'local',
				forceSelection : true,
				editable : false,
				store : Ext.create('SysX.store.Vendor', {
					pageSize : -1,
					autoLoad : true,
					filters : [{
						property : 'status',
						value : 'ACTIVE'
					}]
				})
			}, {
				name : 'title',
				fieldLabel : SysX.t.Title
			}, {
				name : 'url',
				fieldLabel : SysX.t.Url
			}, {
				fieldLabel : SysX.t.Cost,
				xtype : 'fieldcontainer',
				anchor : '100%',
				layout : 'hbox',
				items : [{
					name : 'cost_currency_code',
					allowBlank : false,
					xtype : 'combo',
					valueField : 'code',
					displayField : 'code',
					queryMode : 'local',
					forceSelection : true,
					editable : false,
					store : Ext.create('SysX.store.Currency', {
						pageSize : -1,
						autoLoad : true
					})
				}, {
					name : 'cost',
					xtype : 'textfield'
				}]
			}, {
				fieldLabel : SysX.t.Price,
				xtype : 'fieldcontainer',
				anchor : '100%',
				layout : 'hbox',
				items : [{
					name : 'price_currency_code',
					allowBlank : false,
					xtype : 'combo',
					valueField : 'code',
					displayField : 'code',
					queryMode : 'local',
					forceSelection : true,
					editable : false,
					store : Ext.create('SysX.store.Currency', {
						pageSize : -1,
						autoLoad : true
					})
				}, {
					name : 'price',
					xtype : 'textfield'
				}]
			}, {
				fieldLabel : SysX.t.DeliveryFee,
				xtype : 'fieldcontainer',
				anchor : '100%',
				layout : 'hbox',
				items : [{
					name : 'delivery_fee_currency_code',
					allowBlank : false,
					xtype : 'combo',
					valueField : 'code',
					displayField : 'code',
					queryMode : 'local',
					forceSelection : true,
					editable : false,
					store : Ext.create('SysX.store.Currency', {
						pageSize : -1,
						autoLoad : true
					})
				}, {
					name : 'delivery_fee',
					xtype : 'textfield'
				}]
			}, {
				fieldLabel : SysX.t.RecommendedPrice,
				xtype : 'fieldcontainer',
				anchor : '100%',
				layout : 'hbox',
				items : [{
					name : 'recommended_price_currency_code',
					allowBlank : false,
					xtype : 'combo',
					valueField : 'code',
					displayField : 'code',
					queryMode : 'local',
					forceSelection : true,
					editable : false,
					store : Ext.create('SysX.store.Currency', {
						pageSize : -1,
						autoLoad : true
					})
				}, {
					name : 'recommended_price',
					xtype : 'textfield'
				}]
			}, {
				name : 'status',
				fieldLabel : SysX.t.Status,
				allowBlank : false,
				xtype : 'combo',
				value : 'NEW',
				valueField : 'value',
				displayField : 'text',
				queryMode : 'local',
				forceSelection : true,
				editable : false,
				store : new Ext.data.ArrayStore({
					fields : ['value', 'text', 'attr'],
					data : Ext.Object.getValues(Ext.create('SysX.model.Listing').status)
				})
			}, {
				xtype : 'fieldset',
				title : SysX.t.Description,
				bodyStyle : 'padding:0',
				items : [{
					name : 'description',
					xtype : 'htmleditor',
					anchor : '-5 -5',
					height : 400,
					hideLabel : true
				}]
			}, {
				xtype : 'fieldset',
				title : SysX.t.Note,
				bodyStyle : 'padding:0',
				items : [{
					name : 'note',
					xtype : 'htmleditor',
					anchor : '-5 -5',
					height : 400,
					hideLabel : true
				}]
			}]
		};

		this.callParent(arguments);
	}
});

Ext.define('SysX.view.admin.CompanyUserGroup.Window', {
	extend : 'SysX.view.abstract.Window',
	itemId : 'admin.CompanyUserGroup.Window',
	initComponent : function() {
		this.formItems = {
			title : SysX.t.BasicDetails,
			items : [{
				name : 'id',
				xtype : 'hidden'
			}, {
				name : 'name',
				fieldLabel : SysX.t.Name,
				xtype : 'textfield',
				anchor : '100%'
			}]
		};

		this.callParent(arguments);
	}
});

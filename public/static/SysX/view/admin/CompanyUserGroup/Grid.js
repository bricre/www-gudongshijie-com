Ext.define('SysX.view.admin.CompanyUserGroup.Grid', {
	extend : 'SysX.view.abstract.Grid',
	itemId : 'admin.CompanyUserGroup.Grid',
	store : 'SysX.store.CompanyUserGroup',
	title : SysX.t.CompanyUserGroup,
	columns : [{
		header : SysX.t.ID,
		dataIndex : 'id',
		sortable : true,
		flex : 1,
		hidden : true
	}, {
		header : SysX.t.Name,
		dataIndex : 'name',
		sortable : true,
		flex : 3
	}],
	defaultSorters : [{
		dataIndex : 'name',
		order : 'DESC'
	}]

});

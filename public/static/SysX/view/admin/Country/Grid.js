Ext.define('SysX.view.admin.Country.Grid', {
	extend : 'SysX.view.abstract.Grid',
	itemId : 'admin.Country.Grid',
	store : 'SysX.store.Country',
	title : SysX.t.Country,
	columns : [{
		header : SysX.t.ISO,
		dataIndex : 'iso',
		sortable : true,
		flex:1
	}, {
		header : SysX.t.Code,
		dataIndex : 'name',
		flex:1
	}, {
		header : SysX.t.PrintableName,
		dataIndex : 'printable_name',
		flex:2
	}, {
		header : SysX.t.ISO3,
		dataIndex : 'iso3',
		flex:1
	}, {
		header : SysX.t.Numcode,
		dataIndex : 'numcode',
		flex:1
	}]
}); 
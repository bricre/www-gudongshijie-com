Ext.define('SysX.view.admin.Country.Window', {
	extend : 'SysX.view.abstract.Window',
	itemId : 'admin.Country.Window',
	singlePanel : true,
	initComponent : function() {
		this.formItems = {
			title : SysX.t.BasicDetails,
			items : [{
				name : 'iso',
				fieldLabel : SysX.t.ISO,
				xtype : 'textfield'
			}, {
				name : 'name',
				fieldLabel : SysX.t.Name,
				xtype : 'textfield',
				allowBlank : false
			}, {
				name : 'printable_name',
				fieldLabel : SysX.t.PrintableName,
				xtype : 'textfield',
				allowBlank : false
			}, {
				name : 'iso3',
				fieldLabel : SysX.t.ISO3,
				xtype : 'textfield',
				allowBlank : false
			}, {
				name : 'numcode',
				fieldLabel : SysX.t.Numcode,
				xtype : 'textfield'
			}]
		};

		this.callParent(arguments);
	}
}); 
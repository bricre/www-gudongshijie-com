Ext.define('SysX.view.admin.CompanyUser.Window', {
	extend : 'SysX.view.abstract.Window',
	itemId : 'admin.CompanyUser.Window',
	initComponent : function() {
		this.formItems = {
			title : SysX.t.BasicDetails,

			defaults : {
				xtype : 'textfield',
				anchor : '100%'
			},
			items : [{
				name : 'id',
				xtype : 'hidden'
			}, {
				name : 'username',
				fieldLabel : SysX.t.Username,
				allowBlank : false
			}, {
				name : 'new_password',
				fieldLabel : SysX.t.Password
			}, {
				name : 're_password',
				fieldLabel : SysX.t.ReEnterPassword
			}, {
				name : 'email',
				fieldLabel : SysX.t.Email
			}, {
				name : 'first_name',
				fieldLabel : SysX.t.FirstName
			}, {
				name : 'last_name',
				fieldLabel : SysX.t.LastName
			}, {
				name : 'company_user_group_id',
				fieldLabel : SysX.t.UserGroup,
				allowBlank : true,
				xtype : 'combo',
				valueField : 'id',
				displayField : 'name',
				queryMode : 'local',
				forceSelection : true,
				triggerAction : 'all',
				store : Ext.create('SysX.store.CompanyUserGroup', {
					autoLoad : true,
					pageSize : -1
				})
			}, {
				name : 'status',
				fieldLabel : SysX.t.Status,
				allowBlank : false,
				xtype : 'combo',
				anchor : '50%',
				value : 'ACTIVE',
				valueField : 'value',
				displayField : 'text',
				queryMode : 'local',
				forceSelection : true,
				triggerAction : 'all',
				editable : false,
				store : new Ext.data.ArrayStore({
					fields : ['value', 'text', 'attr'],
					data : Ext.Object.getValues(Ext.create('SysX.model.CompanyUser').status)
				})
			}, {
				xtype : 'displayfield',
				fieldLabel : SysX.t.ApiKey,
				name : 'api_key',
				itemId : 'api_key',
				value : SysX.t.CompanyUserMessage_NoAPIKey
			}, {
				xtype : 'fieldcontainer',
				fieldLabel : SysX.t.Action,
				items : [{
					xtype : 'button',
					text : SysX.t.GenerateNewAPIKey,
					itemId : 'generate_new_api_key_button',
					anchor : null
				}, {
					xtype : 'splitter'
				}, {
					xtype : 'button',
					text : SysX.t.Print,
					handler : function(button) {
						SysX.openNewWindow(modulePath + '/Printer/Company-User/' + this.up('form').getForm().getValues().id);
					}
				}]
			}]
		};

		this.callParent(arguments);
	}
});

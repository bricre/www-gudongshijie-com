Ext.define('SysX.view.admin.CompanyUserGroupPrivilege.Window', {
	extend : 'SysX.view.abstract.Window',
	itemId : 'admin.CompanyUserGroupPrivilege.Window',
	initComponent : function() {
		this.formItems = {
			title : SysX.t.BasicDetails,
			items : []
		};

		this.callParent(arguments);
	}
});

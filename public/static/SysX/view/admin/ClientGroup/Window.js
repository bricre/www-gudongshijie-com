Ext.define('SysX.view.admin.ClientGroup.Window', {
	extend : 'SysX.view.abstract.Window',
	itemId : 'admin.ClientGroup.Window',
	initComponent : function() {
		this.formItems = {
			title : SysX.t.BasicDetails,
			items : [{
				name : 'id',
				xtype : 'hidden'
			}, {
				name : 'name',
				fieldLabel : SysX.t.Name,
				xtype : 'textfield',
				anchor : '100%'
			}, {
				xtype : 'fieldset',
				title : SysX.t.Fee,
				layout : 'form',
				defaults : {
					xtype : 'textfield'
				},
				items : [{
					name : 'handling_fee_limit',
					fieldLabel : SysX.t.HandlingFeeLimit,
					xtype : 'textfield'
				}, {
					name : 'handling_fee_first_item',
					fieldLabel : SysX.t.HandlingFeeFirstItem,
					xtype : 'textfield'
				}, {
					name : 'handling_fee_other_item',
					fieldLabel : SysX.t.HandlingFeeOtherItem,
					xtype : 'textfield'
				}, {
					name : 'handling_fee_first_product',
					fieldLabel : SysX.t.HandlingFeeFirstProduct,
					xtype : 'textfield'
				}, {
					name : 'handling_fee_other_product',
					fieldLabel : SysX.t.HandlingFeeOtherProduct,
					xtype : 'textfield'
				}, {
					xtype : 'fieldcontainer',
					fieldLabel : SysX.t.DeliveryFee,
					layout : 'hbox',
					items : [{
						name : 'delivery_fee',
						xtype : 'textfield'
					}, {
						name : 'delivery_fee_type',
						xtype : 'combo',
						valueField : 'value',
						displayField : 'text',
						editable : false,
						queryMode : 'local',
						triggerAction : 'all',
						allowBlank : false,
						forceSelection : true,
						value : '0',
						store : Ext.create('Ext.data.ArrayStore', {
							fields : ['value', 'text'],
							data : [['0', 'Value'], ['1', 'Percentage']]
						})
					}]
				}, {
					name : 'monthly_membership_fee',
					fieldLabel : SysX.t.MonthlyMembershipFee,
					xtype : 'textfield'
				}]
			}, {
				xtype : 'fieldset',
				title : SysX.t.ClientCreditLimitTopUpTransaction,
				layout : 'form',
				defaults : {
					xtype : 'textfield'
				},
				items : [{
					name : 'credit_limit_top_up_period',
					fieldLabel : SysX.t.CreditLimitTopUpPeriod
				}, {
					name : 'credit_limit_top_up_amount',
					fieldLabel : SysX.t.CreditLimitTopUpAmount
				}, {
					name : 'credit_limit_top_up_times',
					fieldLabel : SysX.t.CreditLimitTopUpTimes
				}, {
					name : 'credit_limit_top_up_period_per_time',
					fieldLabel : SysX.t.CreditLimitTopUpPeriodPerTime
				}]
			}, {
				xtype : 'fieldset',
				title : SysX.t.Status,
				layout : 'form',
				items : [{
					name : 'status',
					fieldLabel : SysX.t.Status,
					allowBlank : false,
					xtype : 'combo',
					value : 'ACTIVE',
					valueField : 'value',
					displayField : 'text',
					queryMode : 'local',
					editable : false,
					store : Ext.Object.getValues(Ext.create('SysX.model.ClientGroup').status)
				}]
			}]
		};

		this.callParent(arguments);
	}
});

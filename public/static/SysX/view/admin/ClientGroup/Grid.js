Ext.define('SysX.view.admin.ClientGroup.Grid', {
	extend : 'SysX.view.abstract.Grid',
	itemId : 'admin.ClientGroup.Grid',
	store : 'SysX.store.ClientGroup',
	title : SysX.t.ClientGroup,
	columns : [{
		header : SysX.t.ID,
		dataIndex : 'id',
		sortable : true,
		flex : 1
	}, {
		header : SysX.t.Name,
		dataIndex : 'name',
		sortable : true,
		flex : 3
	}, {
		header : SysX.t.HandlingFeeLimit,
		dataIndex : 'handling_fee_limit',
		flex : 1
	}, {
		header : SysX.t.HandlingFeeFirstItem,
		dataIndex : 'handling_fee_first_item',
		flex : 1
	}, {
		header : SysX.t.HandlingFeeOtherItem,
		dataIndex : 'handling_fee_other_item',
		flex : 1
	}, {
		header : SysX.t.HandlingFeeFirstProduct,
		dataIndex : 'handling_fee_first_product',
		flex : 1,
		excludeFromSelect : true,
		editor : true
	}, {
		header : SysX.t.HandlingFeeOtherProduct,
		dataIndex : 'handling_fee_other_product',
		flex : 1,
		excludeFromSelect : true,
		editor : {
			xtype : 'textfield',
			allowBlank : false
		}
	}, {
		header : SysX.t.DeliveryFee,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			return record.data.delivery_fee + (1 === record.data.delivery_fee_type ? '%' : '');
		},
		flex : 1
	}, {
		header : SysX.t.MonthlyMembershipFee,
		dataIndex : 'monthly_membership_fee',
		flex : 1
	}, {
		dataIndex : 'delivery_fee',
		hidden : true
	}, {
		dataIndex : 'delivery_fee_type',
		hidden : true
	}, {
		header : SysX.t.Status,
		dataIndex : 'status',
		sortable : true,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			var status = SysX.model.ClientGroup.prototype.status;
			metaData.tdAttr = status[value][2];
			return status[value][1];
		},
		filter : {
			type : 'list',
			options : Ext.Object.getValues(Ext.create('SysX.model.ClientGroup').status),
			value : ['ACTIVE']
		},
		flex : 1
	}]
});

Ext.define('SysX.view.admin.ClientStatement.Window', {
	extend : 'SysX.view.abstract.Window',
	itemId : 'admin.ClientStatement.Window',
	width : 600,
	height : 400,
	initComponent : function() {
		this.formItems = {
			title : SysX.t.BasicDetails,
			items : [{
				name : 'id',
				xtype : 'hidden'
			}, {
				name : 'client_id',
				xtype : 'hidden'
			}, {
				name : 'amount',
				fieldLabel : SysX.t.Amount,
				xtype : 'numberfield',
				allowBlank : false,
				hideTrigger : true
			}, {
				name : 'update_time',
				fieldLabel : SysX.t.Time,
				xtype : 'datetimefield',
				format : 'Y-m-d',
				allowBlank : false,
				value : new Date()
			}, {
				name : 'note',
				fieldLabel : SysX.t.Note,
				xtype : 'htmleditor',
				width : '400',
				height : '300'
			}]
		};

		this.callParent(arguments);
	}
});

Ext.define('SysX.view.admin.User.Login', {
	extend : 'Ext.window.Window',
	itemId : 'admin.userLogin',
	width : 300,
	closable : false,
	resizable : false,
	plain : true,
	border : false,
	modal : true,
	layout : 'fit',
	title : SysX.t.PleaseLogin,
	initComponent : function() {
		this.items = [{
			xtype : 'form',
			labelWidth : 80,
			url : modulePath + '/index/login',
			frame : false,
			monitorValid : true,
			bodyStyle : 'padding:5px',
			defaults : {
				xtype : 'textfield',
				enableKeyEvents : true,
				anchor : '100%'
			},
			items : [{
				fieldLabel : SysX.t.Username,
				name : 'username',
				allowBlank : true
			}, {
				fieldLabel : SysX.t.Password,
				name : 'password',
				inputType : 'password',
				allowBlank : true
			}, {
				fieldLabel : SysX.t.ApiKey,
				name : 'api_key',
				inputType : 'password',
				allowBlank : true
			}, {
				name : 'locale',
				fieldLabel : SysX.t.Language,
				allowBlank : false,
				xtype : 'combo',
				valueField : 'value',
				displayField : 'text',
				mode : 'local',
				forceSelection : true,
				triggerAction : 'all',
				editable : false,
				value : locale, //Global Variable defined in index.phtml
				store : Ext.Object.getValues(SysX.Locale.locales)
			}]
		}];

		this.buttons = [{
			itemId : 'loginButton',
			text : SysX.t.Login,
			formBind : true,
			listeners : {
				click : {
					fn : this.loginButtonHandler
				}
			}
		}];

		this.listeners = {
			show : function() {
				Ext.each(this.query('textfield'), function(item, index) {
					item.on('specialkey', function(field, e) {
						if (e.getKey() === e.ENTER) {
							var button = field.up('window').down('button');
							button.fireEvent('click', button);
						}
					});
				});
			},
			scope : this
		};

		this.callParent(arguments);

	},
	loginButtonHandler : function(button, event) {
		var form = button.up('window').down('form').getForm();
		if (!form.isValid()) {
			return false;
		}

		form.submit({
			method : 'POST',
			success : function(form, action) {
				location.reload(false);
			},
			failure : function(form, action) {
				if (action.failureType === 'server') {
					Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, Ext.JSON.decode(action.response.responseText).message);
				} else {
					Ext.Msg.alert(SysX.t.MessageBoxTitle_Alert, action.response.responseText);
				}
				form.reset();
				form.owner.down('textfield[name=username]').focus();
			}
		});
	}
});

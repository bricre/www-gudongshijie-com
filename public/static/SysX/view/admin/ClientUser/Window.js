Ext.define('SysX.view.admin.ClientUser.Window', {
	extend : 'SysX.view.abstract.Window',
	itemId : 'admin.ClientUser.Window',
	height : 400,
	width : 400,
	initComponent : function() {
		this.formItems = {
			title : SysX.t.BasicDetails,
			defaults : {
				xtype : 'textfield',
				anchor : '100%'
			},
			items : [{
				name : 'id',
				xtype : 'hidden',
				id : 'clientUserIdField'
			}, {
				name : 'client_id',
				xtype : 'hidden'
			}, {
				name : 'username',
				fieldLabel : SysX.t.Username,
				allowBlank : false
			}, {
				name : 'new_password',
				fieldLabel : SysX.t.Password
			}, {
				name : 're_password',
				fieldLabel : SysX.t.ReEnterPassword
			}, {
				name : 'email',
				fieldLabel : SysX.t.Email
			}, {
				name : 'first_name',
				fieldLabel : SysX.t.FirstName
			}, {
				name : 'last_name',
				fieldLabel : SysX.t.LastName
			}, {
				name : 'client_user_group_id',
				fieldLabel : SysX.t.UserGroup,
				allowBlank : true,
				xtype : 'combo',
				valueField : 'id',
				displayField : 'name',
				queryMode : 'local',
				forceSelection : true,
				triggerAction : 'all',
				store : Ext.create('SysX.store.ClientUserGroup', {
					autoLoad : true,
					pageSize : -1
				})
			}, {
				hiddenName : 'status',
				fieldLabel : SysX.t.Status,
				allowBlank : false,
				xtype : 'combo',
				anchor : '50%',
				value : 'ACTIVE',
				valueField : 'value',
				displayField : 'text',
				queryMode : 'local',
				forceSelection : true,
				triggerAction : 'all',
				editable : false,
				store : new Ext.data.ArrayStore({
					fields : ['value', 'text', 'attr'],
					data : Ext.Object.getValues(Ext.create('SysX.model.ClientUser').status)
				})
			}, {
				fieldLabel : SysX.t.AdvancedLogin,
				xtype : 'button',
				text : SysX.t.LoginAsThisUser,
				handler : function() {
					var loginForm = Ext.create('Ext.form.Panel', {
						standardSubmit : true,
						items : [{
							xtype : 'hidden',
							name : 'advancedLogin',
							value : '1'
						}, {
							xtype : 'hidden',
							name : 'client_user_id',
							value : Ext.getCmp('clientUserIdField').getValue()
						}],
						method : 'POST',
						url : '/client/'
					});
					loginForm.getForm().submit();
				}
			}]
		};

		this.callParent(arguments);
	}
});

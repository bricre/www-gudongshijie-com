Ext.define('SysX.view.admin.ClientUser.Grid', {
	extend : 'SysX.view.abstract.Grid',
	alias : 'widget.ClientUser.Grid',
	store : 'SysX.store.ClientUser',
	title : SysX.t.ClientUser,
	columns : [{
		header : 'ID',
		dataIndex : 'id',
		sortable : true,
		flex : 1
	}, {
		header : SysX.t.Username,
		dataIndex : 'username',
		sortable : true,
		flex : 2
	}, {
		header : SysX.t.FirstName,
		dataIndex : 'first_name',
		sortable : true,
		flex : 2
	}, {
		header : SysX.t.LastName,
		dataIndex : 'last_name',
		sortable : true,
		flex : 2
	}, {
		header : SysX.t.Status,
		dataIndex : 'status',
		sortable : true,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			var status = SysX.model.ClientUser.prototype.status;
			metaData.tdAttr = status[value][2];
			return status[value][1];
		},
		flex : 1
	}]
});

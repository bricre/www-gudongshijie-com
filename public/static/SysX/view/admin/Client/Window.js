Ext.define('SysX.view.admin.Client.Window', {
	extend : 'SysX.view.abstract.Window',
	itemId : 'admin.Client.Window',
	initComponent : function() {
		this.formItems = {
			title : SysX.t.BasicInfo,
			layout : 'column',
			defaults : {
				layout : 'form',
				columnWidth : 0.5,
				border : false,
				bodyStyle : 'padding:5px'
			},
			// Columns
			items : [{
				// Fieldsets
				defaults : {
					xtype : 'fieldset',
					layout : 'form',
					anchor : '100%',
					autoHeight : true
				},
				items : [{
					title : SysX.t.BasicDetails,
					defaults : {
						xtype : 'textfield',
						anchor : '100%'
					},
					items : [{
						name : 'id',
						xtype : 'hidden'
					}, {
						name : 'name',
						fieldLabel : SysX.t.CompanyName,
						allowBlank : false
					}, {
						name : 'email',
						fieldLabel : SysX.t.Email,
						allowBlank : false,
						vtype : 'email'
					}, {
						name : 'telephone',
						fieldLabel : SysX.t.Telephone,
						allowBlank : true
					}, {
						name : 'address_line_1',
						fieldLabel : SysX.t.AddressLine1,
						allowBlank : true
					}, {
						name : 'address_line_2',
						fieldLabel : SysX.t.AddressLine2,
						allowBlank : true
					}, {
						name : 'address_line_3',
						fieldLabel : SysX.t.AddressLine3,
						allowBlank : true
					}, {
						name : 'city',
						fieldLabel : SysX.t.City,
						allowBlank : true
					}, {
						name : 'county',
						fieldLabel : SysX.t.County,
						allowBlank : true
					}, {
						name : 'post_code',
						fieldLabel : SysX.t.PostCode,
						allowBlank : true,
						anchor : '50%'
					}, {
						name : 'country_iso',
						fieldLabel : SysX.t.Country,
						allowBlank : false,
						xtype : 'combo',
						valueField : 'iso',
						displayField : 'printable_name',
						queryMode : 'local',
						triggerAction : 'all',
						store : Ext.create('SysX.store.Country', {
							autoLoad : true,
							remoteFilter : false
						})
					}]
				}]
			}, {
				// Fieldsets
				defaults : {
					xtype : 'fieldset',
					layout : 'form',
					anchor : '100%',
					autoHeight : true
				},
				items : [{
					title : SysX.t.UserGroup,
					defaults : {
						xtype : 'textfield',
						anchor : '100%'
					},
					items : [{
						name : 'is_internal',
						fieldLabel : SysX.t.Client_IsInternal,
						xtype : 'checkbox',
						checked : true
					}, {
						name : 'is_external',
						fieldLabel : SysX.t.Client_IsExternal,
						xtype : 'checkbox',
						checked : false
					}, {
						name : 'is_vip',
						fieldLabel : SysX.t.IsVip,
						xtype : 'checkbox',
						checked : false
					}]
				}, {
					title : SysX.t.ConsignmentSettings,
					defaults : {
						xtype : 'textfield'
					},
					items : [{
						name : 'bulk_order_reference_field',
						fieldLabel : SysX.t.BulkOrderReferenceField,
						allowBlank : false,
						xtype : 'combo',
						value : 'ID',
						valueField : 'value',
						displayField : 'text',
						queryMode : 'local',
						forceSelection : true,
						triggerAction : 'all',
						editable : false,
						store : new Ext.data.ArrayStore({
							fields : ['value', 'text'],
							data : Ext.Object.getValues(Ext.create('SysX.model.Client').bulkOrderReferenceField)
						})
					}]
				}, {
					title : SysX.t.Accounting,
					defaults : {
						xtype : 'textfield'
					},
					items : [{
						name : 'credit_limit',
						fieldLabel : SysX.t.CreditLimit,
						allowBlank : false,
						value : 0.00,
						size : '9'
					}, {
						name : 'balance',
						xtype : 'displayfield',
						fieldLabel : SysX.t.Balance,
						value : 0.00
					}, {
						name : 'create_time',
						xtype : 'displayfield',
						fieldLabel : SysX.t.CreateTime
					}, {
						name : 'next_charging_date',
						xtype : 'displayfield',
						fieldLabel : SysX.t.NextChargingDate,
						value : 'N/A'
					}, {
						name : 'client_group_id',
						fieldLabel : SysX.t.ClientGroup,
						allowBlank : false,
						xtype : 'combo',
						valueField : 'id',
						displayField : 'name',
						queryMode : 'local',
						forceSelection : true,
						triggerAction : 'all',
						store : Ext.create('SysX.store.ClientGroup', {
							autoLoad : true,
							pageSize : -1
						})
					}, {
						name : 'status',
						fieldLabel : SysX.t.Status,
						allowBlank : false,
						xtype : 'combo',
						anchor : '80%',
						value : 'ACTIVE',
						valueField : 'value',
						displayField : 'text',
						queryMode : 'local',
						forceSelection : true,
						triggerAction : 'all',
						editable : false,
						store : Ext.Object.getValues(Ext.create('SysX.model.Client').status)
					}]
				}]
			}, {
				columnWidth : 1,
				title : SysX.t.Note,
				bodyStyle : 'padding:0',
				items : [{
					name : 'note',
					xtype : 'htmleditor',
					anchor : '-5 -5',
					height : 400,
					hideLabel : true
				}]
			}]
		};

		this.callParent(arguments);
	}
});

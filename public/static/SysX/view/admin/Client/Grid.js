Ext.define('SysX.view.admin.Client.Grid', {
	extend : 'SysX.view.abstract.Grid',
	itemId : 'admin.Client.Grid',
	store : 'SysX.store.Client',
	title : SysX.t.Clients,
	columns : [{
		header : SysX.t.ID,
		dataIndex : 'id',
		sortable : true,
		filter : {
			type : 'numeric'
		},
		flex : 1
	}, {
		header : SysX.t.Name,
		dataIndex : 'name',
		sortable : true,
		filter : true,
		flex : 2
	}, {
		header : SysX.t.Group,
		dataIndex : 'client_group_id',
		sortable : true,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			return record.data.client_group_name;
		},
		flex : 2
	}, {
		header : SysX.t.Internal,
		dataIndex : 'is_internal',
		sortable : true,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			return record.data.is_internal ? SysX.t.YES : SysX.t.NO;
		},
		filter : true,
		flex : 0.5
	}, {
		header : SysX.t.External,
		dataIndex : 'is_external',
		sortable : true,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			return record.data.is_external ? SysX.t.YES : SysX.t.NO;
		},
		filter : true,
		flex : 0.5
	}, {
		header : SysX.t.Status,
		dataIndex : 'status',
		sortable : true,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			var status = SysX.model.Client.prototype.status;
			metaData.tdAttr = status[value][2];
			return status[value][1];
		},
		filter : {
			type : 'list',
			options : Ext.Object.getValues(Ext.create('SysX.model.Client').status),
			value : ['ACTIVE']
		},
		flex : 1
	}],
	topToolbarExtraItems : [{
		xtype : 'button',
		text : SysX.t.BatchChangeStatus,
		handler : function(button) {
			var view = button.up('grid'), status = view.down('combo[itemId=BatchChangeStatusCombo]').getValue();

			if (!status) {
				Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, SysX.t.BatchChangeStatusMessage_SelectStatusFirst);
				return false;
			}

			var records = view.getSelectionModel().getSelection();
			if (records.length < 1) {
				Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, SysX.t.BatchChangeStatusMessage_SelectRecordFirst);
				return false;
			}

			var selectedIds = [];
			for (var i = records.length - 1; i >= 0; i--) {
				selectedIds.push(records.shift().data.id);
			}

			Ext.Ajax.request({
				url : modulePath + '/Client/Batch-Update-Status',
				method : 'GET',
				scope : this,
				params : {
					ids : selectedIds.join('_'),
					'status' : status
				},
				success : function(response, opts) {
					Ext.Msg.alert(SysX.t.MessageBoxTitle_Success, SysX.t.BatchChangeStatusMessage_AllRecordsUpdated);
					view.getStore().load();
				},
				failure : function(response, opts) {
					Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, Ext.decode(response.responseText).message);
					return false;
				}
			});

		}
	}, {
		xtype : 'combo',
		submitValue : false,
		itemId : 'BatchChangeStatusCombo',
		valueField : 'value',
		displayField : 'text',
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		editable : false,
		store : new Ext.data.ArrayStore({
			fields : ['value', 'text', 'attr'],
			data : Ext.Object.getValues(Ext.create('SysX.model.Client').status)
		})
	}]
});

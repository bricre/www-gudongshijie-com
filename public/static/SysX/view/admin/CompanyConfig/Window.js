Ext.define('SysX.view.admin.CompanyConfig.Window', {
	extend : 'SysX.view.abstract.Window',
	singlePanel : true,
	initComponent : function() {
		var defaults = {
			labelWidth : 200,
			labelAlign : 'right'
		};

		this.formItems = {
			title : SysX.t.Config,

			items : [{
				xtype : 'fieldset',
				collapsible : true,
				title : SysX.t.Config_VipProduct,
				defaults : defaults,
				items : [{
					name : 'vip_product-minimum_stock',
					fieldLabel : SysX.t.Config_VipProduct_MinimumStock,
					xtype : 'numberfield'
				}, {
					name : 'vip_product-minimum_monthly_sales',
					fieldLabel : SysX.t.Config_VipProduct_MinimumMonthlySales,
					xtype : 'numberfield'
				}, {
					name : 'vip_product-require_commodity_code',
					fieldLabel : SysX.t.Config_VipProduct_RequireCommodityCode,
					xtype : 'checkboxfield'
				}, {
					name : 'vip_product-require_name_customs',
					fieldLabel : SysX.t.Config_VipProduct_RequireNameCustoms,
					xtype : 'checkboxfield'
				}, {
					name : 'vip_product-require_price_customs_export',
					fieldLabel : SysX.t.Config_VipProduct_RequirePriceCustomsExport,
					xtype : 'checkboxfield'
				}, {
					name : 'vip_product-require_price_customs_import',
					fieldLabel : SysX.t.Config_VipProduct_RequirePriceCustomsImport,
					xtype : 'checkboxfield'
				}, {
					name : 'vip_product-require_vip_client',
					fieldLabel : SysX.t.Config_VipProduct_RequireVipClient,
					xtype : 'checkboxfield'
				}]
			}, {
				xtype : 'fieldset',
				collapsible : true,
				title : SysX.t.DeliveryService,
				defaults : defaults,
				items : [{
					name : 'delivery_service-royalmail-standard-use_sort_code',
					fieldLabel : SysX.t.Config_DeliveryService_RoyalMail_Standard_UseSortCode,
					xtype : 'checkboxfield'
				}]
			}]
		};

		this.callParent(arguments);
		this.down('form').load({
			method : 'get',
			success : function(form, action) {
				var resultData = action.result.data, modelFields = resultData.map(function(data) {
					return data.config_code;
				}), modelClassName = Ext.id(), modelClass = Ext.define(modelClassName, {
					extend : 'Ext.data.Model',
					fields : modelFields
				}), model = Ext.create(modelClassName);
				Ext.Array.each(resultData, function(data) {
					model.set(data.config_code, data.value);
				});
				form.loadRecord(model);
				Ext.destroy(modelClass);
			}
		});
	}
});

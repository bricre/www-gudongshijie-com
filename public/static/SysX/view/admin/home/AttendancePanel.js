Ext.define('SysX.view.admin.home.AttendancePanel', {
	extend : 'Ext.panel.Panel',
	layout : 'fit',
	minHeight : 200,
	initComponent : function() {
		var additionalView = Ext.create('SysX.view.admin.Attendance.Grid', {
			disableBottomToolbar : true,
			disableItemDbClick : true,
			disableTopToolbar : false,
			disableInsertButton : true,
			disableDeleteButton : true,
			topToolbarExtraItems : [{
				xtype : 'checkbox',
				fieldLabel : SysX.t.AutoReload,
				uncheckedValue : false,
				listeners : {
					change : function(checkbox, newValue, oldValue, eOpts) {
						if (newValue) {
							task.start();
						} else {
							task.stop();
						}
					}
				}
			}]
		});
		var taskRunner = Ext.create('Ext.util.TaskRunner'), task = taskRunner.newTask({
			run : function() {
				this.down('grid').getStore().load();
			},
			scope : this,
			interval : 1000 * 60
		});
		additionalView.getStore().proxy.url = modulePath + '/Attendance/Today-Summary';
		this.items = [additionalView];
		this.callParent();
	}
});


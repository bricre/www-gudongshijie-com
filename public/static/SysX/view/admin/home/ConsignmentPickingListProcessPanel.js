Ext.define('SysX.view.admin.home.ConsignmentPickingListProcessPanel', {
	extend : 'Ext.panel.Panel',
	layout : 'fit',
	minHeight : 200,
	initComponent : function() {
		var additionalView = Ext.create('SysX.view.admin.home.ConsignmentPickingListProcessGrid', {
			topToolbarExtraItems : [{
				xtype : 'checkbox',
				fieldLabel : SysX.t.AutoReload,
				uncheckedValue : false,
				listeners : {
					change : function(checkbox, newValue, oldValue, eOpts) {
						if (newValue) {
							task.start();
						} else {
							task.stop();
						}
					}
				}
			}]
		});
		var taskRunner = Ext.create('Ext.util.TaskRunner'), task = taskRunner.newTask({
			run : function() {
				this.down('grid').getStore().load();
			},
			scope : this,
			interval : 1000 * 60
		});
		this.items = [additionalView];
		this.callParent();
	}
});


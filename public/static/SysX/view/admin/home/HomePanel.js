Ext.define('SysX.view.admin.home.HomePanel', {
	extend : 'Ext.panel.Panel',
	title : SysX.t.HomePanel,
	layout : 'fit',
	initComponent : function() {
		if (environment === 'development' && false) {
			this.items = [];
		} else {
			this.items = [Ext.create('SysX.view.admin.Listing.Grid')];
		}
		this.callParent();
	}
});

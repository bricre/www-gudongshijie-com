Ext.define('SysX.view.admin.home.ConsignmentPerformancePanel', {
	extend : 'Ext.panel.Panel',
	title : SysX.t.ConsignmentPerformance,
	animate : true,
	layout : 'fit',
	requires : ['SysX.model.Consignment'],
	minHeight : 200,
	initComponent : function() {
		this.items = [{
			xtype : 'chart',
			store : Ext.create('Ext.data.JsonStore', {
				autoLoad : true,
				fields : [{
					name : 'day',
					type : 'string'
				}, {
					name : 'quantity',
					type : 'int'
				}],
				proxy : {
					url : modulePath + '/Consignment/Finished-Summary',
					type : 'rest',
					reader : {
						type : 'json',
						root : 'data',
						idProperty : 'day'
					}
				}
			}),
			axes : [{
				type : 'Numeric',
				position : 'left',
				fields : ['quantity'],
				label : {
					renderer : Ext.util.Format.numberRenderer('0,0')
				},
				grid : true,
				minimum : 0

			}, {
				type : 'Category',
				position : 'bottom',
				fields : ['day']
			}],
			series : [{
				type : 'line',
				axis : 'bottom',
				highlight : true,
				xField : 'day',
				yField : 'quantity',
				tips : {
					trackMouse : true,
					width : 140,
					height : 28,
					renderer : function(storeItem, item) {
						this.setTitle(storeItem.get('day') + ': ' + storeItem.get('quantity'));
					}
				}
			}]
		}];
		this.callParent();
	}
});


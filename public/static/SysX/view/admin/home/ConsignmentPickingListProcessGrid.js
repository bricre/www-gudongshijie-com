Ext.define('SysX.view.admin.home.ConsignmentPickingListProcessGrid', {
	extend : 'SysX.view.abstract.Grid',
	title : SysX.t.ConsignmentPickingListProcessSummary,
	selModel : null,
	disableBottomToolbar : true,
	disableItemDbClick : true,
	disableTopToolbar : false,
	disableInsertButton : true,
	disableDeleteButton : true,
	columns : [{
		header : SysX.t.Name,
		dataIndex : 'company_user-name',
		flex : 1
	}, {
		header : SysX.t.ConsignmentPickingListProcess_TotalConsignment,
		dataIndex : 'total_consignment',
		flex : 1
	}, {
		header : SysX.t.ConsignmentPickingListProcess_TotalDuration,
		dataIndex : 'total_duration',
		flex : 1
	}, {
		header : SysX.t.ConsignmentPickingListProcess_AverageConsignmentPerHour,
		dataIndex : 'average_consignment_per_hour',
		flex : 1
	}, {
		header : SysX.t.ConsignmentPickingListProcess_SingleProductConsignment,
		dataIndex : 'single_product_consignment',
		flex : 1
	}, {
		header : SysX.t.ConsignmentPickingListProcess_MultipleProductConsignment,
		dataIndex : 'multiple_product_consignment',
		flex : 1
	}],
	store : Ext.create('SysX.store.abstract.Store', {
		fields : ['company_user-id', 'company_user-name', 'total_consignment', 'total_duration', 'average_consignment_per_hour', 'single_product_consignment', 'multiple_product_consignment'],
		proxy : {
			url : modulePath + '/Public/Consignment-Picking-List-Process',
			type : 'rest',
			reader : {
				type : 'json',
				root : 'data'
			}
		}
	})
});

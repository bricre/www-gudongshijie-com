Ext.define('SysX.view.admin.home.ConsignmentTodaySummaryPanel', {
	extend : 'Ext.panel.Panel',
	title : SysX.t.ConsignmentTodaySummary,
	animate : true,
	layout : 'fit',
	minHeight : 200,
	initComponent : function() {
		var taskRunner = Ext.create('Ext.util.TaskRunner'), task = taskRunner.newTask({
			run : function() {
				this.down('chart').getStore().load();
			},
			scope : this,
			interval : 1000 * 60
		});
		this.tbar = [{
			xtype : 'checkbox',
			fieldLabel : SysX.t.AutoReload,
			uncheckedValue : false,
			listeners : {
				change : function(checkbox, newValue, oldValue, eOpts) {
					if (newValue) {
						task.start();
					} else {
						task.stop();
					}
				}
			}
		}];
		this.items = [{
			xtype : 'chart',
			store : Ext.create('Ext.data.JsonStore', {
				autoLoad : true,
				fields : ['status', 'total'],
				proxy : {
					url : modulePath + '/Consignment/Today-Summary',
					type : 'rest',
					reader : {
						type : 'json',
						root : 'data',
						idProperty : 'status'
					}
				}
			}),
			legend : {
				position : 'top'
			},
			series : [{
				type : 'pie',
				angleField : 'total',
				showInLegend : true,
				label : {
					field : 'status',
					display : 'rotate',
					contrast : true,
					renderer : function(value, label, storeItem, item, i, display, animate, index) {
						return value + ' : ' + storeItem.get('total');
					}
				},
				tips : {
					trackMouse : true,
					width : 140,
					height : 28,
					renderer : function(storeItem, item) {
						this.setTitle(storeItem.get('status') + ': ' + storeItem.get('total'));
					}
				},
				highlight : {
					segment : {
						margin : 20
					}
				}
			}]
		}];
		this.callParent();
	}
});


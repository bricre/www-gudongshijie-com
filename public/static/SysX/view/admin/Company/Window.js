Ext.define('SysX.view.admin.Company.Window', {
	extend : 'SysX.view.abstract.Window',
	singlePanel : true,
	initComponent : function() {
		var defaults = {
			xtype : 'textfield',
			anchor : '100%'
		};
		this.formItems = {
			title : SysX.t.BasicDetails,

			items : [{
				xtype : 'fieldset',
				collapsible : true,
				title : SysX.t.BasicDetails,
				defaults : defaults,
				items : [{
					name : 'id',
					xtype : 'hidden'
				}, {
					name : 'name',
					fieldLabel : SysX.t.Name
				}, {
					name : 'email',
					fieldLabel : SysX.t.Email
				}, {
					name : 'telephone',
					fieldLabel : SysX.t.Telephone
				}, {
					name : 'address_line_1',
					fieldLabel : SysX.t.AddressLine1
				}, {
					name : 'address_line_2',
					fieldLabel : SysX.t.AddressLine2
				}, {
					name : 'address_line_3',
					fieldLabel : SysX.t.AddressLine3
				}, {
					name : 'city',
					fieldLabel : SysX.t.City
				}, {
					name : 'county',
					fieldLabel : SysX.t.County
				}, {
					name : 'post_code',
					fieldLabel : SysX.t.PostCode
				}, {
					name : 'country_iso',
					fieldLabel : SysX.t.Country,
					allowBlank : false,
					xtype : 'combo',
					valueField : 'iso',
					displayField : 'printable_name',
					queryMode : 'local',
					store : Ext.create('SysX.store.Country', {
						autoLoad : true,
						remoteFilter : false
					})
				}]
			}, {
				xtype : 'fieldset',
				collapsible : true,
				title : SysX.t.ReturnAddress,
				defaults : defaults,
				items : [{
					name : 'return_name',
					fieldLabel : SysX.t.Name
				}, {
					name : 'return_address_line_1',
					fieldLabel : SysX.t.AddressLine1
				}, {
					name : 'return_address_line_2',
					fieldLabel : SysX.t.AddressLine2
				}, {
					name : 'return_address_line_3',
					fieldLabel : SysX.t.AddressLine3
				}, {
					name : 'return_city',
					fieldLabel : SysX.t.City
				}, {
					name : 'return_county',
					fieldLabel : SysX.t.County
				}, {
					name : 'return_post_code',
					fieldLabel : SysX.t.PostCode
				}, {
					name : 'return_country_iso',
					fieldLabel : SysX.t.Country,
					allowBlank : false,
					xtype : 'combo',
					valueField : 'iso',
					displayField : 'printable_name',
					queryMode : 'local',
					store : Ext.create('SysX.store.Country', {
						autoLoad : true,
						remoteFilter : false
					})
				}]
			}]
		};

		this.callParent(arguments);
		this.down('form').load({
			method : 'get'
		});
	}
});

Ext.define('SysX.view.admin.CompanyUser.Grid', {
	extend : 'SysX.view.abstract.Grid',
	itemId : 'admin.CompanyUser.Grid',
	store : 'SysX.store.CompanyUser',
	title : SysX.t.CompanyUser,
	columns : [{
		header : SysX.t.ID,
		dataIndex : 'id',
		sortable : true,
		flex : 1
	}, {
		header : SysX.t.UserGroup,
		dataIndex : 'company_user_group_id',
		sortable : true,
		flex : 2,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			return record.data.company_user_group_name;
		}
	}, {
		header : SysX.t.Username,
		dataIndex : 'username',
		sortable : true,
		flex : 2
	}, {
		header : SysX.t.FirstName,
		dataIndex : 'first_name',
		sortable : true,
		flex : 2
	}, {
		header : SysX.t.LastName,
		dataIndex : 'last_name',
		sortable : true,
		flex : 2
	}, {
		header : SysX.t.Status,
		dataIndex : 'status',
		sortable : true,
		renderer : function(value, metaData, record, rowIndex, colIndex, store) {
			var status = SysX.model.CompanyUser.prototype.status;
			metaData.tdAttr = status[value][2];
			return status[value][1];
		},
		filter : {
			type : 'list',
			options : Ext.Object.getValues(Ext.create('SysX.model.CompanyUser').status),
			active : true,
			value : ['ACTIVE']
		},
		flex : 1
	}]
});

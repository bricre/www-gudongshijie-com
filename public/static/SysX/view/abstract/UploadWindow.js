Ext.define('SysX.view.abstract.UploadWindow', {
	extend : 'Ext.window.Window',

	title : SysX.t.FileUpload,
	width : 350,
	maximizable : false,
	modal : true,
	layout : 'fit',
	autoShow : true,
	/**
	 * URL for uploading
	 */
	url : null,
	/**
	 * Addtional params will be passed when submitting
	 */
	params : [],
	/**
	 * Addtional items to be added to the form panel
	 */
	extraItems : [],
	/**
	 * If a grid is set it will be automatically refreshed after successful upload
	 */
	grid : null,

	initComponent : function() {
		if (null === this.url) {
			throw 'URL is undefined for file upload window.';
		}
		this.items = [{
			xtype : 'form',
			fileUpload : true,
			bodyStyle : 'padding:5px',
			items : [{
				name : 'userfile',
				xtype : 'fileuploadfield',
				allowBlank : false,
				fieldLabel : SysX.t.File,
				buttonText : SysX.t.Browse
			}].concat(this.extraItems),
			buttons : [{
				text : SysX.t.Cancel,
				handler : function(button) {
					button.up('window').destroy();
				}
			}, {
				text : SysX.t.Upload,
				handler : this.uploadButtonHandler,
				scope : this
			}]
		}];
		this.callParent();
	},
	uploadButtonHandler : function(button) {
		var window = button.up('window'), form = window.down('form');
		if (form.getForm().isValid()) {
			form.getForm().submit({
				url : this.url,
				params : this.params,
				success : this.uploadSuccessHandler,
				failure : this.uploadFailureHandler,
				scope : this
			});
		}
	},
	uploadSuccessHandler : function(formPanel, action) {
		Ext.Msg.alert(SysX.t.MessageBoxTitle_Success, SysX.t.FileUploadMessage_Success);
		if (this.grid !== null) {
			this.grid.down('pagingtoolbar').doRefresh();
		}
		formPanel.owner.up('window').destroy();
	},
	uploadFailureHandler : function(formPanel, action) {
		Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, SysX.t.FileUploadMessage_Fail + '<br />' + action.result.message);
	}
});

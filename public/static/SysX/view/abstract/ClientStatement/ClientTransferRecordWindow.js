Ext.define('SysX.view.abstract.ClientStatement.ClientTransferRecordWindow', {
	extend : 'Ext.window.Window',
	itemId : 'abstract.ClientStatement.ClientTransferRecordWindow',
	autoShow : true,
	maximizable : true,
	modal : true,
	layout : 'fit',
	width : 600,
	height : 400,
	initComponent : function() {
		this.title = SysX.t.AddClientTransferRecord;

		this.buttons = [{
			text : SysX.t.Cancel,
			itemId : 'cancelButton',
			listeners : {
				click : this.onCancelButtonClick
			}
		}, {
			text : SysX.t.Save,
			itemId : 'saveButton',
			listeners : {
				click : this.onSaveButtonClick
			}
		}];

		this.formItems = {
			items : [{
				name : 'id',
				xtype : 'hidden'
			}, {
				name : 'client_id',
				xtype : 'hidden'
			}, {
				name : 'record_id',
				fieldLabel : SysX.t.ClientID,
				xtype : 'textfield',
				allowBlank : false
			}, {
				name : 'amount',
				fieldLabel : SysX.t.Amount,
				xtype : 'numberfield',
				allowBlank : false,
				hideTrigger : true
			}, {
				name : 'note',
				fieldLabel : SysX.t.Note,
				xtype : 'htmleditor',
				anchor : '100% 100%',
				height : 250
			}, {
				name : 'type',
				xtype : 'hidden',
				value : 'CLIENT_TRANSFER'
			}]
		};

		Ext.apply(this.formItems, {
			bodyStyle : 'padding:5px'
		});

		this.items = [{
			xtype : 'panel',
			border : false,
			anchor : '100% 100%',
			autoDestroy : false,
			defaults : {
				autoScroll : true,
				closable : false,
				border : false,
				xtype : 'form'
			},
			items : this.formItems
		}];

		this.callParent(arguments);

		this.down('form').form.url = modulePath + '/Client-Statement/Add-Client-Transfer-Record';
		this.down('form').getForm().on({
			beforeaction : function(form, action, options) {
				form.owner.up('window').setLoading(true);
			},
			actioncomplete : function(form, action, options) {
				form.owner.up('window').setLoading(false);
			},
			actionfailed : function(form, action, options) {
				form.owner.up('window').setLoading(false);
			}
		});
	},
	onCancelButtonClick : function(button) {
		button.up('window').close();
	},
	onSaveButtonClick : function(button) {
		var window = button.up('window'), form = window.down('form');
		if (form.getForm().isValid()) {
			form.submit({
				success : function(form) {
					window.contentModified = true;
					window.close();
				}
			});
		}
	}
});

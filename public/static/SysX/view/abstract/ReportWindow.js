Ext.define('SysX.view.abstract.ReportWindow', {
	extend : 'Ext.window.Window',

	title : SysX.t.Reports,
	modal : true,
	layout : 'anchor',
	maximizable : true,

	requires : ['Ext.ux.TreePicker'],

	reports : [],

	initComponent : function() {
		this.width = this.width || Ext.getBody().getWidth() * 0.75;
		this.height = this.height || Ext.getBody().getHeight() * 0.75;

		var generateReport = function(toDownload) {
			var report = this.down('treecombo').getValue();
			if (!report) {
				return false;
			}

			var criterias = {}, errorOccuredDuringParsingItems = false, reportCriteriaToolbarItems = [], reportCriteriaToolbars = this.query('toolbar');
			Ext.Array.each(reportCriteriaToolbars, function(item, index, toolbars) {
				if (item.isCriteriaToolbar) {
					reportCriteriaToolbarItems = Ext.Array.merge(reportCriteriaToolbarItems, item.items.items);
				}
			});
			Ext.Array.each(reportCriteriaToolbarItems, function(item, index, length) {
				if (item.name) {
					if (item.validate()) {
						if (item.getValue() !== null) {
							criterias[item.name] = Ext.isFunction(item.getSubmitValue) ? item.getSubmitValue() : item.getValue();
						}
					} else {
						errorOccuredDuringParsingItems = true;
						Ext.Msg.alert(SysX.t.MessageBoxTitle_Alert, SysX.t.Report_FillAllForms);
						return false;
					}
				}
			});

			if (errorOccuredDuringParsingItems === true) {
				return false;
			}

			criterias.report = report;
			criterias.limit = this.down('combo[name=limit]').getValue();

			if (toDownload) {
				criterias.toDownload = true;
				criterias.format = this.down('combo[name=format]').getValue();
				if (Ext.Object.getSize(criterias) > 0) {
					Ext.getCmp('reportDownloadButton').setParams(criterias);
				}
			} else {
				Ext.get('reportResultPanel').mask(SysX.t.Loading);
				Ext.Ajax.request({
					url : modulePath + '/Report',
					method : 'GET',
					params : criterias,
					success : function(response, opts) {
						if (response.status === 200) {
							var responseJson = Ext.decode(response.responseText);
							if (!responseJson.success) {
								Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, responseJson.message);
							}

							Ext.getCmp('reportResultPanel').update(responseJson.html);

						} else {
							Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, SysX.t.Report_ErrorOccured);
						}
					},
					callback : function(options, success, response) {
						Ext.get('reportResultPanel').unmask();
					}
				});
			}
		};

		this.dockedItems = [{
			xtype : 'toolbar',
			dock : 'top',
			items : [{
				xtype : 'tbtext',
				text : SysX.t.Reports
			}, {
				xtype : 'treecombo',
				store : Ext.create('Ext.data.TreeStore', {
					storeId : 'reportsMenuStore',
					fields : [{
						name : 'text',
						type : 'string'
					}, {
						name : 'criterias',
						type : 'auto'
					}, {
						name : 'report',
						type : 'string'
					}],
					root : {
						text : SysX.t.Reports,
						expanded : true,
						children : Ext.clone(this.reports)
					}
				}),
				selectChildren : true,
				canSelectFolders : false,
				editable : false,
				valueField : 'report',
				itemTreeClick : function(view, record, item, index, e, eOpts, treeCombo) {
					if (record.get('leaf') === true) {
						var reportCriteriaContainer = treeCombo.up('window').down('container[itemId=reportCriteriaContainer]'), toolbars = [], newToolbar = function() {
							return Ext.create('Ext.toolbar.Toolbar', {
								isCriteriaToolbar : true,
								enableOverflow : true,
								border : false
							});
						}, toolbar = newToolbar();

						reportCriteriaContainer.removeAll();
						for (var i = 0, j = record.data.criterias.length; i < j; i++) {
							var criteria = record.data.criterias[i];
							//'---' Doesn't exist in Ext but we create one to force adding a new toolbar here
							if (Ext.isString(criteria) && criteria == '---') {
								reportCriteriaContainer.add(toolbar);
								toolbar = newToolbar();
							} else {
								if (criteria.xtype == 'numberfield' || criteria.xtype == 'textfield') {
									criteria.listeners = Ext.Object.merge(Ext.isObject(criteria.listeners) ? criteria.listeners : {}, {
										specialkey : function(field, e) {
											if (e.getKey() == e.ENTER) {
												field.up('window').down('toolbar[dock=top]').down('[itemId=reportGenerateButton]').fireEvent('click');
											}
										}
									});
								}
								toolbar.add(record.data.criterias[i]);
							}
						}
						reportCriteriaContainer.add(toolbar);

						Ext.getCmp('reportResultPanel').update('');
						treeCombo.setValue(record.get('report'));
						treeCombo.setRawValue(record.get('text'));
						treeCombo.collapse();
					}
				}
			}, '-', {
				text : SysX.t.Generate,
				itemId : 'reportGenerateButton',
				listeners : {
					click : Ext.bind(generateReport, this, [false])
				}
			}, {
				xtype : 'button',
				text : SysX.t.Download,
				href : modulePath + '/Report',
				id : 'reportDownloadButton',
				hrefTarget : '_blank',
				listeners : {
					click : Ext.bind(generateReport, this, [true])
				}
			}, {
				xtype : 'combo',
				editable : false,
				name : 'format',
				value : 'Excel',
				store : ['Excel', 'CSV']
			}, {
				xtype : 'tbtext',
				text : SysX.t.NumberOfReturnedResult
			}, {
				xtype : 'combo',
				editable : false,
				name : 'limit',
				value : '1000',
				store : ['1000', '2000', '3000', '4000', '5000']
			}]
		}];

		this.items = [{
			xtype : 'container',
			itemId : 'reportCriteriaContainer',
			items : [{
				xtype : 'toolbar',
				enableOverflow : true,
				border : false,
				items : [{
					xtype : 'tbtext',
					text : SysX.t.Report_SelectReport
				}]
			}]
		}, {
			xtype : 'box',
			id : 'reportResultPanel',
			cls : 'x-panel-body',
			padding : '20px',
			anchor : '100% 100%',
			overflowX : 'auto',
			overflowY : 'auto',
			border : 1,
			html : SysX.t.Report_InitialMessage
		}];
		this.callParent();
	}
});

Ext.define('SysX.view.abstract.Window', {
	extend : 'Ext.window.Window',
	autoShow : true,
	maximizable : true,
	modal : true,
	layout : 'fit',
	formItems : {},
	contentModified : false,
	store : null,
	singlePanel : false,
	cancelButton : null,
	saveButton : null,

	initComponent : function() {
		var store = this.getStore(), textfields;

		this.width = this.width || Ext.getBody().getWidth() * 0.75;
		this.height = this.height || Ext.getBody().getHeight() * 0.75;

		this.cancelButton = {
			text : SysX.t.Cancel,
			itemId : 'cancelButton',
			listeners : {
				click : this.onCancelButtonClick
			}
		};

		this.saveButton = {
			text : SysX.t.Save,
			itemId : 'saveButton',
			listeners : {
				click : this.onSaveButtonClick
			}
		};

		this.on({
			single : true,
			'beforeinitcomponent' : this.onBeforeInitComponent,
			'afterinitcomponent' : this.onAfterInitComponent
		});
		this.onBeforeInitComponent();
		this.fireEvent('beforeinitcomponent', this);

		Ext.apply(this.formItems, {
			bodyStyle : 'padding:5px',
			buttons : [this.cancelButton, this.saveButton]
		});

		if (this.singlePanel) {
			this.items = [{
				xtype : 'form',
				autoScroll : true,
				closable : false,
				border : false,
				bodyStyle : 'padding:5px',
				items : this.formItems.items,
				buttons : [this.cancelButton, this.saveButton]
			}];
			this.title = this.title || this.formItems.title;
		} else {
			this.items = [{
				xtype : 'tabpanel',
				activeItem : 0,
				border : false,
				anchor : '100% 100%',
				autoDestroy : false,
				defaults : {
					autoScroll : true,
					closable : false,
					border : false,
					xtype : 'form'
				},
				items : this.formItems
			}];
		}

		this.callParent(arguments);

		//Allow ENTER to submit form
		textfields = this.query('field');
		Ext.each(textfields, function(item, index) {
			item.on('specialkey', function(field, e) {
				if (e.getKey() === e.ENTER) {
					field.up('window').down('button[itemId=saveButton]').fireEvent('click', this);
				}
			});
		});

		this.down('form').form.url = store.proxy.url;
		this.down('form').getForm().on({
			beforeaction : function(form, action, options) {
				form.owner.up('window').setLoading(true);
			},
			actioncomplete : function(form, action, options) {
				form.owner.up('window').setLoading(false);
			},
			actionfailed : function(form, action, options) {
				form.owner.up('window').setLoading(false);
			}
		});

		this.fireEvent('afterinitcomponent', this);
		this.onAfterInitComponent();
	},
	reloadForm : function() {
		if (this.down('form').getValues().id) {
			this.down('form').load({
				method : 'get',
				params : {
					id : this.down('form').getValues().id
				}
			});
		}
	},
	getStore : function() {
		if (Ext.isObject(this.store)) {
			return this.store;
		}
		if (Ext.isString(this.store)) {
			return Ext.create(this.store);
		}
		return Ext.create('SysX.store.' + this.getModuleName().replace(/admin\./, '').replace(/client\./, ''));
	},
	getModuleName : function() {
		return Ext.getClassName(this).replace(/^SysX\.view\./, '').replace(/\.Window$/, '');
	},
	onBeforeInitComponent : Ext.emptyFn,
	onAfterInitComponent : Ext.emptyFn,
	onCancelButtonClick : function(button) {
		button.up('window').close();
	},
	onSaveButtonClick : function(button) {
		var window = button.up('window'), form = button.up('form');
		if (form.getForm().isValid()) {
			form.submit({
				success : function(form) {
					if (window) {
						window.contentModified = true;
						window.close();
					}
				}
			});
		}
	}
});

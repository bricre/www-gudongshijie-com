Ext.define('SysX.view.abstract.Grid', {
	// Inferited configs
	extend : 'Ext.grid.Panel',
	itemid : 'gridPanel',
	enableColumnHide : false,
	closable : false,
	border : false,
	autoScroll : true,

	viewConfig : {
		preserveScrollOnRefresh : true
	},
	layout : 'fit',
	loadMask : true,
	selModel : {
		selType : 'checkboxmodel',
		mode : 'MULTI'
	},
	requires : ['Ext.ux.grid.FiltersFeature'],
	features : [{
		ftype : 'filters',
		encode : true,
		phpMode : true,
		filters : []
	}],

	// Module spcific configs
	storeConfig : {},
	defaultFilters : [],
	defaultSorters : [],
	isShown : false,
	disableTopToolbar : false,
	disableInsertButton : false,
	disableDeleteButton : false,
	topToolbarExtraItems : [],
	disableBottomToolbar : false,
	disableItemDbClick : false,
	disableStoreAutoLoad : false,

	initComponent : function() {
		var selectFields = [], topToolbarButtons = [];

		this.on({
			'beforeinitcomponent' : this.onBeforeInitComponent,
			'afterinitcomponent' : this.onAfterInitComponent
		});
		this.fireEvent('beforeinitcomponent', this);

		this.store = this.getStore();

		if (!this.disableItemDbClick && this.columns[0].xtype !== 'actioncolumn') {
			this.columns.unshift({
				xtype : 'actioncolumn',
				width : 16,
				items : [{
					iconCls : 'x-column-action-edit',
					handler : function(grid, rowIndex, colIndex) {
						var record = grid.getStore().getAt(rowIndex);
						this.onGridPanelItemDbClick(grid, record);
					},
					scope : this
				}]
			});
		}

		// initComponnent
		this.callParent(arguments);

		// Only select fields required in this grid
		for (var i = this.columns.length - 1; i >= 0; i--) {
			if (this.columns[i].dataIndex && !this.columns[i].excludeFromSelect) {
				selectFields.push(this.columns[i].dataIndex);
			}
		}
		this.store.getProxy().setExtraParam('selectFields', selectFields.join(','));

		// Set default sort
		if (this.defaultSorters.length > 0) {
			Ext.each(this.defaultSorters, function(item, index) {
				this.store.sort(item.dataIndex, item.order ? item.order : 'DESC', null, false);
			}, this);
		}

		// Top Toolbar
		if (!this.disableTopToolbar) {
			if (!this.topToolbar) {
				if (!this.disableInsertButton) {
					topToolbarButtons[topToolbarButtons.length] = {
						text : SysX.t.Insert,
						itemId : 'insertButton',
						listeners : {
							click : this.onGridPanelInsertButtonClick
						}
					};
				}
				if (!this.disableDeleteButton) {
					topToolbarButtons[topToolbarButtons.length] = {
						text : SysX.t.Delete,
						itemId : 'deleteButton',
						listeners : {
							click : this.onGridPanelDeleteButtonClick
						}
					}
				}
				topToolbarButtons = Ext.Array.merge(topToolbarButtons, this.topToolbarExtraItems);

				this.addDocked(Ext.create('Ext.toolbar.Toolbar', {
					itemId : 'toptoolbar',
					dock : 'top',
					items : topToolbarButtons
				}));
			} else {
				this.addDocked(Ext.create('Ext.toolbar.Toolbar', Ext.apply(this.topToolbar, {
					itemId : 'toptoolbar',
					dock : 'top'
				})));
			}
		}

		// Bottom(paging) Toolbar
		if (!this.bottomToolbar && !this.disableBottomToolbar) {
			this.addDocked(Ext.create('Ext.toolbar.Paging', {
				itemId : 'pagingtoolbar',
				dock : 'bottom',
				store : this.getStore(),
				//pageSize : this.getStore().pageSize,
				displayInfo : true,
				items : ['-', {
					xtype : 'tbtext',
					text : SysX.t.RecordsPerPage
				}, {
					xtype : 'combo',
					width : 50,
					store : Ext.create('Ext.data.ArrayStore', {
						fields : ['display', 'value'],
						data : [['50', 50], ['100', 100], ['200', 200]]
					}),
					queryMode : 'local',
					displayField : 'display',
					valueField : 'value',
					listeners : {
						afterrender : function(combo) {
							var pagingToolbar = combo.up('pagingtoolbar');
							combo.suspendEvents();
							combo.select(pagingToolbar.getStore().pageSize);
							combo.resumeEvents();
						},
						change : function(combo, newValue, oldValue) {
							var pagingToolbar = combo.up('pagingtoolbar');
							pagingToolbar.getStore().pageSize = newValue;
							pagingToolbar.getStore().loadPage(1);
						}
					}
				}, '-', {
					text : SysX.t.Print,
					handler : function(button) {
						Ext.ux.grid.Printer.printAutomatically = true;
						Ext.ux.grid.Printer.stylesheetPath = '/static/Ext/ux/grid/gridPrinterCss/print.css';
						Ext.ux.grid.Printer.print(button.up('grid'));
					}
				}]
			}));
		}

		if (this.filters) {
			this.filters.createFilters();
		}

		this.on('afterrender', function() {
			this.getStore().load();
		}, this);

		// Handles item edit (double click)
		this.on('itemdblclick', this.onGridPanelItemDbClick, this);

		this.fireEvent('afterinitcomponent', this);
	},

	onGridPanelItemDbClick : function(gridView, record, item, index) {
		if (this.disableItemDbClick) {
			return false;
		}

		var moduleName = this.getModuleName(), controller = app.getController('SysX.controller.' + moduleName), view = controller.editRecord(record);
		// set this.editRecord to return boolean to prevent editing.
		if (!Ext.isObject(view)) {
			return true;
		}

		view.on('close', function() {
			if (gridView.panel.getDockedComponent('pagingtoolbar') && view.contentModified === true) {
				gridView.panel.getDockedComponent('pagingtoolbar').doRefresh();
			}
		});
	},

	onGridPanelInsertButtonClick : function(button, e, opts) {
		var gridPanel = button.up('gridpanel'), moduleName = gridPanel.getModuleName(), controller = app.getController('SysX.controller.' + moduleName), view = controller.getWindow();

		view.on('close', function() {
			if (gridPanel.getDockedComponent('pagingtoolbar') && view.contentModified === true) {
				gridPanel.getDockedComponent('pagingtoolbar').doRefresh();
			}
		});
	},

	onGridPanelDeleteButtonClick : function(button, e, opts) {
		var gridPanel = button.up('gridpanel'), records = gridPanel.getSelectionModel().getSelection();

		if (records.length < 1) {
			return false;
		}
		Ext.Msg.show({
			buttons : Ext.Msg.YESNO,
			icon : Ext.MessageBox.QUESTION,
			title : SysX.t.Confirm,
			msg : SysX.t.ConfirmDelete,
			fn : function(button) {
				if (button == 'yes') {
					gridPanel.getStore().remove(records);
					if (gridPanel.getStore().autoSync !== true) {
						gridPanel.getStore().sync();
					}
				}
			}
		});
	},
	getStore : function() {
		var store, proxy, storeConfig;
		if (!Ext.isObject(this.store)) {
			storeConfig = Ext.clone(this.storeConfig);
			if (this.groupField) {
				storeConfig.groupField = this.groupField;
			}
			store = Ext.create(this.store, storeConfig);
			proxy = Ext.clone(store.getProxy());
			proxy.extraParams = {};
			store.setProxy(proxy);
			this.store = store;
		}
		return this.store;
	},

	getModuleName : function() {
		return Ext.getClassName(this).replace(/^SysX\.view\./, '').replace(/\.Grid$/, '');
	},

	onBeforeInitComponent : Ext.emptyFn,
	onAfterInitComponent : Ext.emptyFn
});

Ext.define('SysX.view.MobileAdmin.Login', {
	xtype : 'viewlogin',
	extend : 'Ext.form.Panel',
	requires : ['Ext.field.Password', 'Ext.field.Text'],
	config : {
		fullscreen : true,
		url : modulePath + '/public/login',
		defaults : {
			labelWidth : 100
		},
		items : [{
			xtype : 'textfield',
			name : 'username',
			label : 'Username'
		}, {
			xtype : 'passwordfield',
			name : 'password',
			label : 'Password'
		}, {
			xtype : 'button',
			itemId : 'submit',
			text : 'Login',
			ui : 'action'
		}]
	}
});

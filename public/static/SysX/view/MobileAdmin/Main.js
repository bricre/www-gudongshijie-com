Ext.define('SysX.view.MobileAdmin.Main', {
	extend : 'Ext.NavigationView',
	xtype : 'main',
	requires : ['Ext.dataview.List', 'Ext.data.Store'],

	config : {
		fullscreen : true,
		autoDestroy : true,
		items : [{
			title : 'Mobile Admin',
			xtype : 'list',
			store : {
				fields : ['title', 'view'],
				data : [{
					title : 'Product',
					view : 'Product'
				}, {
					title : 'Container',
					view : 'Container'
				}, {
					title : 'Update Inventory',
					view : 'ContainerProductTransaction'
				}]
			},
			itemTpl : '{title}',
			listeners : {
				itemsingletap : function(view, index, target, record) {
					var mainView = view.getParent(), viewClass = 'SysX.view.MobileAdmin.' + record.get('view') + '.Main';
					mainView.push(Ext.create(viewClass));
				}
			},
			selectItem : function(view, record) {

			}
		}],
		navigationBar : {
			items : [{
				xtype : 'button',
				text : 'Logout',
				ui : 'action',
				align : 'right',
				handler : function() {
					Ext.Ajax.request({
						url : modulePath + '/public/logout',
						success : function(response) {
							location.reload(false);
						}
					});
				}
			}]
		}
	}
});

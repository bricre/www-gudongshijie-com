Ext.define('SysX.view.MobileAdmin.ContainerProductTransaction.List', {
	xtype : 'viewcontainerproducttransactionlist',
	extend : 'Ext.dataview.List',
	config : {
		title : 'Inventory',
		scrollable : true,
		emptyText : 'No record found',
		store : {
			xclass : 'SysX.store.ContainerProductTransaction'
		},
		plugins : [{
			xclass : 'Ext.plugin.ListPaging',
			autoPaging : true
		}],
		itemTpl : "{product_id} x {quantity} @ Container {container_id}<br /> {update_time:date('Y-m-d H:i:s')}",
		iconCls : 'organize',
		items : [{
			xtype : 'toolbar',
			docked : 'top',
			items : [{
				xtype : 'searchfield',
				component : {
					xtype : 'input',
					type : 'tel'
				}
			}, {
				xtype : 'button',
				text : 'Search',
				itemId : 'search'
			}]
		}]
	}
}); 
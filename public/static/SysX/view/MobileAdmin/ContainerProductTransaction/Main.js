Ext.define('SysX.view.MobileAdmin.ContainerProductTransaction.Main', {
	xtype : 'viewcontainerproducttransactionmain',
	extend : 'Ext.tab.Panel',
	requires : ['SysX.view.MobileAdmin.ContainerProductTransaction.ModifyStock', 'SysX.view.MobileAdmin.ContainerProductTransaction.MoveStock'],
	config : {
		items : [{
			xtype : 'viewcontainerproducttransactionmodifystock'
		}, {
			xtype : 'viewcontainerproducttransactionmovestock'
		}]
	}
});

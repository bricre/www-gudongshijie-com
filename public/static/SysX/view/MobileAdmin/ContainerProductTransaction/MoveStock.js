Ext.define('SysX.view.MobileAdmin.ContainerProductTransaction.MoveStock', {
	xtype : 'viewcontainerproducttransactionmovestock',
	extend : 'Ext.form.Panel',
	requires : ['Ext.field.Radio', 'Ext.field.Number'],
	config : {
		title : 'Move Stock',
		url : modulePath + '/Container-Product-Transaction/Move-Stock',
		items : [{
			xtype : 'numberfield',
			component : {
				xtype : 'input',
				type : 'tel'
			},
			name : 'product_id',
			label : 'Product'
		}, {
			xtype : 'numberfield',
			component : {
				xtype : 'input',
				type : 'tel'
			},
			name : 'container_id_from',
			label : 'From'
		}, {
			xtype : 'numberfield',
			component : {
				xtype : 'input',
				type : 'tel'
			},
			name : 'container_id_to',
			label : 'To'
		}, {
			xtype : 'numberfield',
			component : {
				xtype : 'input',
				type : 'tel'
			},
			name : 'quantity',
			label : 'Quantity'
		}, {
			xtype : 'container',
			layout : 'hbox',
			defaults : {
				flex : 1
			},
			items : [{
				xtype : 'button',
				itemId : 'submit',
				text : 'Submit',
				ui : 'action'
			}, {
				xtype : 'spacer'
			}, {
				xtype : 'button',
				text : 'Cancel',
				ui : 'decline'
			}]
		}]
	}
});

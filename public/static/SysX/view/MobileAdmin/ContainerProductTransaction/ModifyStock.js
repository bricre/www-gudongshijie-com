Ext.define('SysX.view.MobileAdmin.ContainerProductTransaction.ModifyStock', {
	xtype : 'viewcontainerproducttransactionmodifystock',
	extend : 'Ext.form.Panel',
	requires : ['Ext.field.Radio', 'Ext.field.Number'],
	config : {
		title : 'Update Inventory',
		url : modulePath + '/Container-Product-Transaction',
		items : [{
			xtype : 'radiofield',
			name : 'add',
			label : 'Add',
			value : true,
			action : 'add',
			checked : true
		}, {
			xtype : 'radiofield',
			name : 'add',
			label : 'Minus',
			action : 'minus',
			value : false
		}, {
			xtype : 'numberfield',
			component : {
				xtype : 'input',
				type : 'tel'
			},
			name : 'product_id',
			label : 'Product'
		}, {
			xtype : 'numberfield',
			component : {
				xtype : 'input',
				type : 'tel'
			},
			name : 'container_id',
			label : 'Container'
		}, {
			xtype : 'numberfield',
			component : {
				xtype : 'input',
				type : 'tel'
			},
			name : 'quantity',
			label : 'Quantity'
		}, {
			xtype : 'container',
			layout : 'hbox',
			defaults : {
				flex : 1
			},
			items : [{
				xtype : 'button',
				itemId : 'submit',
				text : 'Submit',
				ui : 'action'
			}, {
				xtype : 'spacer'
			}, {
				xtype : 'button',
				text : 'Cancel',
				ui : 'decline'
			}]
		}]
	}
});

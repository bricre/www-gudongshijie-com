Ext.define('SysX.view.MobileAdmin.ContainerProduct.List', {
	xtype : 'viewcontainerproductlist',
	extend : 'Ext.dataview.List',
	config : {
		title : 'Stock',
		scrollable : true,
		emptyText : 'No record found',
		store : {
			xclass : 'SysX.store.ContainerProduct'
		},
		itemTpl : '{product_id} x {quantity} @ Container {container_id}',
		iconCls : 'settings',
		items : [{
			xtype : 'toolbar',
			docked : 'top',
			items : [{
				xtype : 'searchfield',
				component : {
					xtype : 'input',
					type : 'tel'
				}
			}, {
				xtype : 'button',
				text : 'Search',
				itemId : 'search'
			}]
		}, {
			xtype : 'toolbar',
			docked : 'bottom',
			layout : 'vbox',
			items : [{
				text : 'Up',
				handler : function(button, event) {
					var list = button.up('list'), searchField = list.down('searchfield'), searchButton = list.down('button[itemId=search]');
					searchField.setValue(parseInt(searchField.getValue(), 10) + 1);
					searchButton.fireEvent('tap', searchButton);
				}
			}, {
				text : 'Down',
				handler : function(button, event) {
					var list = button.up('list'), searchField = list.down('searchfield'), searchButton = list.down('button[itemId=search]');
					searchField.setValue(parseInt(searchField.getValue(), 10) - 1);
					searchButton.fireEvent('tap', searchButton);
				}
			}]
		}],
		onItemDisclosure : true
	}
});

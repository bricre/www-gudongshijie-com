Ext.define('SysX.view.MobileAdmin.Product.Main', {
	xtype : 'viewproductmain',
	extend : 'Ext.tab.Panel',
	requires : ['Ext.dataview.List', 'Ext.field.Search', 'Ext.util.Filter', 'SysX.store.ContainerProduct', 'SysX.store.ContainerProductTransaction', 'Ext.plugin.ListPaging', 'SysX.view.MobileAdmin.ContainerProduct.List', 'SysX.view.MobileAdmin.ContainerProductTransaction.List'],
	config : {
		title : 'Product',
		tabBarPosition : 'bottom',
		items : [{
			xtype : 'viewcontainerproductlist',
			itemId : 'stock'
		}, {
			xtype : 'viewcontainerproducttransactionlist',
			itemId : 'inventory'
		}]
	}
});

Ext.define('SysX.view.MobileAdmin.Container.Main', {
	xtype : 'viewcontainermain',
	extend : 'Ext.tab.Panel',
	requires : ['Ext.dataview.List', 'Ext.field.Search', 'Ext.util.Filter', 'SysX.store.ContainerProduct', 'SysX.store.ContainerProductTransaction', 'Ext.plugin.ListPaging', 'SysX.view.MobileAdmin.ContainerProduct.List', 'SysX.view.MobileAdmin.ContainerProductTransaction.List'],
	config : {
		title : 'Container',
		tabBarPosition : 'bottom',
		items : [{
			xtype : 'viewcontainerproductlist',
			itemId : 'stock'
		}, {
			xtype : 'viewcontainerproducttransactionlist',
			itemId : 'inventory'
		}]
	}
});

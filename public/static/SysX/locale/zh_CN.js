Ext.define("SysX.locale.zh_CN.Locale", {
	override : "SysX.Locale",
	//Global words
	ID : 'ID',
	YES : '是',
	NO : '否',

	Name : '名称',
	Group : '组',
	Internal : '内部',
	External : '外部',
	Status : '状态',
	FileUpload : '文件上传',
	Insert : "添加",
	Delete : "删除",
	Save : "保存",
	Cancel : "取消",
	Upload : '上传',
	Print : '打印',
	Invoice : '收据',
	Check : '确认',
	Time : '时间',
	Type : '种类',
	Loading : '读取中...',
	Initializing : '正在初始化...',
	Enter : '确定',
	Days : '天',
	Generate : '生成',
	Download : '下载',
	Total : '总计',
	From : '从',
	To : '至',
	User : '用户',
	Login : '登陆',
	Logout : '退出',
	Browse : '浏览',
	Fee : '费用',
	File : '文件',
	Pay : '付款',
	Connect : '连接',
	Title : '标题',
	Date : '日期',
	Progress : '进度',
	Language : '语言',
	Volume : '体积',
	Pallet : '托盘',
	DownloadTemplateFile : '下载模板文件',
	BatchUpload : '批量上传',
	RecordsPerPage : '每页记录数',
	Submit : '提交',
	Staff : '员工',
	Action : '操作',
	LoggedInAs : '当前用户: ',
	Confirm : '确认',
	ConfirmDelete : '你确认删除这些记录么?',
	Url : '网址',
	View : '查看',

	FileUploadMessage_Success : '文件上传成功!',
	FileUploadMessage_Fail : '文件上传失败!',

	//Status
	Status_ACTIVE : '正常',
	Status_INACTIVE : '停用',
	Status_DELETED : '已删除',
	Status_PREPARING : '准备中',
	Status_DISPATCHED : '已发送',
	Status_REVIEWING : '审核中',
	Status_FINISHED : '已完成',
	Status_PENDING : '待处理',
	Status_PROCESSING : '处理中',
	Status_PICKING : '拣货中',
	Status_OUT_OF_STOCK : '售罄',
	Status_DEPRECATED : '已下架',
	Status_USED : '已用',
	Status_CREATED : '未用',
	Status_DOWNLOADED : '已下载',
	Status_PROBLEM : '有问题',
	Status_OPEN : '未解决',
	Status_CLOSED : '已解决',
	Status_RECEIVED : '已收货',
	Status_STOCKED : '已上架',
	Status_CANCELLED : '已取消',
	Status_DORMANT : '静止',
	Status_CLEARING : '清关中',
	Status_NEW : '新上架',
	Status_ENQUIRING : '查询中',
	Status_INSTOCK : '已到货',
	Status_SOLD : '已售出',

	//Message Box Title
	MessageBoxTitle_Notice : '注意',
	MessageBoxTitle_Alert : '警告',
	MessageBoxTitle_Error : '错误',
	MessageBoxTitle_Success : '成功',

	//Global Menu
	Clients : '客户',
	Tickets : '留言',
	Reports : '报表',
	Settings : '设置',
	ClientGroup : '客户组',
	UserManagement : '用户管理',
	Users : '用户',
	UserGroup : '用户组',
	Misc : '杂项',
	Country : '国家',
	Account : '账户',

	BasicInfo : '基本信息',
	BasicDetails : '基本细节',
	ConsignmentSettings : '订单设置',
	Accounting : '财务',

	//Client
	Client_IsInternal : '内部共享客户',
	Client_IsExternal : '外部共享客户',
	IsVip : 'VIP',
	CompanyName : '公司名',
	Email : '电子邮件',
	Telephone : '电话',
	AddressLine1 : '地址行1',
	AddressLine2 : '地址行2',
	AddressLine3 : '地址行3',
	City : '城市',
	County : '省/郡',
	PostCode : '邮编',
	CountryISO : '国家',
	CreditLimit : '透支限额',
	CreateTime : '创建时间',
	NextChargingDate : '下次扣费日期',
	Note : '备注',
	BulkOrderReferenceField : '批量订单订单栏',
	Balance : '余额',
	ClientGroupName : '客户组',
	ConsignmentSummaryInLastSevenDays : '过去7天订单统计',

	//ClientStatement
	ClientStatement : '账单',
	Amount : '数额',
	UpdateTime : '更新时间',
	AddClientTransferRecord : '添加客户转账记录',

	//ClientStatementType
	ClientStatementType_CONSIGNMENT_FEE : '订单费用',
	ClientStatementType_WAREHOUSE_DISPATCH_FEE : '仓库发货费用',
	ClientStatementType_REFUND : '退货',
	ClientStatementType_MEMBERSHIP_FEE : '会员月费',
	ClientStatementType_ADJUSTMENT : '调整',
	ClientStatementType_TOP_UP : '充值',
	ClientStatementType_CLIENT_TRANSFER : '客户转账',

	//ClientUser
	//CompanyUser
	CompanyUser : '管理员用户',
	ClientUser : '客户用户',
	Username : '用户名',
	Password : '密码',
	ReEnterPassword : '再次输入密码',
	ApiKey : 'API秘钥',
	FirstName : '名',
	LastName : '姓',
	IsManager : '是否是经理',
	ClientUserGroupName : '客户用户组',
	CompanyUserGroupName : '管理员用户组',
	ClientUserGroup : '客户用户组',
	CompanyUserGroup : '管理员用户组',
	AdvancedLogin : '直接登陆',
	LoginAsThisUser : '以该用户身份登陆',
	ClientUserMessage_NoAPIKey : "您还没有API秘钥，点击下面的按钮生成新秘钥",
	GenerateNewAPIKey : '生成新秘钥',
	CompanyUserMessage_NoAPIKey : "您还没有API秘钥，点击下面的按钮生成新秘钥",
	ReturnAddress : '退货地址',

	//ClientUserGroupPrivilege
	//CompanyUserGroupPrivilege
	UserGroupPrivilege : '用户组权限',
	PrivilegeCode : '权限码',
	PrivilegeDescription : '权限说明',
	IsDenied : '禁止',
	IsAllowed : '允许',

	//Country
	PrintableName : '打印名称',
	ISO : 'ISO 代码',
	ISO3 : 'ISO 3 代码',
	Numcode : '数字代码',

	//Ticket
	Subject : '标题',
	UserName : '用户姓名',
	TicketMessage_StatusUpdateSuccess : '状态更新成功！',
	TicketMessage_StatusUpdateFailed : '状态更新失败，请联系开发人员',

	//User
	PleaseLogin : '请登陆',
	User_SessionExpired : '登陆超时，请重新登陆。',

	//Vendor
	Vendor : '供应商',

	//Listing
	Description : '说明',
	Listing : '货品',
	Cost : '成本',
	Price : '售价',
	DeliveryFee : '邮费',
	RecommendedPrice : '建议售价',

	//HomePanel
	HomePanel : '首页',
	LastLoginTime : '上次登录时间',
	InLastXDays : ' (过去7天内)',
	AutoReload : '自动刷新',

	//Config
	Config : '设置',

	LAST : '最后'
});

Ext.define("SysX.locale.zh_CN.view.Viewport", {
	override : "SysX.view.Viewport",
	homeText : "首页"
});


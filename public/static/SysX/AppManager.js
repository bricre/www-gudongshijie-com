Ext.define('SysX.AppManager', {
	statics : {
		addModuleGrid : function(button) {
			if (!button.module) {
				return false;
			}

			var xtype = 'SysX.controller.' + button.module, mainPanel = Ext.getCmp('mainPanel'), config = Ext.isObject(button.config) ? button.config : {}, grid, controller;

			grid = mainPanel.getComponent(button.module + '.Grid');
			if (grid) {
				mainPanel.setActiveTab(grid);
			} else {
				var loadMask = new Ext.LoadMask({
					target : viewport
				});
				loadMask.show();
				controller = SysX.AppManager.getController(xtype);
				grid = controller.getGridPanel(Ext.apply(config, {
					closable : true
				}));

				mainPanel.add(grid);
				mainPanel.setActiveTab(grid);
				loadMask.destroy();
			}
		},
		openModuleWindow : function(button) {
			if (!button.module) {
				return false;
			}

			var loadMask = new Ext.LoadMask({
				target : viewport
			}).show();
			var xtype = 'SysX.controller.' + button.module, config = Ext.isObject(button.config) ? button.config : {}, view;
			SysX.AppManager.getController(xtype).getWindow(config).show();
			loadMask.destroy();
		},
		editRecord : function(button) {
			if (!button.module) {
				return false;
			}

			var loadMask = new Ext.LoadMask({
				target : viewport
			}).show();
			var xtype = 'SysX.controller.' + button.module, config = Ext.isObject(button.config) ? button.config : {}, view;
			SysX.AppManager.getController(xtype).editRecord(button.record);
			loadMask.destroy();
		},
		getController : function(controllerClass) {
			try {
				var controller = app.getController(controllerClass);
				return controller;
			} catch(error) {
				Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, 'Undefined module ' + controllerClass + ', please contact developer!');
				return false;
			}
		}
	}
});

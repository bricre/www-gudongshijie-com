Ext.define('SysX.model.Media', {
	extend : 'Ext.data.Model',
	fields : [ {
		name : 'id',
		type : 'int'
	}, {
		name : 'type',
		type : 'string'
	}, {
		name : 'file_path',
		type : 'string'
	}, {
		name : 'file_name',
		type : 'string'
	}, {
		name : 'file_extension',
		type : 'string'
	}, {
		name : 'description',
		type : 'string'
	}, {
		name : 'update_time',
		type : 'string'
	} ]
});
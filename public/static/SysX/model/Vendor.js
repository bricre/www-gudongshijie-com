Ext.define('SysX.model.Vendor', {
	extend: 'Ext.data.Model',
	fields: [
		'id',
		'name',
		'url',
		'address_line_1',
		'address_line_2',
		'address_line_3',
		'city',
		'county',
		'post_code',
		'country_iso',
		'telephone',
		'email',
		'vendor',
		'status'
	],
	status: {
		'ACTIVE': ['ACTIVE', SysX.t.Status_ACTIVE, 'style="color:green"'],
		'INACTIVE': ['INACTIVE', SysX.t.Status_INACTIVE, 'style="color:red"']
	}
});
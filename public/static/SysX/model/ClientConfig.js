Ext.define('SysX.model.ClientConfig', {
	extend : 'Ext.data.Model',
	fields : [{
		name : 'company_id',
		type : 'int'
	}, {
		name : 'config_code',
		type : 'string'
	}, {
		name : 'value',
		type : 'auto'
	}]
});

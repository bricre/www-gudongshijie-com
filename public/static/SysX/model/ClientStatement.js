Ext.define('SysX.model.ClientStatement', {
	extend : 'Ext.data.Model',
	fields : [{
		name : 'id',
		type : 'int'
	}, {
		name : 'client_id',
		type : 'int'
	}, {
		name : 'record_id',
		type : 'int'
	}, {
		name : 'amount',
		type : 'float'
	}, {
		name : 'note',
		type : 'string'
	}, {
		name : 'update_time',
		type : 'date',
		dateFormat : 'c'
	}, {
		name : 'type',
		type : 'string'
	}, {
		name : 'status',
		type : 'string'
	}],
	type : {
		'CONSIGNMENT_FEE' : ['CONSIGNMENT_FEE', SysX.t.ClientStatementType_CONSIGNMENT_FEE],
		'WAREHOUSE_DISPATCH_FEE' : ['WAREHOUSE_DISPATCH_FEE', SysX.t.ClientStatementType_WAREHOUSE_DISPATCH_FEE],
		'REFUND' : ['REFUND', SysX.t.ClientStatementType_REFUND],
		'MEMBERSHIP_FEE' : ['MEMBERSHIP_FEE', SysX.t.ClientStatementType_MEMBERSHIP_FEE],
		'ADJUSTMENT' : ['ADJUSTMENT', SysX.t.ClientStatementType_ADJUSTMENT],
		'TOP_UP' : ['TOP_UP', SysX.t.ClientStatementType_TOP_UP],
		'CLIENT_TRANSFER' : ['CLIENT_TRANSFER', SysX.t.ClientStatementType_CLIENT_TRANSFER]
	},
	status : {
		'INACTIVE' : ['INACTIVE', SysX.t.Status_INACTIVE, 'style="color:red"'],
		'ACTIVE' : ['ACTIVE', SysX.t.Status_ACTIVE, 'style="color:green"']
	}

});

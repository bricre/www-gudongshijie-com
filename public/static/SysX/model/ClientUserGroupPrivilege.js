Ext.define('SysX.model.ClientUserGroupPrivilege', {
	extend : 'Ext.data.Model',
	fields : [{
		name : 'id',
		type : 'string'
	}, {
		name : 'client_user_group_id',
		type : 'int'
	}, {
		name : 'privilege_code',
		type : 'string'
	}, {
		name : 'privilege_description',
		type : 'string'
	}, {
		name : 'is_denied',
		type : 'boolean',
		convert : function(value) {
			return '1' === value ? true : false;
		}
	}, {
		name : 'is_allowed',
		type : 'boolean',
		convert : function(value) {
			return '1' === value ? true : false;
		}
	}]
});

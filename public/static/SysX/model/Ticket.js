Ext.define('SysX.model.Ticket', {
	extend : 'Ext.data.Model',
	fields : [{
		name : 'id',
		type : 'int'
	}, {
		name : 'parent_id',
		type : 'int'
	}, {
		name : 'company_id',
		type : 'int'
	}, {
		name : 'company_user_id',
		type : 'int'
	}, {
		name : 'client_id',
		type : 'int'
	}, {
		name : 'client_user_id',
		type : 'int'
	}, {
		name : 'sysxtype',
		type : 'string'
	}, {
		name : 'record_id',
		type : 'int'
	}, {
		name : 'subject',
		type : 'string'
	}, {
		name : 'content',
		type : 'string'
	}, {
		name : 'create_time',
		type : 'string'
	}, {
		name : 'status',
		type : 'string'
	}, {
		name : 'user_name',
		type : 'string'
	}],
	status : {
		'OPENED' : ['OPENED', SysX.t.Status_OPEN, 'style="color:red"'],
		'CLOSED' : ['CLOSED', SysX.t.Status_CLOSED, 'style="color:green"']
	}
});

Ext.define('SysX.model.Config', {
	extend : 'Ext.data.Model',
	fields : [{
		name : 'code',
		type : 'string'
	}, {
		name : 'type',
		type : 'string'
	}, {
		name : 'default_value',
		type : 'auto'
	}, {
		name : 'is_for_company',
		type : 'boolean'
	}, {
		name : 'is_for_client',
		type : 'boolean'
	}],
	type : {
		'BOOL' : ['BOOL', 'BOOL'],
		'NUMBER' : ['NUMBER', 'NUMBER'],
		'TEXT' : ['TEXT', 'TEXT'],
		'LIST' : ['LIST', 'LIST'],
		'DATE' : ['DATE', 'DATE']
	}
});

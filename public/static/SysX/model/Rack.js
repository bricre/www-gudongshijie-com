Ext.define('SysX.model.Rack', {
	extend : 'Ext.data.Model',
	fields : [{
		name : 'id',
		type : 'int'
	}, {
		name : 'warehouse_id',
		type : 'int'
	}, {
		name : 'order',
		type : 'int'
	}, {
		name : 'name',
		type : 'string'
	}, {
		name : 'status',
		type : 'string'
	}, {
		name : 'warehouse_name',
		type : 'string',
		isAdditional : true
	}],
	status : {
		'INACTIVE' : ['INACTIVE', SysX.t.Status_INACTIVE, 'style="color:red"'],
		'ACTIVE' : ['ACTIVE', SysX.t.Status_ACTIVE, 'style="color:green"']
	}
});

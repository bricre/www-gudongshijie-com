Ext.define('SysX.model.ClientUser', {
	extend : 'Ext.data.Model',
	fields : [{
		name : 'id',
		type : 'int'
	}, {
		name : 'client_id',
		type : 'int'
	}, {
		name : 'client_user_group_id',
		type : 'int'
	}, {
		name : 'username',
		type : 'string'
	}, {
		name : 'password',
		type : 'string'
	}, {
		name : 'api_key',
		type : 'string'
	}, {
		name : 'email',
		type : 'string'
	}, {
		name : 'first_name',
		type : 'string'
	}, {
		name : 'last_name',
		type : 'string'
	}, {
		name : 'is_manager',
		type : 'boolean'
	}, {
		name : 'status',
		type : 'string'
	}, {
		name : 'client_user_group_name',
		type : 'string'
	}],
	status : {
		'INACTIVE' : ['INACTIVE', SysX.t.Status_INACTIVE, 'style="color:red"'],
		'ACTIVE' : ['ACTIVE', SysX.t.Status_ACTIVE, 'style="color:green"']
	}
});

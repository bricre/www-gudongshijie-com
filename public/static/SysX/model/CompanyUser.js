Ext.define('SysX.model.CompanyUser', {
	extend : 'Ext.data.Model',
	fields : [{
		name : 'id',
		type : 'int'
	}, {
		name : 'company_id',
		type : 'int'
	}, {
		name : 'company_user_group_id',
		type : 'int'
	}, {
		name : 'username',
		type : 'string'
	}, {
		name : 'password',
		type : 'string'
	}, {
		name : 'api_key',
		type : 'string'
	}, {
		name : 'email',
		type : 'string'
	}, {
		name : 'first_name',
		type : 'string'
	}, {
		name : 'last_name',
		type : 'string'
	}, {
		name : 'status',
		type : 'string'
	}, {
		name : 'company_user_group_name',
		type : 'string'
	}],
	status : {
		'INACTIVE' : ['INACTIVE', SysX.t.Status_INACTIVE, 'style="color:red"'],
		'ACTIVE' : ['ACTIVE', SysX.t.Status_ACTIVE, 'style="color:green"']
	}
});

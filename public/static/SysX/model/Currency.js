Ext.define('SysX.model.Currency', {
	extend : 'Ext.data.Model',
	fields : ['code', 'name', 'symbol', {
		name : 'exchange_rate',
		type : 'float'
	}, {
		name : 'is_primary',
		type : 'boolean'
	}]
});

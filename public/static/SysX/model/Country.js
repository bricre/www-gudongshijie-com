Ext.define('SysX.model.Country', {
	extend : 'Ext.data.Model',
	fields : [ {
		name : 'iso',
		type : 'string'
	}, {
		name : 'name',
		type : 'string'
	}, {
		name : 'printable_name',
		type : 'string'
	}, {
		name : 'iso3',
		type : 'string'
	}, {
		name : 'numcode',
		type : 'int'
	} ],
	idProperty : 'iso'
});
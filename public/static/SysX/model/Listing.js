Ext.define('SysX.model.Listing',
		{
			extend : 'Ext.data.Model',
			fields : [ 'id', 'vendor_id', 'title', 'url', 'description',
					'cost_currency_code', {
						name : 'cost',
						type : 'float'
					}, 'price_currency_code', {
						name : 'price',
						type : 'float'
					}, 'delivery_currency_code', {
						name : 'delivery_fee',
						type : 'float'
					}, 'recommended_price_currency_code', {
						name : 'recommended_price',
						type : 'float'
					}, 'note', 'status', 'cost_currency-symbol',
					'price_currency-symbol', 'delivery_fee_currency-symbol',
					'recommended_price_currency-symbol', 'vendor-name' ],
			status : {
				'NEW' : [ 'NEW', SysX.t.Status_NEW, 'style="color:green"' ],
				'ENQUIRING' : [ 'ENQUIRING', SysX.t.Status_ENQUIRING,
						'style="color:peru"' ],
				'INSTOCK' : [ 'INSTOCK', SysX.t.Status_INSTOCK,
						'style="color:orange"' ],
				'SOLD' : [ 'SOLD', SysX.t.Status_SOLD, 'style="color:sold"' ],
				'DELETED' : [ 'DELETED', SysX.t.Status_DELETED,
						'style="color:grey"' ]
			}
		});

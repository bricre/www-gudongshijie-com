Ext.define('SysX.model.Client', {
	extend : 'Ext.data.Model',
	fields : [{
		name : 'id',
		type : 'int'
	}, {
		name : 'client_group_id',
		type : 'int'
	}, {
		name : 'company_id',
		type : 'int'
	}, {
		name : 'is_internal',
		type : 'boolean'
	}, {
		name : 'is_external',
		type : 'boolean'
	}, {
		name : 'is_vip',
		type : 'boolean'
	}, {
		name : 'name',
		type : 'string'
	}, {
		name : 'email',
		type : 'string'
	}, {
		name : 'telephone',
		type : 'string'
	}, {
		name : 'address_line_1',
		type : 'string'
	}, {
		name : 'address_line_2',
		type : 'string'
	}, {
		name : 'address_line_3',
		type : 'string'
	}, {
		name : 'city',
		type : 'string'
	}, {
		name : 'county',
		type : 'string'
	}, {
		name : 'post_code',
		type : 'string'
	}, {
		name : 'country_iso',
		type : 'string'
	}, {
		name : 'credit_limit',
		type : 'float'
	}, {
		name : 'create_time',
		type : 'string'
	}, {
		name : 'next_charging_date',
		type : 'string'
	}, {
		name : 'note',
		type : 'string'
	}, {
		name : 'bulk_order_reference_field',
		type : 'string'
	}, {
		name : 'status',
		type : 'string'
	}, {
		name : 'balance',
		type : 'float'
	}, {
		name : 'client_group_name',
		type : 'string'
	}],
	status : {
		'ACTIVE' : ['ACTIVE', SysX.t.Status_ACTIVE, 'style="color:green"'],
		'INACTIVE' : ['INACTIVE', SysX.t.Status_INACTIVE, 'style="color:red"'],
		'DORMANT' : ['DORMANT', SysX.t.Status_DORMANT, 'style="color:peru"']
	},
	bulkOrderReferenceField : {
		'ID' : ['ID', SysX.t.SystemRecordID],
		'CLIENT_REF' : ['CLIENT_REF', SysX.t.ClientSKU],
		'COMPANY_REF' : ['COMPANY_REF', SysX.t.SystemSKU]
	}
});

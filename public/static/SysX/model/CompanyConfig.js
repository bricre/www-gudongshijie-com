Ext.define('SysX.model.CompanyConfig', {
	extend : 'Ext.data.Model',
	fields : [{
		name : 'company_id',
		type : 'int'
	}, {
		name : 'config_code',
		type : 'string'
	}, {
		name : 'value',
		type : 'auto'
	}],
	idProperty : {
		name : 'id',
		convert : function(value, record) {
			return record.get('company_id') + '-' + record.get('config_code');
		}
	}
});

Ext.define('SysX.model.User', {
	extend : 'Ext.data.Model',
	proxy : {
		type : 'ajax',
		api : {
			read : '/user'
		},
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success'
		}
	},
	fields : [{
		name : 'id',
		type : 'int'
	}, {
		name : 'name',
		type : 'string'
	}, {
		name : 'userhash',
		type : 'string'
	}],
	statics : {
		cookieName : 'user'
	},
	reset : function() {
		this.set({
			id : null,
			username : null,
			userhash : null
		});
		Ext.util.Cookies.clear(this.statics.cookieName);
	}
}); 
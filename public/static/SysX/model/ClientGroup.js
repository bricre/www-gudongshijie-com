Ext.define('SysX.model.ClientGroup', {
	extend : 'Ext.data.Model',
	fields : [{
		name : 'id',
		type : 'int'
	}, {
		name : 'name',
		type : 'string'
	}, {
		name : 'handling_fee_limit',
		type : 'float'
	}, {
		name : 'handling_fee_first_item',
		type : 'float'
	}, {
		name : 'handling_fee_other_item',
		type : 'float'
	}, {
		name : 'handling_fee_first_product',
		type : 'float'
	}, {
		name : 'handling_fee_other_product',
		type : 'float'
	}, {
		name : 'delivery_fee',
		type : 'float'
	}, {
		name : 'delivery_fee_type',
		type : 'int'
	}, {
		name : 'monthly_membership_fee',
		type : 'float'
	}, {
		name : 'credit_limit_top_up_period',
		type : 'int'
	}, {
		name : 'credit_limit_top_up_amount',
		type : 'int'
	}, {
		name : 'credit_limit_top_up_times',
		type : 'int'
	}, {
		name : 'credit_limit_top_up_period_per_time',
		type : 'int'
	}, {
		name : 'status',
		type : 'string'
	}],
	status : {
		'ACTIVE' : ['ACTIVE', SysX.t.Status_ACTIVE, 'style="color:green"'],
		'INACTIVE' : ['INACTIVE', SysX.t.Status_INACTIVE, 'style="color:red"'],
		'DELETED' : ['DELETED', SysX.t.Status_DELETED, 'style="color:grey"']
	}
});

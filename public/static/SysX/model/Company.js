Ext.define('SysX.model.Company', {
	extend : 'Ext.data.Model',
	fields : [{
		name : 'id',
		type : 'int'
	}, {
		name : 'name',
		type : 'string'
	}, {
		name : 'email',
		type : 'string'
	}, {
		name : 'telephone',
		type : 'string'
	}, {
		name : 'address_line_1',
		type : 'string'
	}, {
		name : 'address_line_2',
		type : 'string'
	}, {
		name : 'address_line_3',
		type : 'string'
	}, {
		name : 'city',
		type : 'string'
	}, {
		name : 'county',
		type : 'string'
	}, {
		name : 'post_code',
		type : 'string'
	}, {
		name : 'country_iso',
		type : 'string'
	}, {
		name : 'return_name',
		type : 'string'
	}, {
		name : 'return_address_line_1',
		type : 'string'
	}, {
		name : 'return_address_line_2',
		type : 'string'
	}, {
		name : 'return_address_line_3',
		type : 'string'
	}, {
		name : 'return_city',
		type : 'string'
	}, {
		name : 'return_county',
		type : 'string'
	}, {
		name : 'return_post_code',
		type : 'string'
	}, {
		name : 'return_country_iso',
		type : 'string'
	}]
});

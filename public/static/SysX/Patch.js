var app, viewport, user, SysX = SysX || {};

Ext.USE_NATIVE_JSON = true;
Ext.Date.defaultFormat = 'Y-m-d H:i:s';
Ext.override(Ext, {
	isIterable : function(value) {
		var type = typeof value, checkLength = false;
		if (Ext.isObject(value)) {
			return false;
		}
		if (value && type !== 'string') {
			// Functions have a length property, so we need to filter them out
			if (type === 'function') {
				// In Safari, NodeList/HTMLCollection both return "function" when using typeof, so we need
				// to explicitly check them here.
				if (Ext.isSafari) {
					checkLength = value instanceof NodeList || value instanceof HTMLCollection;
				}
			} else {
				checkLength = true;
			}
		}
		return checkLength ? length !== undefined : false;
	}
});

Ext.override(Ext.form.action.Load, {
	onSuccess : function(response) {
		var result = this.processResponse(response), form = this.form, data = Ext.isArray(result.data) ? result.data[0] : result.data;
		if (result === true || !result.success || !result.data) {
			this.failureType = Ext.form.action.Action.LOAD_FAILURE;
			form.afterAction(this, false);
			return;
		}
		form.clearInvalid();
		form.setValues(data);
		form.afterAction(this, true);
	}
});

Ext.override(Ext.Date, {
	defaultFormat : 'Y-m-d H:i:s'
});

Ext.override(Ext.data.Field, {
	dateFormat : 'Y-m-d H:i:s'
});

Ext.override(Ext.form.field.Checkbox, {
	inputValue : 1,
	value : 1,
	uncheckedValue : 0
});

Ext.override(Ext.grid.column.Date, {
	format : 'Y-m-d H:i:s'
});

Ext.apply(SysX, {
	openNewWindow : function(url) {
		window.open(url, null, 'toolbar=no,location=no,directories=no,menubar=no,scrollbars=yes,status=1, height=' + Ext.getBody().getHeight() * 0.5 + ',width=' + Ext.getBody().getWidth() * 0.5);
	}
});

Ext.Ajax.on('requestcomplete', function(conn, response, options) {
	if (!response.responseXML) {
		var jsonResponse = Ext.decode(response.responseText, true);
		if (null === jsonResponse) {
			Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, response.responseText);
		} else if (jsonResponse.success === false) {
			Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, jsonResponse.message);
		}
	}
});

Ext.Ajax.on('requestexception', function(conn, response, options) {
	if (403 === response.status) {
		Ext.Msg.alert(SysX.t.MessageBoxTitle_Error, SysX.t.User_SessionExpired);
		document.location.reload(false);
	}
});

Ext.require(['Ext.ux.grid.Printer']);

// http://www.sencha.com/forum/showthread.php?198856-Ext.ux.TreeCombo
Ext.define('Ext.ux.TreeCombo', {
	extend : 'Ext.form.field.Picker',
	alias : 'widget.treecombo',
	tree : false,
	constructor : function(config) {
		this.addEvents({
			"itemclick" : true
		});

		this.listeners = config.listeners;
		this.callParent(arguments);
	},
	records : [],
	recursiveRecords : [],
	ids : [],
	selectChildren : true,
	canSelectFolders : true,
	multiselect : false,
	displayField : 'text',
	valueField : 'id',
	treeWidth : 300,
	matchFieldWidth : false,
	treeHeight : 400,
	masN : 0,
	recursivePush : function(node, setIds) {
		var me = this;

		me.addRecRecord(node);
		if (setIds)
			me.addIds(node);

		node.eachChild(function(nodesingle) {
			if (nodesingle.hasChildNodes() == true) {
				me.recursivePush(nodesingle, setIds);
			} else {
				me.addRecRecord(nodesingle);
				if (setIds)
					me.addIds(nodesingle);
			}
		});
	},
	recursiveUnPush : function(node) {
		var me = this;
		me.removeIds(node);

		node.eachChild(function(nodesingle) {
			if (nodesingle.hasChildNodes() == true) {
				me.recursiveUnPush(nodesingle);
			} else
				me.removeIds(nodesingle);
		});
	},
	addRecRecord : function(record) {
		var me = this;

		for (var i = 0, j = me.recursiveRecords.length; i < j; i++) {
			var item = me.recursiveRecords[i];
			if (item) {
				if (item.getId() == record.getId())
					return;
			}
		}
		me.recursiveRecords.push(record);
	},
	afterLoadSetValue : false,
	setValue : function(valueInit) {
		if ( typeof valueInit == 'undefined')
			return;

		var me = this, tree = this.tree, values = (valueInit == '') ? [] : valueInit.split(','), valueFin = [];

		inputEl = me.inputEl;

		if (tree.store.isLoading()) {
			me.afterLoadSetValue = valueInit;
		}

		if (inputEl && me.emptyText && !Ext.isEmpty(values)) {
			inputEl.removeCls(me.emptyCls);
		}

		if (tree == false)
			return false;

		var node = tree.getRootNode();
		if (node == null)
			return false;

		me.recursiveRecords = [];
		me.recursivePush(node, false);

		me.records = [];
		Ext.each(me.recursiveRecords, function(record) {
			var id = record.get(me.valueField), index = values.indexOf('' + id);

			if (me.multiselect == true)
				record.set('checked', false);

			if (index != -1) {
				valueFin.push(record.get(me.displayField));
				if (me.multiselect == true)
					record.set('checked', true);
				me.addRecord(record);
			}
		});

		me.value = valueInit;
		me.setRawValue(valueFin.join(', '));

		me.checkChange();
		me.applyEmptyText();
		return me;
	},
	getValue : function() {
		return this.value;
	},
	getSubmitValue : function() {
		return this.value;
	},
	checkParentNodes : function(node) {
		if (node == null)
			return;

		var me = this, checkedAll = true;

		node.eachChild(function(nodesingle) {
			var id = nodesingle.getId(), index = me.ids.indexOf('' + id);

			if (index == -1)
				checkedAll = false;
		});

		if (checkedAll == true) {
			me.addIds(node);
			me.checkParentNodes(node.parentNode);
		} else {
			me.removeIds(node);
			me.checkParentNodes(node.parentNode);
		}
	},
	initComponent : function() {
		var me = this;

		me.tree = Ext.create('Ext.tree.Panel', {
			alias : 'widget.assetstree',
			hidden : true,
			minHeight : 300,
			rootVisible : ( typeof me.rootVisible != 'undefined') ? me.rootVisible : true,
			floating : true,
			useArrows : true,
			width : me.treeWidth,
			autoScroll : true,
			height : me.treeHeight,
			store : me.store,
			listeners : {
				load : function(store, records) {
					if (me.afterLoadSetValue != false) {
						me.setValue(me.afterLoadSetValue);
					}
				},
				itemclick : function(view, record, item, index, e, eOpts) {
					me.itemTreeClick(view, record, item, index, e, eOpts, me)
				}
			}
		});

		if (me.tree.getRootNode().get('checked') != null)
			me.multiselect = true;

		this.createPicker = function() {
			var me = this;
			return me.tree;
		};

		this.callParent(arguments);
	},
	addIds : function(record) {
		var me = this;

		if (me.ids.indexOf('' + record.getId()) == -1)
			me.ids.push('' + record.get(me.valueField));
	},
	removeIds : function(record) {
		var me = this, index = me.ids.indexOf('' + record.getId());

		if (index != -1) {
			me.ids.splice(index, 1);
		}
	},
	addRecord : function(record) {
		var me = this;

		for (var i = 0, j = me.records.length; i < j; i++) {
			var item = me.records[i];
			if (item) {
				if (item.getId() == record.getId())
					return;
			}
		}
		me.records.push(record);
	},
	removeRecord : function(record) {
		var me = this;

		for (var i = 0, j = me.records.length; i < j; i++) {
			var item = me.records[i];
			if (item && item.getId() == record.getId())
				delete (me.records[i]);
		}
	},
	itemTreeClick : function(view, record, item, index, e, eOpts, treeCombo) {
		var me = treeCombo, checked = !record.get('checked');
		//it is still not checked if will be checked in this event

		if (me.multiselect == true)
			record.set('checked', checked);
		//check record

		var node = me.tree.getRootNode().findChild(me.valueField, record.get(me.valueField), true);
		if (node == null) {
			if (me.tree.getRootNode().get(me.valueField) == record.get(me.valueField))
				node = me.tree.getRootNode();
			else
				return false;
		}

		if (me.multiselect == false)
			me.ids = [];

		//if it can't select folders and it is a folder check existing values and return false
		if (me.canSelectFolders == false && record.get('leaf') == false) {
			me.setRecordsValue(view, record, item, index, e, eOpts, treeCombo);
			return false;
		}

		//if record is leaf
		if (record.get('leaf') == true) {
			if (checked == true) {
				me.addIds(record);
			} else {
				me.removeIds(record);
			}
		} else//it's a directory
		{
			me.recursiveRecords = [];
			if (checked == true) {
				if (me.multiselect == false) {
					if (me.canSelectFolders == true)
						me.addIds(record);
				} else {
					if (me.canSelectFolders == true) {
						me.recursivePush(node, true);
					}
				}
			} else {
				if (me.multiselect == false) {
					if (me.canSelectFolders == true)
						me.recursiveUnPush(node);
					else
						me.removeIds(record);
				} else
					me.recursiveUnPush(node);
			}
		}

		//this will check every parent node that has his all children selected
		if (me.canSelectFolders == true && me.multiselect == true)
			me.checkParentNodes(node.parentNode);

		me.setRecordsValue(view, record, item, index, e, eOpts, treeCombo);
	},
	fixIds : function() {
		var me = this;

		for (var i = 0, j = me.ids.length; i < j; i++) {
			if (me.ids[i] == 'NaN')
				me.ids.splice(i, 1);
		}
	},
	setRecordsValue : function(view, record, item, index, e, eOpts, treeCombo) {
		var me = treeCombo;

		me.fixIds();

		me.setValue(me.ids.join(','));

		me.fireEvent('itemclick', me, record, item, index, e, eOpts, me.records, me.ids);

		if (me.multiselect == false)
			me.onTriggerClick();
	}
});

/**
 * @class Ext.ux.form.field.ClearButton
 *
 * http://www.sencha.com/forum/showthread.php?132775-Ext.ux.form.field.ClearButton-Small-clear-button-icon-over-field&p=963418&viewfull=1#post963418
 *
 * Plugin for text components that shows a "clear" button over the text field.
 * When the button is clicked the text field is set empty.
 * Icon image and positioning can be controlled using CSS.
 * Works with Ext.form.field.Text, Ext.form.field.TextArea, Ext.form.field.ComboBox and Ext.form.field.Date.
 *
 * Plugin alias is 'clearbutton' (use "plugins: 'clearbutton'" in GridPanel config).
 *
 * @author <a href="mailto:stephen.friedrich@fortis-it.de">Stephen Friedrich</a>
 * @author <a href="mailto:fabian.urban@fortis-it.de">Fabian Urban</a>
 *
 * @copyright (c) 2011 Fortis IT Services GmbH
 * @license Ext.ux.form.field.ClearButton is released under the
 * <a target="_blank" href="http://www.apache.org/licenses/LICENSE-2.0">Apache License, Version 2.0</a>.
 *
 */
Ext.define('Ext.ux.form.field.ClearButton', {
	alias : 'plugin.clearbutton',

	/**
	 * @cfg {Boolean} Hide the clear button when the field is empty (default: true).
	 */
	hideClearButtonWhenEmpty : true,

	/**
	 * @cfg {Boolean} Hide the clear button until the mouse is over the field (default: true).
	 */
	hideClearButtonWhenMouseOut : true,

	/**
	 * @cfg {Boolean} When the clear buttons is hidden/shown, this will animate the button to its new state (using opacity) (default: true).
	 */
	animateClearButton : true,

	/**
	 * @cfg {Boolean} Empty the text field when ESC is pressed while the text field is focused.
	 */
	clearOnEscape : false,

	/**
	 * @cfg {String} CSS class used for the button div.
	 * Also used as a prefix for other classes (suffixes: '-mouse-over-input', '-mouse-over-button', '-mouse-down', '-on', '-off')
	 */
	clearButtonCls : 'ext-ux-clearbutton',

	/**
	 * The text field (or text area, combo box, date field) that we are attached to
	 */
	textField : null,

	/**
	 * Will be set to true if animateClearButton is true and the browser supports CSS 3 transitions
	 * @private
	 */
	animateWithCss3 : false,

	/////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Set up and tear down
	//
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	constructor : function(cfg) {
		Ext.apply(this, cfg);

		this.callParent(arguments);
	},

	/**
	 * Called by plug-in system to initialize the plugin for a specific text field (or text area, combo box, date field).
	 * Most all the setup is delayed until the component is rendered.
	 */
	init : function(textField) {
		this.textField = textField;
		this.textField.addEvents('clear');
		if (!textField.rendered) {
			textField.on('afterrender', this.handleAfterRender, this);
		} else {
			// probably an existing input element transformed to extjs field
			this.handleAfterRender();
		}
	},

	/**
	 * After the field has been rendered sets up the plugin (create the Element for the clear button, attach listeners).
	 * @private
	 */
	handleAfterRender : function(textField) {
		this.isTextArea = (this.textField.inputEl.dom.type.toLowerCase() == 'textarea');

		this.createClearButtonEl();
		this.addListeners();

		this.repositionClearButton();
		this.updateClearButtonVisibility();

		this.addEscListener();
	},

	/**
	 * Creates the Element and DOM for the clear button
	 */
	createClearButtonEl : function() {
		var animateWithClass = this.animateClearButton && this.animateWithCss3;
		this.clearButtonEl = this.textField.bodyEl.createChild({
			tag : 'div',
			cls : this.clearButtonCls
		});
		if (this.animateClearButton) {
			this.animateWithCss3 = this.supportsCssTransition(this.clearButtonEl);
		}
		if (this.animateWithCss3) {
			this.clearButtonEl.addCls(this.clearButtonCls + '-off');
		} else {
			this.clearButtonEl.setStyle('visibility', 'hidden');
		}
	},

	/**
	 * Returns true iff the browser supports CSS 3 transitions
	 * @param el an element that is checked for support of the "transition" CSS property (considering any
	 *           vendor prefixes)
	 */
	supportsCssTransition : function(el) {
		var styles = ['transitionProperty', 'WebkitTransitionProperty', 'MozTransitionProperty', 'OTransitionProperty', 'msTransitionProperty', 'KhtmlTransitionProperty'];

		var style = el.dom.style;
		for (var i = 0, length = styles.length; i < length; ++i) {
			if (style[styles[i]] !== 'undefined') {
				// Supported property will result in empty string
				return true;
			}
		}
		return false;
	},

	/**
	 * If config option "clearOnEscape" is true, then add a key listener that will clear this field
	 */
	addEscListener : function() {
		if (this.clearOnEscape) {
			// Using a KeyMap did not work: ESC is swallowed by combo box and date field before it reaches our own KeyMap
			this.textField.inputEl.on('keydown', function(e) {
				if (e.getKey() == Ext.EventObject.ESC) {
					if (this.textField.isExpanded) {
						// Let combo box or date field first remove the popup
						return;
					}
					// No idea why the defer is necessary, but otherwise the call to setValue('') is ignored
					Ext.Function.defer(this.clearValue, 1, this);
					e.stopEvent();
				}
			}, this);
		}
	},

	/**
	 * Adds listeners to the field, its input element and the clear button to handle resizing, mouse over/out events, click events etc.
	 */
	addListeners : function() {
		// listeners on input element (DOM/El level)
		var textField = this.textField;
		var bodyEl = textField.bodyEl;
		bodyEl.on('mouseover', this.handleMouseOverInputField, this);
		bodyEl.on('mouseout', this.handleMouseOutOfInputField, this);

		// listeners on text field (component level)
		textField.on('destroy', this.handleDestroy, this);
		textField.on('resize', this.repositionClearButton, this);
		textField.on('change', function() {
			this.repositionClearButton();
			this.updateClearButtonVisibility();
		}, this);

		// listeners on clear button (DOM/El level)
		var clearButtonEl = this.clearButtonEl;
		clearButtonEl.on('mouseover', this.handleMouseOverClearButton, this);
		clearButtonEl.on('mouseout', this.handleMouseOutOfClearButton, this);
		clearButtonEl.on('mousedown', this.handleMouseDownOnClearButton, this);
		clearButtonEl.on('mouseup', this.handleMouseUpOnClearButton, this);
		clearButtonEl.on('click', this.handleMouseClickOnClearButton, this);
	},

	/**
	 * When the field is destroyed, we also need to destroy the clear button Element to prevent memory leaks.
	 */
	handleDestroy : function() {
		this.clearButtonEl.destroy();
	},

	/////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Mouse event handlers
	//
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Tada - the real action: If user left clicked on the clear button, then empty the field
	 */
	handleMouseClickOnClearButton : function(event, htmlElement, object) {
		if (!this.isLeftButton(event)) {
			return;
		}
		this.clearValue();
		this.textField.focus();
	},

	handleMouseOverInputField : function(event, htmlElement, object) {
		this.clearButtonEl.addCls(this.clearButtonCls + '-mouse-over-input');
		if (event.getRelatedTarget() == this.clearButtonEl.dom) {
			// Moused moved to clear button and will generate another mouse event there.
			// Handle it here to avoid duplicate updates (else animation will break)
			this.clearButtonEl.removeCls(this.clearButtonCls + '-mouse-over-button');
			this.clearButtonEl.removeCls(this.clearButtonCls + '-mouse-down');
		}
		this.updateClearButtonVisibility();
	},

	handleMouseOutOfInputField : function(event, htmlElement, object) {
		this.clearButtonEl.removeCls(this.clearButtonCls + '-mouse-over-input');
		if (event.getRelatedTarget() == this.clearButtonEl.dom) {
			// Moused moved from clear button and will generate another mouse event there.
			// Handle it here to avoid duplicate updates (else animation will break)
			this.clearButtonEl.addCls(this.clearButtonCls + '-mouse-over-button');
		}
		this.updateClearButtonVisibility();
	},

	handleMouseOverClearButton : function(event, htmlElement, object) {
		event.stopEvent();
		if (this.textField.bodyEl.contains(event.getRelatedTarget())) {
			// has been handled in handleMouseOutOfInputField() to prevent double update
			return;
		}
		this.clearButtonEl.addCls(this.clearButtonCls + '-mouse-over-button');
		this.updateClearButtonVisibility();
	},

	handleMouseOutOfClearButton : function(event, htmlElement, object) {
		event.stopEvent();
		if (this.textField.bodyEl.contains(event.getRelatedTarget())) {
			// will be handled in handleMouseOverInputField() to prevent double update
			return;
		}
		this.clearButtonEl.removeCls(this.clearButtonCls + '-mouse-over-button');
		this.clearButtonEl.removeCls(this.clearButtonCls + '-mouse-down');
		this.updateClearButtonVisibility();
	},

	handleMouseDownOnClearButton : function(event, htmlElement, object) {
		if (!this.isLeftButton(event)) {
			return;
		}
		this.clearButtonEl.addCls(this.clearButtonCls + '-mouse-down');
	},

	handleMouseUpOnClearButton : function(event, htmlElement, object) {
		if (!this.isLeftButton(event)) {
			return;
		}
		this.clearButtonEl.removeCls(this.clearButtonCls + '-mouse-down');
	},

	/////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Utility methods
	//
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Repositions the clear button element based on the textfield.inputEl element
	 * @private
	 */
	repositionClearButton : function() {
		var clearButtonEl = this.clearButtonEl;
		if (!clearButtonEl) {
			return;
		}
		var right = 0;
		if (this.fieldHasScrollBar()) {
			right += Ext.getScrollBarWidth();
		}
		// See http://www.sencha.com/forum/showthread.php?132775-Ext.ux.form.field.ClearButton-Small-clear-button-icon-over-field&p=961461&viewfull=1#post961461
		//if (this.textField.triggerWrap) {
		//    right += this.textField.getTriggerWidth();
		//}
		//clearButtonEl.alignTo(this.textField.bodyEl, 'tr-tr', [-1 * (right + 3), 5]);
		clearButtonEl.alignTo(this.textField.inputEl, 'tr-tr', [-1 * (right + 3), 5]);
	},

	//        /**
	//         * Calculates the position of the clear button based on the textfield.inputEl element
	//         * @private
	//         */
	//        calculateClearButtonPosition: function(textField) {
	//            var positions = textField.inputEl.getBox(true, true);
	//            var top = positions.y;
	//            var right = positions.x;
	//            if (this.fieldHasScrollBar()) {
	//                right += Ext.getScrollBarWidth();
	//            }
	//            if (this.textField.triggerWrap) {
	//                right += this.textField.getTriggerWidth();
	//            }
	//            return {
	//                right: right,
	//                top: top
	//            };
	//        },

	/**
	 * Checks if the field we are attached to currently has a scrollbar
	 */
	fieldHasScrollBar : function() {
		if (!this.isTextArea) {
			return false;
		}

		var inputEl = this.textField.inputEl;
		var overflowY = inputEl.getStyle('overflow-y');
		if (overflowY == 'hidden' || overflowY == 'visible') {
			return false;
		}
		if (overflowY == 'scroll') {
			return true;
		}
		//noinspection RedundantIfStatementJS
		if (inputEl.dom.scrollHeight <= inputEl.dom.clientHeight) {
			return false;
		}
		return true;
	},

	/**
	 * Small wrapper around clearButtonEl.isVisible() to handle setVisible animation that may still be in progress.
	 */
	isButtonCurrentlyVisible : function() {
		if (this.animateClearButton && this.animateWithCss3) {
			return this.clearButtonEl.hasCls(this.clearButtonCls + '-on');
		}

		// This should not be necessary (see Element.setVisible/isVisible), but else there is confusion about visibility
		// when moving the mouse out and _quickly_ over then input again.
		var cachedVisible = Ext.core.Element.data(this.clearButtonEl.dom, 'isVisible');
		if ( typeof (cachedVisible) == 'boolean') {
			return cachedVisible;
		}
		return this.clearButtonEl.isVisible();
	},

	/**
	 * Checks config options and current mouse status to determine if the clear button should be visible.
	 */
	shouldButtonBeVisible : function() {
		if (this.hideClearButtonWhenEmpty && Ext.isEmpty(this.textField.getValue())) {
			return false;
		}

		var clearButtonEl = this.clearButtonEl;
		//noinspection RedundantIfStatementJS
		if (this.hideClearButtonWhenMouseOut && !clearButtonEl.hasCls(this.clearButtonCls + '-mouse-over-button') && !clearButtonEl.hasCls(this.clearButtonCls + '-mouse-over-input')) {
			return false;
		}

		return true;
	},

	/**
	 * Called after any event that may influence the clear button visibility.
	 */
	updateClearButtonVisibility : function() {
		var oldVisible = this.isButtonCurrentlyVisible();
		var newVisible = this.shouldButtonBeVisible();

		var clearButtonEl = this.clearButtonEl;
		if (oldVisible != newVisible) {
			if (this.animateClearButton && this.animateWithCss3) {
				this.clearButtonEl.removeCls(this.clearButtonCls + ( oldVisible ? '-on' : '-off'));
				clearButtonEl.addCls(this.clearButtonCls + ( newVisible ? '-on' : '-off'));
			} else {
				clearButtonEl.stopAnimation();
				clearButtonEl.setVisible(newVisible, this.animateClearButton);
			}

			// Set background-color of clearButton to same as field's background-color (for those browsers/cases
			// where the padding-right (see below) does not work)
			clearButtonEl.setStyle('background-color', this.textField.inputEl.getStyle('background-color'));

			// Adjust padding-right of the input tag to make room for the button
			// IE (up to v9) just ignores this and Gecko handles padding incorrectly with  textarea scrollbars
			//                if (!(this.isTextArea && Ext.isGecko) && !Ext.isIE) {
			//                    // See https://bugzilla.mozilla.org/show_bug.cgi?id=157846
			//                    var deltaPaddingRight = clearButtonEl.getWidth() - this.clearButtonEl.getMargin('l');
			//                    var currentPaddingRight = this.textField.inputEl.getPadding('r');
			//                    var factor = (newVisible ? +1 : -1);
			//                    this.textField.inputEl.dom.style.paddingRight = (currentPaddingRight + factor * deltaPaddingRight) + 'px';
			//                }
		}
	},

	isLeftButton : function(event) {
		return event.button === 0;
	},

	clearValue : function() {
		if (Ext.isFunction(this.textField.clearValue)) {
			this.textField.clearValue();
		} else {
			this.textField.setValue('');
		}
		this.textField.fireEvent('clear', this.textField);
	}
});

Ext.override(Ext.form.field.Text, {
	plugins : ['clearbutton']
});

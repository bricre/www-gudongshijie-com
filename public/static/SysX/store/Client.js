Ext.define('SysX.store.Client', {
	extend : 'SysX.store.abstract.Store',
	model : 'SysX.model.Client',
	proxy : {
		url : modulePath + '/Client',
		type : 'rest',
		reader : {
			type : 'json',
			root : 'data'
		}
	}
});

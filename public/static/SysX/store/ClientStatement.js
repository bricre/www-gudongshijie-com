Ext.define('SysX.store.ClientStatement', {
	extend : 'SysX.store.abstract.Store',
	model : 'SysX.model.ClientStatement',
	proxy : {
		url : modulePath + '/Client-Statement',
		type : 'rest',
		reader : {
			type : 'json',
			root : 'data'
		}
	}
});

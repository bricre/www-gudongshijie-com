Ext.define('SysX.store.CompanyConfig', {
	extend : 'SysX.store.abstract.Store',
	model : 'SysX.model.CompanyConfig',
	proxy : {
		url : modulePath + '/Company-Config',
		type : 'rest',
		reader : {
			type : 'json',
			root : 'data'
		}
	}
});

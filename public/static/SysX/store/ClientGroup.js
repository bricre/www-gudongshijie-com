Ext.define('SysX.store.ClientGroup', {
	extend : 'SysX.store.abstract.Store',
	model : 'SysX.model.ClientGroup',
	proxy : {
		url : modulePath + '/Client-Group',
		type : 'rest',
		reader : {
			type : 'json',
			root : 'data'
		}
	}
});

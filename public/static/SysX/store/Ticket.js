Ext.define('SysX.store.Ticket', {
	extend : 'SysX.store.abstract.Store',
	model : 'SysX.model.Ticket',
	proxy : {
		url : modulePath + '/Ticket',
		type : 'rest',
		reader : {
			type : 'json',
			root : 'data'
		}
	}
}); 
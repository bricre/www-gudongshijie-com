Ext.define('SysX.store.abstract.Store', {
	extend : 'Ext.data.Store',
	requires : ['Ext.data.proxy.Rest'],
	autoLoad : false,
	pageSize : 50,
	remoteFilter : true,
	remoteSort : true,

	config : {
		autoLoad : false,
		pageSize : 50,
		remoteFilter : true,
		remoteSort : true
	}
});

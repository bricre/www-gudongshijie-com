Ext.define('SysX.store.Vendor', {
	extend: 'SysX.store.abstract.Store',
	model: 'SysX.model.Vendor',
	proxy: {
		url: modulePath + '/Vendor',
		type: 'rest',
		reader: {
			type: 'json',
			root: 'data'
		}
	}
});
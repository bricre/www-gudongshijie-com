Ext.define('SysX.store.CompanyUserGroupPrivilege', {
	extend : 'SysX.store.abstract.Store',
	model : 'SysX.model.CompanyUserGroupPrivilege',
	autoSync : true,
	proxy : {
		url : modulePath + '/Company-User-Group-Privilege',
		type : 'rest',
		reader : {
			type : 'json',
			root : 'data'
		},
		appendId : false
	}
});

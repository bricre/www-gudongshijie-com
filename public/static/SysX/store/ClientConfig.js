Ext.define('SysX.store.ClientConfig', {
	extend : 'SysX.store.abstract.Store',
	model : 'SysX.model.ClientConfig',
	proxy : {
		url : modulePath + '/Client-Config',
		type : 'rest',
		reader : {
			type : 'json',
			root : 'data'
		}
	}
});

Ext.define('SysX.store.Listing', {
	extend: 'SysX.store.abstract.Store',
	model: 'SysX.model.Listing',
	proxy: {
		url: modulePath + '/Listing',
		type: 'rest',
		reader: {
			type: 'json',
			root: 'data'
		}
	}
});
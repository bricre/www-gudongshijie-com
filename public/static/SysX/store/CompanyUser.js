Ext.define('SysX.store.CompanyUser', {
	extend : 'SysX.store.abstract.Store',
	model : 'SysX.model.CompanyUser',
	proxy : {
		url : modulePath + '/Company-User',
		type : 'rest',
		reader : {
			type : 'json',
			root : 'data'
		}
	}
});

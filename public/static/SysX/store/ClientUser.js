Ext.define('SysX.store.ClientUser', {
	extend : 'SysX.store.abstract.Store',
	model : 'SysX.model.ClientUser',
	proxy : {
		url : modulePath + '/Client-User',
		type : 'rest',
		reader : {
			type : 'json',
			root : 'data'
		}
	}
});

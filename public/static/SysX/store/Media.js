Ext.define('SysX.store.Media', {
	extend : 'SysX.store.abstract.Store',
	model : 'SysX.model.Media',
	proxy : {
		url : modulePath + '/Media',
		type : 'rest',
		reader : {
			type : 'json',
			root : 'data'
		}
	}
});

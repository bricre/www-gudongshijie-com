Ext.define('SysX.store.Currency', {
	extend: 'SysX.store.abstract.Store',
	model: 'SysX.model.Currency',
	proxy: {
		url: modulePath + '/Currency',
		type: 'rest',
		reader: {
			type: 'json',
			root: 'data'
		}
	}
});
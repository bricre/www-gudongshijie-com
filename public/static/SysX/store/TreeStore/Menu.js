Ext.define('SysX.store.TreeStore.Menu', {
	extend : 'Ext.data.TreeStore',
	fields : [{
		name : 'controller',
		type : 'string'
	}, {
		name : 'text',
		type : 'string'
	}, {
		name : 'handler'
	}, {
		name : 'config'
	}, {
		name : 'module'
	}, {
		name : 'itemId',
		type : 'string'
	}, {
		name : 'method',
		type : 'string'
	}]
});

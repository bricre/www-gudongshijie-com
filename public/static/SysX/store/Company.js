Ext.define('SysX.store.Company', {
	extend : 'SysX.store.abstract.Store',
	model : 'SysX.model.Company',
	proxy : {
		url : modulePath + '/Company',
		type : 'rest',
		reader : {
			type : 'json',
			root : 'data'
		}
	}
});

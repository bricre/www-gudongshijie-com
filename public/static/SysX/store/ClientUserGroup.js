Ext.define('SysX.store.ClientUserGroup', {
	extend : 'SysX.store.abstract.Store',
	model : 'SysX.model.ClientUserGroup',
	proxy : {
		url : modulePath + '/Client-User-Group',
		type : 'rest',
		reader : {
			type : 'json',
			root : 'data'
		}
	}
});

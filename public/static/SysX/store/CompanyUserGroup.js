Ext.define('SysX.store.CompanyUserGroup', {
	extend : 'SysX.store.abstract.Store',
	model : 'SysX.model.CompanyUserGroup',
	proxy : {
		url : modulePath + '/Company-User-Group',
		type : 'rest',
		reader : {
			type : 'json',
			root : 'data'
		}
	}
});

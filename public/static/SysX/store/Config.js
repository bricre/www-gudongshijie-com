Ext.define('SysX.store.Config', {
	extend : 'SysX.store.abstract.Store',
	model : 'SysX.model.Config',
	proxy : {
		url : modulePath + '/Config',
		type : 'rest',
		reader : {
			type : 'json',
			root : 'data'
		}
	}
});

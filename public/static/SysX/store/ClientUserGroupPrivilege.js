Ext.define('SysX.store.ClientUserGroupPrivilege', {
	extend : 'SysX.store.abstract.Store',
	model : 'SysX.model.ClientUserGroupPrivilege',
	autoSync : true,
	proxy : {
		url : modulePath + '/Client-User-Group-Privilege',
		type : 'rest',
		reader : {
			type : 'json',
			root : 'data'
		},
		appendId : false
	}
});

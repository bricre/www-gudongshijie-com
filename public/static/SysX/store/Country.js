Ext.define('SysX.store.Country', {
	extend : 'SysX.store.abstract.Store',
	model : 'SysX.model.Country',
	proxy : {
		url : modulePath + '/Country',
		type : 'ajax',
		reader : {
			type : 'json',
			root : 'data'
		}
	},
	pageSize : -1
});

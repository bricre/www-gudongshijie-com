<?php
$versionControl = new SysX\View\Helper\VersionControl();
return array(
    'asset_bundle' => array(
        'production' => false, // Application environment (Developpement => false)
        'lastModifiedTime' => $versionControl(), // Arbitrary last modified time in production
        'cachePath' => '@zfRootPath/public/cache', // Cache directory absolute path
        'assetsPath' => '@zfRootPath/public', // Assets directory absolute path (allows you to define relative path for assets config)
        'baseUrl' => null, // Base URL of the application
        'cacheUrl' => '@zfBaseUrl/cache/', // Cache directory base url
        'mediaExt' => array(
            'jpeg',
            'jpg',
            'png',
            'gif',
            'cur',
            'ttf',
            'eot',
            'svg',
            'woff'
        ), // Put here all media extensions to be cached
        'recursiveSearch' => true, // Allows search for matching assets in required folder and its subfolders
        'filters' => array(
//            \AssetsBundle\Service\Service::ASSET_LESS => 'LessFilter',
//            \AssetsBundle\Service\Service::ASSET_CSS => 'CssFilter',
//            \AssetsBundle\Service\Service::ASSET_JS => 'JsFilter',
            'png' => 'PngFilter',
            'jpg' => 'JpegFilter',
            'jpeg' => 'JpegFilter',
            'gif' => 'GifFilter'
        ),
        'assets' => array(
            'Application' => array(
                'media' => array(
                    'img',
                    'fonts'
                ),
                'css' => array(
                    'css'
                ),
                'js' => array(
                    'js/jquery.min.js',
                    'js/bootstrap.min.js',
                    'js/html5.js'
                ),
                'less' => array(
                '@zfRootPath/module/Application/less/bootstrap.less'
                                )
            ),
            'Admin' => array(
                'media' => array(
                    'img'
                ),
                'css' => array(
                    'css'
                ),
                'js' => array(
                    'static/Ext/ux/grid/filter',
                    'static/SysX/Locale.js',
                    'static/SysX/model',
                    'static/SysX/store/abstract',
                    'static/SysX/store',
                    'static/SysX/view/abstract',
                    'static/SysX/view/admin',
                    'static/SysX/view/TreeMenu.js',
                    'static/SysX/view/Viewport.js',
                    'static/SysX/controller/abstract',
                    'static/SysX/controller/admin',
                    'static/SysX/AppManager.js',
                    'static/SysX/application/Admin.js'
                )
            )
        )
    )
);

<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'Admin\Controller\Client' => 'Admin\Controller\Client',
            'Admin\Controller\ClientConfig' => 'Admin\Controller\ClientConfig',
            'Admin\Controller\ClientGroup' => 'Admin\Controller\ClientGroup',
            'Admin\Controller\ClientStatement' => 'Admin\Controller\ClientStatement',
            'Admin\Controller\ClientUser' => 'Admin\Controller\ClientUser',
            'Admin\Controller\ClientUserGroup' => 'Admin\Controller\ClientUserGroup',
            'Admin\Controller\ClientUserGroupPrivilege' => 'Admin\Controller\ClientUserGroupPrivilege',
            'Admin\Controller\Company' => 'Admin\Controller\Company',
            'Admin\Controller\CompanyConfig' => 'Admin\Controller\CompanyConfig',
            'Admin\Controller\CompanyUser' => 'Admin\Controller\CompanyUser',
            'Admin\Controller\CompanyUserGroup' => 'Admin\Controller\CompanyUserGroup',
            'Admin\Controller\CompanyUserGroupPrivilege' => 'Admin\Controller\CompanyUserGroupPrivilege',
            'Admin\Controller\Config' => 'Admin\Controller\Config',
            'Admin\Controller\Country' => 'Admin\Controller\Country',
            'Admin\Controller\Currency' => 'Admin\Controller\Currency',
            'Admin\Controller\Debug' => 'Admin\Controller\Debug',
            'Admin\Controller\Listing' => 'Admin\Controller\Listing',
            'Admin\Controller\ListingMedia' => 'Admin\Controller\ListingMedia',
            'Admin\Controller\Media' => 'Admin\Controller\Media',
            'Admin\Controller\Privilege' => 'Admin\Controller\Privilege',
            'Admin\Controller\Ticket' => 'Admin\Controller\Ticket',
            'Admin\Controller\Vendor' => 'Admin\Controller\Vendor',
        )
    )
);
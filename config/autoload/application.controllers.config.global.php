<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Client' => 'Application\Controller\Client',
            'Application\Controller\ClientConfig' => 'Application\Controller\ClientConfig',
            'Application\Controller\ClientGroup' => 'Application\Controller\ClientGroup',
            'Application\Controller\ClientStatement' => 'Application\Controller\ClientStatement',
            'Application\Controller\ClientUser' => 'Application\Controller\ClientUser',
            'Application\Controller\ClientUserGroup' => 'Application\Controller\ClientUserGroup',
            'Application\Controller\ClientUserGroupPrivilege' => 'Application\Controller\ClientUserGroupPrivilege',
            'Application\Controller\Company' => 'Application\Controller\Company',
            'Application\Controller\CompanyConfig' => 'Application\Controller\CompanyConfig',
            'Application\Controller\CompanyUser' => 'Application\Controller\CompanyUser',
            'Application\Controller\CompanyUserGroup' => 'Application\Controller\CompanyUserGroup',
            'Application\Controller\CompanyUserGroupPrivilege' => 'Application\Controller\CompanyUserGroupPrivilege',
            'Application\Controller\Config' => 'Application\Controller\Config',
            'Application\Controller\Country' => 'Application\Controller\Country',
            'Application\Controller\Currency' => 'Application\Controller\Currency',
            'Application\Controller\Debug' => 'Application\Controller\Debug',
            'Application\Controller\Listing' => 'Application\Controller\Listing',
            'Application\Controller\ListingMedia' => 'Application\Controller\ListingMedia',
            'Application\Controller\Media' => 'Application\Controller\Media',
            'Application\Controller\Privilege' => 'Application\Controller\Privilege',
            'Application\Controller\Ticket' => 'Application\Controller\Ticket',
            'Application\Controller\Vendor' => 'Application\Controller\Vendor',
        )
    )
);
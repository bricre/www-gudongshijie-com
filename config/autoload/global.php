<?php
use SysX\Authentication\AuthenticationService;
use Zend\Db\TableGateway\Feature\GlobalAdapterFeature;
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */
return array(
    'service_manager' => array(
        'factories' => array(
            'cache-dbtable-metadata' => function ()
            {
                return \Zend\Cache\StorageFactory::factory(array(
                    'adapter' => array(
                        'name' => 'filesystem',
                        'options' => array(
                            'cache_dir' => 'data/cache/',
                            'ttl' => 3600
                        )
                    ),
                    'plugins' => array(
                        // Don't throw exceptions on cache errors
                        'exception_handler' => array(
                            'throw_exceptions' => true
                        ),
                        // We store database rows on filesystem so we need to serialize them
                        'Serializer'
                    )
                ));
            },
            'AuthenticationService' => function ($sm)
            {
                $adapter = GlobalAdapterFeature::getStaticAdapter();
                $authenticationService = new AuthenticationService($adapter);
                $authenticationService->setServiceLocator($sm);
                $authenticationService->setTableName('company_user');
                return $authenticationService;
            },
        )
    )
);

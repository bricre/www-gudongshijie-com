<?php
return array(
    'service_manager' => array(
        'factories' => array(
            'Application\Db\TableGateway\Company' => function ($sm)
            {
                $tableGateway = new Application\Db\TableGateway\Company();
                $tableGateway->setServiceLocator($sm);
                return $tableGateway;
            }
        )
    )
);
<?php
namespace ApplicationTest\Controller;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

/**
 * IndexController test case.
 */
class IndexControllerTest extends AbstractHttpControllerTestCase
{

    /**
     *
     * @var IndexController
     */
    private $IndexController;

    /**
     * Prepares the environment before running a test.
     */
    protected function setUp ()
    {
        $this->setApplicationConfig(
                include BASE_PATH . '/config/application.config.php');
        parent::setUp();
    }

    /**
     * Cleans up the environment after running a test.
     */
    protected function tearDown ()
    {
        // TODO Auto-generated IndexControllerTest::tearDown()
        $this->IndexController = null;

        parent::tearDown();
    }

    /**
     * Constructs the test case.
     */
    public function __construct ()
    {
        // TODO Auto-generated constructor
    }

    /**
     * Tests IndexController->indexAction()
     */
    public function testIndexAction ()
    {
        $this->dispatch('/');
        $this->assertResponseStatusCode(200);

        $this->assertModuleName('Application');
        $this->assertControllerName('Application\Controller\Index');
        $this->assertControllerClass('IndexController');
        $this->assertMatchedRouteName('home');
    }

    /**
     * Tests IndexController->testAction()
     */
    public function testTestAction ()
    {
        // TODO Auto-generated IndexControllerTest->testTestAction()
        $this->markTestIncomplete("testAction test not implemented");

        $this->IndexController->testAction(/* parameters */);
    }
}


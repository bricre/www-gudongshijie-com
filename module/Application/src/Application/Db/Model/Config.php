<?php

/**
 * Auto generated by MySQL Workbench Schema Exporter.
 * Version 2.1.3-dev (zf2-dbtable) on 2014-07-17 00:46:12.
 * Goto https://github.com/johmue/mysql-workbench-schema-exporter for more
 * information.
 */

namespace Application\Db\Model;

use Zend\Form\Annotation as Annotation;
use SysX\Db\Model\AbstractModel as Base;

/**
 * Application\Db\Model\Config
 *
 * @Annotation\Hydrator("Zend\Stdlib\Hydrator\ArraySerializable")
 * @Annotation\Name("Config")
 */
class Config extends Base
{
    /**
     * @Annotation\Name("code")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required(true)
     * @Annotation\Options({"label":"Code"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $code;

    /**
     * @Annotation\Name("type")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required(true)
     * @Annotation\Options({"label":"Type"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $type;

    /**
     * @Annotation\Name("default_value")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\AllowEmpty(true)
     * @Annotation\Options({"label":"DefaultValue"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $default_value;

    /**
     * @Annotation\Name("is_for_company")
     * @Annotation\Type("Zend\Form\Element\Radio")
     * @Annotation\Attributes({"options":{"No", "Yes"}})
     * @Annotation\Attributes({"value":0})
     * @Annotation\AllowEmpty(true)
     * @Annotation\Options({"label":"IsForCompany"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $is_for_company;

    /**
     * @Annotation\Name("is_for_client")
     * @Annotation\Type("Zend\Form\Element\Radio")
     * @Annotation\Attributes({"options":{"No", "Yes"}})
     * @Annotation\Attributes({"value":0})
     * @Annotation\AllowEmpty(true)
     * @Annotation\Options({"label":"IsForClient"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $is_for_client;

    /**
     * Set the value of code.
     *
     * @param string $value
     * @return Application\Db\Model
     */
    public function setCode($value)
    {
        $this->code = $value;

        return $this;
    }

    /**
     * Get the value of code.
     *
     * @return string code
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set the value of type.
     *
     * @param string $value
     * @return Application\Db\Model
     */
    public function setType($value)
    {
        $this->type = $value;

        return $this;
    }

    /**
     * Get the value of type.
     *
     * @return string type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set the value of default_value.
     *
     * @param string $value
     * @return Application\Db\Model
     */
    public function setDefaultValue($value)
    {
        $this->default_value = $value;

        return $this;
    }

    /**
     * Get the value of default_value.
     *
     * @return string default_value
     */
    public function getDefaultValue()
    {
        return $this->default_value;
    }

    /**
     * Set the value of is_for_company.
     *
     * @param boolean $value
     * @return Application\Db\Model
     */
    public function setIsForCompany($value)
    {
        $this->is_for_company = $value;

        return $this;
    }

    /**
     * Get the value of is_for_company.
     *
     * @return boolean is_for_company
     */
    public function getIsForCompany()
    {
        return $this->is_for_company;
    }

    /**
     * Set the value of is_for_client.
     *
     * @param boolean $value
     * @return Application\Db\Model
     */
    public function setIsForClient($value)
    {
        $this->is_for_client = $value;

        return $this;
    }

    /**
     * Get the value of is_for_client.
     *
     * @return boolean is_for_client
     */
    public function getIsForClient()
    {
        return $this->is_for_client;
    }

    public function __sleep()
    {
        return array('code', 'type', 'default_value', 'is_for_company', 'is_for_client');
    }
}
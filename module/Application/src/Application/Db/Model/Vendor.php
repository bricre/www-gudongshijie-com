<?php

/**
 * Auto generated by MySQL Workbench Schema Exporter.
 * Version 2.1.3-dev (zf2-dbtable) on 2014-07-17 00:46:12.
 * Goto https://github.com/johmue/mysql-workbench-schema-exporter for more
 * information.
 */

namespace Application\Db\Model;

use Zend\Form\Annotation as Annotation;
use SysX\Db\Model\AbstractModel as Base;

/**
 * Application\Db\Model\Vendor
 *
 * @Annotation\Hydrator("Zend\Stdlib\Hydrator\ArraySerializable")
 * @Annotation\Name("Vendor")
 */
class Vendor extends Base
{
    /**
     * @Annotation\Name("id")
     * @Annotation\Type("Zend\Form\Element\Hidden")
     * @Annotation\Required(true)
     * @Annotation\Options({"label":"Id"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $id;

    /**
     * @Annotation\Name("name")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\AllowEmpty(true)
     * @Annotation\Options({"label":"Name"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $name;

    /**
     * @Annotation\Name("url")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\AllowEmpty(true)
     * @Annotation\Options({"label":"Url"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $url;

    /**
     * @Annotation\Name("address_line_1")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\AllowEmpty(true)
     * @Annotation\Options({"label":"AddressLine1"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $address_line_1;

    /**
     * @Annotation\Name("address_line_2")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\AllowEmpty(true)
     * @Annotation\Options({"label":"AddressLine2"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $address_line_2;

    /**
     * @Annotation\Name("address_line_3")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\AllowEmpty(true)
     * @Annotation\Options({"label":"AddressLine3"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $address_line_3;

    /**
     * @Annotation\Name("city")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\AllowEmpty(true)
     * @Annotation\Options({"label":"City"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $city;

    /**
     * @Annotation\Name("county")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\AllowEmpty(true)
     * @Annotation\Options({"label":"County"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $county;

    /**
     * @Annotation\Name("post_code")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\AllowEmpty(true)
     * @Annotation\Options({"label":"PostCode"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $post_code;

    /**
     * @Annotation\Name("country_iso")
     * @Annotation\Attributes({"value_tablegateway":"Application\Db\TableGateway\Country"})
     * @Annotation\Type("Zend\Form\Element\Select")
     * @Annotation\Required(true)
     * @Annotation\Options({"label":"CountryIso"})
     * @Annotation\Filter({"name":"StringTrim"})
     * @Annotation\Validator({"name":"SysX\Validator\Db\RecordExists", "options":{"table":"country", "field":"iso"}})
     * @Annotation\Validator({"name":"StringLength", "options":{"max":2}})
     */
    protected $country_iso;

    /**
     * @Annotation\Name("telephone")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\AllowEmpty(true)
     * @Annotation\Options({"label":"Telephone"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $telephone;

    /**
     * @Annotation\Name("email")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\AllowEmpty(true)
     * @Annotation\Options({"label":"Email"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $email;

    /**
     * Set the value of id.
     *
     * @param integer $value
     * @return Application\Db\Model
     */
    public function setId($value)
    {
        $this->id = $value;

        return $this;
    }

    /**
     * Get the value of id.
     *
     * @return integer id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of name.
     *
     * @param string $value
     * @return Application\Db\Model
     */
    public function setName($value)
    {
        $this->name = $value;

        return $this;
    }

    /**
     * Get the value of name.
     *
     * @return string name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of url.
     *
     * @param string $value
     * @return Application\Db\Model
     */
    public function setUrl($value)
    {
        $this->url = $value;

        return $this;
    }

    /**
     * Get the value of url.
     *
     * @return string url
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set the value of address_line_1.
     *
     * @param string $value
     * @return Application\Db\Model
     */
    public function setAddressLine1($value)
    {
        $this->address_line_1 = $value;

        return $this;
    }

    /**
     * Get the value of address_line_1.
     *
     * @return string address_line_1
     */
    public function getAddressLine1()
    {
        return $this->address_line_1;
    }

    /**
     * Set the value of address_line_2.
     *
     * @param string $value
     * @return Application\Db\Model
     */
    public function setAddressLine2($value)
    {
        $this->address_line_2 = $value;

        return $this;
    }

    /**
     * Get the value of address_line_2.
     *
     * @return string address_line_2
     */
    public function getAddressLine2()
    {
        return $this->address_line_2;
    }

    /**
     * Set the value of address_line_3.
     *
     * @param string $value
     * @return Application\Db\Model
     */
    public function setAddressLine3($value)
    {
        $this->address_line_3 = $value;

        return $this;
    }

    /**
     * Get the value of address_line_3.
     *
     * @return string address_line_3
     */
    public function getAddressLine3()
    {
        return $this->address_line_3;
    }

    /**
     * Set the value of city.
     *
     * @param string $value
     * @return Application\Db\Model
     */
    public function setCity($value)
    {
        $this->city = $value;

        return $this;
    }

    /**
     * Get the value of city.
     *
     * @return string city
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set the value of county.
     *
     * @param string $value
     * @return Application\Db\Model
     */
    public function setCounty($value)
    {
        $this->county = $value;

        return $this;
    }

    /**
     * Get the value of county.
     *
     * @return string county
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * Set the value of post_code.
     *
     * @param string $value
     * @return Application\Db\Model
     */
    public function setPostCode($value)
    {
        $this->post_code = $value;

        return $this;
    }

    /**
     * Get the value of post_code.
     *
     * @return string post_code
     */
    public function getPostCode()
    {
        return $this->post_code;
    }

    /**
     * Set the value of country_iso.
     *
     * @param string $value
     * @return Application\Db\Model
     */
    public function setCountryIso($value)
    {
        $this->country_iso = $value;

        return $this;
    }

    /**
     * Get the value of country_iso.
     *
     * @return string country_iso
     */
    public function getCountryIso()
    {
        return $this->country_iso;
    }

    /**
     * Set the value of telephone.
     *
     * @param string $value
     * @return Application\Db\Model
     */
    public function setTelephone($value)
    {
        $this->telephone = $value;

        return $this;
    }

    /**
     * Get the value of telephone.
     *
     * @return string telephone
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set the value of email.
     *
     * @param string $value
     * @return Application\Db\Model
     */
    public function setEmail($value)
    {
        $this->email = $value;

        return $this;
    }

    /**
     * Get the value of email.
     *
     * @return string email
     */
    public function getEmail()
    {
        return $this->email;
    }

    public function __sleep()
    {
        return array('id', 'name', 'url', 'address_line_1', 'address_line_2', 'address_line_3', 'city', 'county', 'post_code', 'country_iso', 'telephone', 'email');
    }
}
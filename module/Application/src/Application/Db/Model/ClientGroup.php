<?php

/**
 * Auto generated by MySQL Workbench Schema Exporter.
 * Version 2.1.3-dev (zf2-dbtable) on 2014-07-17 00:46:12.
 * Goto https://github.com/johmue/mysql-workbench-schema-exporter for more
 * information.
 */

namespace Application\Db\Model;

use Zend\Form\Annotation as Annotation;
use SysX\Db\Model\AbstractModel as Base;

/**
 * Application\Db\Model\ClientGroup
 *
 * @Annotation\Hydrator("Zend\Stdlib\Hydrator\ArraySerializable")
 * @Annotation\Name("ClientGroup")
 */
class ClientGroup extends Base
{
    /**
     * @Annotation\Name("id")
     * @Annotation\Type("Zend\Form\Element\Hidden")
     * @Annotation\Required(true)
     * @Annotation\Options({"label":"Id"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $id;

    /**
     * @Annotation\Name("name")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\AllowEmpty(true)
     * @Annotation\Options({"label":"Name"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $name;

    /**
     * @Annotation\Name("handling_fee_limit")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required(true)
     * @Annotation\Options({"label":"HandlingFeeLimit"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $handling_fee_limit;

    /**
     * @Annotation\Name("handling_fee_first_item")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required(true)
     * @Annotation\Options({"label":"HandlingFeeFirstItem"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $handling_fee_first_item;

    /**
     * @Annotation\Name("handling_fee_other_item")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required(true)
     * @Annotation\Options({"label":"HandlingFeeOtherItem"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $handling_fee_other_item;

    /**
     * @Annotation\Name("handling_fee_first_product")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required(true)
     * @Annotation\Options({"label":"HandlingFeeFirstProduct"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $handling_fee_first_product;

    /**
     * @Annotation\Name("handling_fee_other_product")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required(true)
     * @Annotation\Options({"label":"HandlingFeeOtherProduct"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $handling_fee_other_product;

    /**
     * @Annotation\Name("delivery_fee")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required(true)
     * @Annotation\Options({"label":"DeliveryFee"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $delivery_fee;

    /**
     * @Annotation\Name("delivery_fee_type")
     * @Annotation\Type("Zend\Form\Element\Radio")
     * @Annotation\Attributes({"options":{"No", "Yes"}})
     * @Annotation\Attributes({"value":0})
     * @Annotation\Required(true)
     * @Annotation\Options({"label":"DeliveryFeeType"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $delivery_fee_type;

    /**
     * @Annotation\Name("monthly_membership_fee")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required(true)
     * @Annotation\Options({"label":"MonthlyMembershipFee"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $monthly_membership_fee;

    /**
     * @Annotation\Name("credit_limit_top_up_period")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required(true)
     * @Annotation\Options({"label":"CreditLimitTopUpPeriod"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $credit_limit_top_up_period;

    /**
     * @Annotation\Name("credit_limit_top_up_amount")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required(true)
     * @Annotation\Options({"label":"CreditLimitTopUpAmount"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $credit_limit_top_up_amount;

    /**
     * @Annotation\Name("credit_limit_top_up_times")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required(true)
     * @Annotation\Options({"label":"CreditLimitTopUpTimes"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $credit_limit_top_up_times;

    /**
     * @Annotation\Name("credit_limit_top_up_period_per_time")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required(true)
     * @Annotation\Options({"label":"CreditLimitTopUpPeriodPerTime"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $credit_limit_top_up_period_per_time;

    /**
     * @Annotation\Name("status")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required(true)
     * @Annotation\Options({"label":"Status"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $status;

    /**
     * Set the value of id.
     *
     * @param integer $value
     * @return Application\Db\Model
     */
    public function setId($value)
    {
        $this->id = $value;

        return $this;
    }

    /**
     * Get the value of id.
     *
     * @return integer id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of name.
     *
     * @param string $value
     * @return Application\Db\Model
     */
    public function setName($value)
    {
        $this->name = $value;

        return $this;
    }

    /**
     * Get the value of name.
     *
     * @return string name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of handling_fee_limit.
     *
     * @param double $value
     * @return Application\Db\Model
     */
    public function setHandlingFeeLimit($value)
    {
        $this->handling_fee_limit = $value;

        return $this;
    }

    /**
     * Get the value of handling_fee_limit.
     *
     * @return double handling_fee_limit
     */
    public function getHandlingFeeLimit()
    {
        return $this->handling_fee_limit;
    }

    /**
     * Set the value of handling_fee_first_item.
     *
     * @param double $value
     * @return Application\Db\Model
     */
    public function setHandlingFeeFirstItem($value)
    {
        $this->handling_fee_first_item = $value;

        return $this;
    }

    /**
     * Get the value of handling_fee_first_item.
     *
     * @return double handling_fee_first_item
     */
    public function getHandlingFeeFirstItem()
    {
        return $this->handling_fee_first_item;
    }

    /**
     * Set the value of handling_fee_other_item.
     *
     * @param double $value
     * @return Application\Db\Model
     */
    public function setHandlingFeeOtherItem($value)
    {
        $this->handling_fee_other_item = $value;

        return $this;
    }

    /**
     * Get the value of handling_fee_other_item.
     *
     * @return double handling_fee_other_item
     */
    public function getHandlingFeeOtherItem()
    {
        return $this->handling_fee_other_item;
    }

    /**
     * Set the value of handling_fee_first_product.
     *
     * @param double $value
     * @return Application\Db\Model
     */
    public function setHandlingFeeFirstProduct($value)
    {
        $this->handling_fee_first_product = $value;

        return $this;
    }

    /**
     * Get the value of handling_fee_first_product.
     *
     * @return double handling_fee_first_product
     */
    public function getHandlingFeeFirstProduct()
    {
        return $this->handling_fee_first_product;
    }

    /**
     * Set the value of handling_fee_other_product.
     *
     * @param double $value
     * @return Application\Db\Model
     */
    public function setHandlingFeeOtherProduct($value)
    {
        $this->handling_fee_other_product = $value;

        return $this;
    }

    /**
     * Get the value of handling_fee_other_product.
     *
     * @return double handling_fee_other_product
     */
    public function getHandlingFeeOtherProduct()
    {
        return $this->handling_fee_other_product;
    }

    /**
     * Set the value of delivery_fee.
     *
     * @param double $value
     * @return Application\Db\Model
     */
    public function setDeliveryFee($value)
    {
        $this->delivery_fee = $value;

        return $this;
    }

    /**
     * Get the value of delivery_fee.
     *
     * @return double delivery_fee
     */
    public function getDeliveryFee()
    {
        return $this->delivery_fee;
    }

    /**
     * Set the value of delivery_fee_type.
     *
     * @param boolean $value
     * @return Application\Db\Model
     */
    public function setDeliveryFeeType($value)
    {
        $this->delivery_fee_type = $value;

        return $this;
    }

    /**
     * Get the value of delivery_fee_type.
     *
     * @return boolean delivery_fee_type
     */
    public function getDeliveryFeeType()
    {
        return $this->delivery_fee_type;
    }

    /**
     * Set the value of monthly_membership_fee.
     *
     * @param double $value
     * @return Application\Db\Model
     */
    public function setMonthlyMembershipFee($value)
    {
        $this->monthly_membership_fee = $value;

        return $this;
    }

    /**
     * Get the value of monthly_membership_fee.
     *
     * @return double monthly_membership_fee
     */
    public function getMonthlyMembershipFee()
    {
        return $this->monthly_membership_fee;
    }

    /**
     * Set the value of credit_limit_top_up_period.
     *
     * @param integer $value
     * @return Application\Db\Model
     */
    public function setCreditLimitTopUpPeriod($value)
    {
        $this->credit_limit_top_up_period = $value;

        return $this;
    }

    /**
     * Get the value of credit_limit_top_up_period.
     *
     * @return integer credit_limit_top_up_period
     */
    public function getCreditLimitTopUpPeriod()
    {
        return $this->credit_limit_top_up_period;
    }

    /**
     * Set the value of credit_limit_top_up_amount.
     *
     * @param integer $value
     * @return Application\Db\Model
     */
    public function setCreditLimitTopUpAmount($value)
    {
        $this->credit_limit_top_up_amount = $value;

        return $this;
    }

    /**
     * Get the value of credit_limit_top_up_amount.
     *
     * @return integer credit_limit_top_up_amount
     */
    public function getCreditLimitTopUpAmount()
    {
        return $this->credit_limit_top_up_amount;
    }

    /**
     * Set the value of credit_limit_top_up_times.
     *
     * @param integer $value
     * @return Application\Db\Model
     */
    public function setCreditLimitTopUpTimes($value)
    {
        $this->credit_limit_top_up_times = $value;

        return $this;
    }

    /**
     * Get the value of credit_limit_top_up_times.
     *
     * @return integer credit_limit_top_up_times
     */
    public function getCreditLimitTopUpTimes()
    {
        return $this->credit_limit_top_up_times;
    }

    /**
     * Set the value of credit_limit_top_up_period_per_time.
     *
     * @param integer $value
     * @return Application\Db\Model
     */
    public function setCreditLimitTopUpPeriodPerTime($value)
    {
        $this->credit_limit_top_up_period_per_time = $value;

        return $this;
    }

    /**
     * Get the value of credit_limit_top_up_period_per_time.
     *
     * @return integer credit_limit_top_up_period_per_time
     */
    public function getCreditLimitTopUpPeriodPerTime()
    {
        return $this->credit_limit_top_up_period_per_time;
    }

    /**
     * Set the value of status.
     *
     * @param string $value
     * @return Application\Db\Model
     */
    public function setStatus($value)
    {
        $this->status = $value;

        return $this;
    }

    /**
     * Get the value of status.
     *
     * @return string status
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function __sleep()
    {
        return array('id', 'name', 'handling_fee_limit', 'handling_fee_first_item', 'handling_fee_other_item', 'handling_fee_first_product', 'handling_fee_other_product', 'delivery_fee', 'delivery_fee_type', 'monthly_membership_fee', 'credit_limit_top_up_period', 'credit_limit_top_up_amount', 'credit_limit_top_up_times', 'credit_limit_top_up_period_per_time', 'status');
    }
}
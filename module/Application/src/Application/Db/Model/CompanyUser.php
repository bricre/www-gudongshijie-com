<?php

/**
 * Auto generated by MySQL Workbench Schema Exporter.
 * Version 2.1.3-dev (zf2-dbtable) on 2014-07-17 00:46:12.
 * Goto https://github.com/johmue/mysql-workbench-schema-exporter for more
 * information.
 */

namespace Application\Db\Model;

use Zend\Form\Annotation as Annotation;
use SysX\Db\Model\AbstractModel as Base;

/**
 * Application\Db\Model\CompanyUser
 *
 * @Annotation\Hydrator("Zend\Stdlib\Hydrator\ArraySerializable")
 * @Annotation\Name("CompanyUser")
 */
class CompanyUser extends Base
{
    /**
     * @Annotation\Name("id")
     * @Annotation\Type("Zend\Form\Element\Hidden")
     * @Annotation\Required(true)
     * @Annotation\Options({"label":"Id"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $id;

    /**
     * @Annotation\Name("company_id")
     * @Annotation\Attributes({"value_tablegateway":"Application\Db\TableGateway\Company"})
     * @Annotation\Type("Zend\Form\Element\Select")
     * @Annotation\Required(true)
     * @Annotation\Options({"label":"Company"})
     * @Annotation\Filter({"name":"StringTrim"})
     * @Annotation\Validator({"name":"SysX\Validator\Db\RecordExists", "options":{"table":"company", "field":"id"}})
     */
    protected $company_id;

    /**
     * @Annotation\Name("company_user_group_id")
     * @Annotation\Attributes({"value_tablegateway":"Application\Db\TableGateway\CompanyUserGroup"})
     * @Annotation\Type("Zend\Form\Element\Select")
     * @Annotation\AllowEmpty(true)
     * @Annotation\Options({"label":"CompanyUserGroup"})
     * @Annotation\Filter({"name":"StringTrim"})
     * @Annotation\Validator({"name":"SysX\Validator\Db\RecordExists", "options":{"table":"company_user_group", "field":"id"}})
     */
    protected $company_user_group_id;

    /**
     * @Annotation\Name("username")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required(true)
     * @Annotation\Options({"label":"Username"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $username;

    /**
     * @Annotation\Name("password")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required(true)
     * @Annotation\Options({"label":"Password"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $password;

    /**
     * @Annotation\Name("api_key")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\AllowEmpty(true)
     * @Annotation\Options({"label":"ApiKey"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $api_key;

    /**
     * @Annotation\Name("api_key_crc32")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required(true)
     * @Annotation\Options({"label":"ApiKeyCrc32"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $api_key_crc32;

    /**
     * @Annotation\Name("email")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\AllowEmpty(true)
     * @Annotation\Options({"label":"Email"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $email;

    /**
     * @Annotation\Name("first_name")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\AllowEmpty(true)
     * @Annotation\Options({"label":"FirstName"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $first_name;

    /**
     * @Annotation\Name("last_name")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\AllowEmpty(true)
     * @Annotation\Options({"label":"LastName"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $last_name;

    /**
     * @Annotation\Name("status")
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required(true)
     * @Annotation\Options({"label":"Status"})
     * @Annotation\Filter({"name":"StringTrim"})
     */
    protected $status;

    /**
     * Set the value of id.
     *
     * @param integer $value
     * @return Application\Db\Model
     */
    public function setId($value)
    {
        $this->id = $value;

        return $this;
    }

    /**
     * Get the value of id.
     *
     * @return integer id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of company_id.
     *
     * @param integer $value
     * @return Application\Db\Model
     */
    public function setCompanyId($value)
    {
        $this->company_id = $value;

        return $this;
    }

    /**
     * Get the value of company_id.
     *
     * @return integer company_id
     */
    public function getCompanyId()
    {
        return $this->company_id;
    }

    /**
     * Set the value of company_user_group_id.
     *
     * @param integer $value
     * @return Application\Db\Model
     */
    public function setCompanyUserGroupId($value)
    {
        $this->company_user_group_id = $value;

        return $this;
    }

    /**
     * Get the value of company_user_group_id.
     *
     * @return integer company_user_group_id
     */
    public function getCompanyUserGroupId()
    {
        return $this->company_user_group_id;
    }

    /**
     * Set the value of username.
     *
     * @param string $value
     * @return Application\Db\Model
     */
    public function setUsername($value)
    {
        $this->username = $value;

        return $this;
    }

    /**
     * Get the value of username.
     *
     * @return string username
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set the value of password.
     *
     * @param string $value
     * @return Application\Db\Model
     */
    public function setPassword($value)
    {
        $this->password = $value;

        return $this;
    }

    /**
     * Get the value of password.
     *
     * @return string password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of api_key.
     *
     * @param string $value
     * @return Application\Db\Model
     */
    public function setApiKey($value)
    {
        $this->api_key = $value;

        return $this;
    }

    /**
     * Get the value of api_key.
     *
     * @return string api_key
     */
    public function getApiKey()
    {
        return $this->api_key;
    }

    /**
     * Set the value of api_key_crc32.
     *
     * @param integer $value
     * @return Application\Db\Model
     */
    public function setApiKeyCrc32($value)
    {
        $this->api_key_crc32 = $value;

        return $this;
    }

    /**
     * Get the value of api_key_crc32.
     *
     * @return integer api_key_crc32
     */
    public function getApiKeyCrc32()
    {
        return $this->api_key_crc32;
    }

    /**
     * Set the value of email.
     *
     * @param string $value
     * @return Application\Db\Model
     */
    public function setEmail($value)
    {
        $this->email = $value;

        return $this;
    }

    /**
     * Get the value of email.
     *
     * @return string email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of first_name.
     *
     * @param string $value
     * @return Application\Db\Model
     */
    public function setFirstName($value)
    {
        $this->first_name = $value;

        return $this;
    }

    /**
     * Get the value of first_name.
     *
     * @return string first_name
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Set the value of last_name.
     *
     * @param string $value
     * @return Application\Db\Model
     */
    public function setLastName($value)
    {
        $this->last_name = $value;

        return $this;
    }

    /**
     * Get the value of last_name.
     *
     * @return string last_name
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Set the value of status.
     *
     * @param string $value
     * @return Application\Db\Model
     */
    public function setStatus($value)
    {
        $this->status = $value;

        return $this;
    }

    /**
     * Get the value of status.
     *
     * @return string status
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function __sleep()
    {
        return array('id', 'company_id', 'company_user_group_id', 'username', 'password', 'api_key', 'api_key_crc32', 'email', 'first_name', 'last_name', 'status');
    }
}
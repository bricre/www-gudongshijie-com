<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Application\Controller;

use SysX\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Application\Db\TableGateway\Listing as TableGateway;
use Application\Form\Listing as Form;

class IndexController extends AbstractActionController
{

    public function onDispatch(MvcEvent $e)
    {
        $this->form = Form::class;
        $this->tableGateway = TableGateway::class;

        parent::onDispatch($e);
    }

    public function indexAction()
    {
        parent::indexAction();
        return $this->viewModel->setTemplate('application/index/index');
    }
}
<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Admin for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Admin;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\MvcEvent;

class Module implements AutoloaderProviderInterface
{

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php'
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/', __NAMESPACE__)
                )
            )
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $eventManager->attach(MvcEvent::EVENT_DISPATCH, function (MvcEvent $e)
        {
            $controllerClass = $e->getRouteMatch()
                ->getParam('controller');
            $controller = new $controllerClass();
            if (is_subclass_of($controller, 'SysX\Authentication\AuthenticationServiceAwareInterface')) {
                $authenticationService = $e->getApplication()
                    ->getServiceManager()
                    ->get('AuthenticationService');
                $controller->setAuthenticationService($authenticationService);
                if (! $controller->getAuthenticationService()
                    ->isAuthenticated()) {
                    $url = $e->getRouter()
                        ->assemble(array(), array(
                        'name' => 'admin/login'
                    ));
                    $response = $e->getResponse();
                    $response->getHeaders()
                        ->addHeaderLine('Location', $url);
                    $response->setStatusCode(302);
                    $response->sendHeaders();
                    $stopCallBack = function ($event) use($response)
                    {
                        $event->stopPropagation();
                        return $response;
                    };

                    $e->getApplication()
                        ->getEventManager()
                        ->attach(MvcEvent::EVENT_ROUTE, $stopCallBack, - 10000);
                    return $response;
                }
            }
        });

        $translator = $e->getApplication()
            ->getServiceManager()
            ->get('translator');
        @$translator->setLocale(\Locale::acceptFromHttp($_SERVER['HTTP_ACCEPT_LANGUAGE']))->setFallbackLocale('zh_CN');
    }
}

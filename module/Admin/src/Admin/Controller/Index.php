<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Admin for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Admin\Controller;

use SysX\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Admin\Db\TableGateway\CompanyUser;
use Zend\View\Model\JsonModel;

class Index extends AbstractActionController
{

    public function indexAction()
    {
        $this->viewModel = new ViewModel();

        $this->viewModel->setVariable('locale', $this->getServiceLocator()
            ->get('translator')
            ->getLocale());
        $this->viewModel->sl = $this->getServiceLocator();

        $auth = $this->getServiceLocator()->get('AuthenticationService');

        if ($auth->isAuthenticated()) {
            $this->viewModel->user = $auth->getIdentityModel();
        }
        $this->layout('layout/admin');
        return $this->viewModel;
    }

    public function loginAction()
    {
        $User = new CompanyUser();
        $auth = $this->getServiceLocator()->get('AuthenticationService');
        $auth->setIdentity($this->params()
            ->fromPost('username'));
        $auth->setCredential($this->params()
            ->fromPost('password'));
        if (! $auth->authenticate()) {
            return new JsonModel(array(
                'success' => false,
                'message' => $this->_('Incorrect login!')
            ));
        } else
            return new JsonModel(array(
                'success' => true
            ));
    }

    public function editAction()
    {
        throw new \DomainException($this->_('Action now allowed!'));
    }

    public function deleteAction()
    {
        throw new \DomainException($this->_('Action now allowed!'));
    }

    public function createAction()
    {
        throw new \DomainException($this->_('Action now allowed!'));
    }

    public function s3Action()
    {
    }
}
